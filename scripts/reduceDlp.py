import sys
from tqdm import tqdm

ATTRIBUTE_VALUE_MAP = {}

if __name__ == "__main__":
    filename = sys.argv[1] if len(sys.argv) > 1 else input('file> ')
    output_name = filename.replace('.dlp', '-reduced.dlp')

    file_input = open(filename, 'r', encoding='utf8')
    file_output = open(output_name, 'w', encoding='utf8')

    for line in tqdm(file_input):
        pred_offset = line.index('(')

        pred = line[:pred_offset]
        attributes = line[pred_offset+1:-3].split(',')
        reduced_attributes = []

        for attr in attributes:
            attr = attr.strip()[1:-1] # Remove extra space and quotes

            if attr not in ATTRIBUTE_VALUE_MAP:
                ATTRIBUTE_VALUE_MAP[attr] = str(len(ATTRIBUTE_VALUE_MAP))

            if attr[0] == '_':
                reduced_attributes.append('\'_' + ATTRIBUTE_VALUE_MAP[attr] + '\'')
            
            else:
                reduced_attributes.append('\'' + ATTRIBUTE_VALUE_MAP[attr] + '\'')

        file_output.write('%s(%s).\n' % (pred, ', '.join(reduced_attributes)))

    file_input.close()
    file_output.close()