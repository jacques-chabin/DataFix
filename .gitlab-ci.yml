# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Maven.gitlab-ci.yml

# Build JAVA applications using Apache Maven (http://maven.apache.org)
# For docker image tags see https://hub.docker.com/_/maven/
#
# For general lifecycle information see https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html

# This template will build and test your projects
# * Caches downloaded dependencies and plugins between invocation.
# * Verify but don't deploy merge requests.
# * Deploy built artifacts from master branch only.

variables:
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: >-
    -Dhttps.protocols=TLSv1.2
    -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository
    -Dmaven.test.failure.ignore=true
    -Dorg.slf4j.simpleLogger.showDateTime=true
    -Djava.awt.headless=true

  # As of Maven 3.3.0 instead of this you MAY define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # As of Maven 3.6.1, the use of `--no-tranfer-progress` (or `-ntp`) suppresses download and upload messages. The use
  # of the `Slf4jMavenTransferListener` is no longer necessary.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: >-
    --batch-mode
    --errors
    --fail-at-end
    --show-version
    --no-transfer-progress
    -DinstallAtEnd=true
    -DdeployAtEnd=true

# This template uses the latest Maven 3 release, e.g., 3.8.6, and OpenJDK 11 (LTS)
# for verifying and deploying images
# Maven 3.8.x REQUIRES HTTPS repositories.
# See https://maven.apache.org/docs/3.8.1/release-notes.html#how-to-fix-when-i-get-a-http-repository-blocked for more.
image: maven:3-openjdk-11

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
# Be aware that `mvn deploy` will install the built jar into this repository. If you notice your cache size
# increasing, consider adding `-Dmaven.install.skip=true` to `MAVEN_OPTS` or in `.mvn/maven.config`
cache:
  paths:
    - .m2/repository

stages:
  - test
  - deploy
  - container
  - benchmarks

.maven:
  # Database used for tests
  services:
    - name: mysql:8
      alias: mysql
      variables:
        MYSQL_ROOT_PASSWORD: root
        MYSQL_DATABASE: test_database
        MYSQL_USER: test_user
        MYSQL_PASSWORD: test_pass

    - name: neo4j:4.4
      alias: neo4j
      variables:
        NEO4J_AUTH: neo4j/root
        NEO4JLABS_PLUGINS: '["apoc"]'
        NEO4J_dbms_security_procedures_unrestricted: apoc.*
        NEO4J_dbms_security_procedures_whitelist: apoc.*
        NEO4J_dbms_tx__log_rotation_retention__policy: keep_none

  before_script:
    # Wait for databases to be online
    - rounds=32;
      while [ $rounds -gt 0 ]; do
      set +e;
      curl -fsSL http://neo4j:7474 | grep html && break;
      set -e;
      rounds=$(($rounds - 1));
      sleep 10;
      done;

  artifacts:
    when: always
    paths:
      - target
    reports:
      junit: target/surefire-reports/TEST-*.xml
      codequality: target/codeclimate.json

# For merge requests do not `deploy` but only run `verify`.
# See https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html
verify:
  extends: .maven
  stage: test
  script:
    - mvn $MAVEN_CLI_OPTS verify
  except:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# To deploy packages from CI, create a `ci_settings.xml` file
# For deploying packages to GitLab's Maven Repository: See https://docs.gitlab.com/ee/user/packages/maven_repository/index.html#create-maven-packages-with-gitlab-cicd for more details.
# Please note: The GitLab Maven Repository is currently only available in GitLab Premium / Ultimate.
# For `master` or `main` branch run `mvn deploy` automatically.
deploy:
  extends: .maven
  stage: deploy
  script:
    - if [ ! -f ci_settings.xml ]; then
      echo "CI settings missing\! If deploying to GitLab Maven Repository, please see https://docs.gitlab.com/ee/user/packages/maven_repository/index.html#create-maven-packages-with-gitlab-cicd for instructions.";
      fi
    - mvn $MAVEN_CLI_OPTS deploy --settings ci_settings.xml
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# For merge requests do not push image but only build OCI.
build container image:
  stage: container
  needs:
    - verify
  dependencies:
    - verify
  script:
    - mvn jib:buildTar --settings ci_settings.xml
  except:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

build-push container image:
  stage: container
  needs:
    - deploy
  dependencies:
    - deploy
  script:
    - if [ ! -f ci_settings.xml ]; then
      echo "CI settings missing\! If deploying to GitLab Registry, please see https://docs.gitlab.com/ee/user/packages/maven_repository/index.html#create-maven-packages-with-gitlab-cicd for instructions.";
      fi
    - mvn jib:build --settings ci_settings.xml
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

benchmarks:
  stage: benchmarks
  allow_failure: true
  needs:
    - build-push container image
  trigger:
    include: .benchs-ci.yml
  when: manual
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
