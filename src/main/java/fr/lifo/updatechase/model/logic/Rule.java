package fr.lifo.updatechase.model.logic;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.UnauthorizedRuleException;
import fr.lifo.updatechase.model.db.Bdd;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p><b>Rule represente une regle a appliquer</b>
 * Que ce une source-to-target Tuple Generating Dependency, target TGD oubien encore target Equality
 * Generating Dependency.</p>
 * <p>Rule est compose de :
 * <ul><li>body : une ArrayList<Atome></li>
 * <li>head : un Atome</li></p>
 *
 * @see Atom
 */
public class Rule implements Comparable<Rule> {

  protected final Atom head;
  protected final List<Atom> body;
  protected final List<Atom> atomesToDelInBody; //La structure du ou des atome(s) à supprimer quand on voudra supprimer un atome

  /**
   * Constructeur de Rule.
   *
   * @param h L'Atome de la partie head de la regle.
   * @param b Une liste d'Atome qui composent le body de la regle.
   */
  public Rule(Atom h, List<Atom> b) {
    atomesToDelInBody = new ArrayList<>();
    body = new ArrayList<>();
    for (Atom a : b) {
      body.add(a);
      if (a.isDeletableInRuleBody() || b.size() == 1) {
        atomesToDelInBody.add(a);
      }
    }
    //si on a pas choisi d'atome to del, alors il choisi le premier par defaut
    if (atomesToDelInBody.size() == 0) {
      b.get(0).setFullName(b.get(0).getName() + "-");
      atomesToDelInBody.add(b.get(0));
    }
    head = h;
  }

  /**
   * Constructeur de Rule. SPECIFIQUE A LA FONCTION CORE pour trouver une substitution
   *
   * @param h L'Atome de la partie head de la regle.
   * @param b Une liste d'Atome qui composent le body de la regle.
   */
  public Rule(Atom h, List<Atom> b, boolean forCore) {
    atomesToDelInBody = new ArrayList<>();
    body = new ArrayList<>();
    for (Atom a : b) {
      body.add(a);
      if (a.isDeletableInRuleBody() || b.size() == 1) {
        atomesToDelInBody.add(a);
      }
    }
    head = h;
  }

  /**
   * Constructeur de Rule.
   *
   * @param h L'Atome de la partie head de la regle.
   * @param b Un ensemble d'Atome qui composent le body de la regle.
   */
  public Rule(Atom h, Set<Atom> b) {
    atomesToDelInBody = new ArrayList<>();
    body = new ArrayList<>();
    for (Atom a : b) {
      body.add(a);
      if (a.isDeletableInRuleBody() || b.size() == 1) {
        atomesToDelInBody.add(a);
      }
    }
    //si on a pas choisi d'atome to del, alors il choisi le premier par defaut
    if (atomesToDelInBody.size() == 0) {
      ArrayList<Atom> b1 = new ArrayList<>(b);
      b1.get(0).setFullName(b1.get(0).getName() + "-");
      atomesToDelInBody.add(b1.get(0));
    }
    head = h;
  }

  /**
   * Constructeur de Rule. SPECIFIQUE A LA FONCTION CORE pour trouver une substitution
   *
   * @param h L'Atome de la partie head de la regle.
   * @param b Une liste d'Atome qui composent le body de la regle.
   */
  public Rule(Atom h, Set<Atom> b, boolean forCore) {
    atomesToDelInBody = new ArrayList<>();
    body = new ArrayList<>();
    for (Atom a : b) {
      body.add(a);
      if (a.isDeletableInRuleBody() || b.size() == 1) {
        atomesToDelInBody.add(a);
      }
    }
    head = h;
  }

  /**
   * Parse un String en Rule compose d'Atome.
   *
   * @param r Le String source depuis duquel la Rule est parsee.
   * @return La Rule parsee a partir du String.
   * @throws SyntaxError Exception levee si le String s ne correspond pas a une Rule.
   * @see SyntaxError
   */
  public static Rule stringToRule(String r) throws SyntaxError {
    //r=r.replace(" ", "");
    //System.out.println("before delspace" +r);

    //r=LinkedListRules.delSpace(r);
    //System.out.println("after delspace" +r);
    //System.out.println("Step 1 delspace ok");

    //int nombreEntier;
    //System.out.println("Rentrez un nombre positif ou nul :");
    //Scanner lectureClavier = new Scanner(System.in);
    //nombreEntier = lectureClavier.nextInt();
    String tete;
    String corp;
    int pos = r.indexOf("->");
    //System.out.println("Step 2 indexof ok");
    boolean isAQuery = false;
    if (pos != -1) {
      corp = r.substring(0, pos);
      tete = r.substring(pos + 2);
    } else {
      pos = r.indexOf("<-");
      if (pos != -1) {
        corp = r.substring(pos + 2);
        tete = r.substring(0, pos);

      } else {
        throw new SyntaxError("Syntax error in " + r);
      }
    }
    if (tete.charAt(0) == '?') {
      isAQuery = true;
    }

    Atom t = Atom.stringToAtom(tete);

    // supprime eventuellement [....] les crochets autour du corp
    if (corp.charAt(0) == '[') {
      corp = corp.substring(1, corp.length() - 1);
    }
    //pour simplifeir le split d'après
    corp = corp.replace("),", ");");

    //System.out.println("Step 3 replace ok");

    List<Atom> lebody = new ArrayList<>();
    Atom t2;
    String[] cpSplit = corp.split(";");
    //System.out.println("Step 4 split ok " + cpSplit.length + " strings");
    //for(String s2 : corp.split(";")){

    int limit = 0;
    Map<String, Integer> nbAForName = new HashMap<>();
    boolean ok;
    for (String s2 : cpSplit) {
      ok = true;
      if (limit > 0) {
        String name = s2.substring(0, s2.indexOf("("));
        if (nbAForName.containsKey(name)) {
          if (nbAForName.get(name) < limit) {
            nbAForName.put(name, nbAForName.get(name) + 1);
          } else {
            ok = false;
          }
        } else {
          nbAForName.put(name, 1);
        }
      }

      if (ok) {
        t2 = Atom.stringToAtom(s2);
        lebody.add(t2);
      }
    }

    if (lebody.size() == 1 && !lebody.get(0).isDeletableInRuleBody()) {
      lebody.get(0).setFullName(lebody.get(0).getFullName() + "-");
    }

    if (isAQuery) {
      return new Query(t, lebody);
    } else {
      return new Rule(t, lebody);
    }
  }

  /**
   * Getter du body.
   *
   * @return ArrayList<Atome> La liste des Atome du body de Rule.
   */
  public List<Atom> getBody() {
    return Collections.unmodifiableList(this.body);
  }

  /**
   * Getteur du head.
   *
   * @return Atome L'Atome du head de Rule.
   */
  public Atom getHead() {
    return head;
  }

  /**
   * Gere l'affichage de Rule.
   *
   * @return String Le String qui represente une regle.
   */
  public String toString() {
    StringBuilder res = new StringBuilder();
    for (Atom a : body) {
      res.append(a).append(", ");
    }
    res = new StringBuilder(res.substring(0, res.length() - 2));
    res.append(" -> ").append(head);
    return res.toString();
  }

  public String getHeadNames() {
    return head.getName();
  }

  public void setHeadNames(String s) {
    head.setName(s);
  }

  public List<Atom> getAtomesToDelInBody() {
    return atomesToDelInBody;
  }

  @Override
  public boolean equals(Object o) {
    return o instanceof Rule && toString().equals(o.toString());
  }

  @Override
  public int hashCode() {
    return (toString()).hashCode();
  }

  @Override
  public int compareTo(Rule o) {
    return toString().compareTo(o.toString());
  }

  /**
   * Renomme les Atome d'une Rule a l'aide d'un entier.
   *
   * @param num L'entier utilise pour le renommage.
   * @return Une nouvelle Rule dont les Atome sont renommes.
   */
  public Rule renameVar(int num) {
    Atom h = this.head.renameVar(num);
    List<Atom> newb = new ArrayList<>();
    for (Atom a : this.body) {
      newb.add(a.renameVar(num));
    }
    return new Rule(h, newb);
  }

  public boolean bodyContainsOne(Collection<String> s) {
    return this.body.stream().map(Atom::getName).anyMatch(s::contains);
  }

  public boolean bodyContains(String s) {
    return this.body.stream().map(Atom::getName).anyMatch(s::equals);
  }

  /**
   * Reverses a Rule of type (1 Atom -> 1 Atom) Head and body are interchanged: (XX -> XY) becomes
   * (XY -> XX)
   *
   * @return Rule The reversed Rule
   */
  public Rule reverse() throws UnauthorizedRuleException {
    if (this.body.size() != 1) {
      throw new UnauthorizedRuleException();
    }

    return new Rule(this.body.get(0), Collections.singleton(this.head));
  }

  public Set<Variable> getVariablesInBody() {
    Set<Variable> variables = new HashSet<>();

    for (Atom atom : this.getBody()) {
      variables.addAll(atom.getVariables());
    }

    return variables;
  }

  public Set<Variable> getVariables() {
    Set<Variable> variables = this.getVariablesInBody();
    variables.addAll(this.head.getVariables());

    return variables;
  }

  public Substitution getBodySubstitution() {
    Substitution substitution = new Substitution();
    for (Variable var : this.getVariablesInBody()) {
      substitution.put(var, var);
    }

    return substitution;
  }

  public Substitution getSubstitution() {
    Substitution substitution = new Substitution();
    for (Variable var : this.getVariables()) {
      substitution.put(var, var);
    }

    return substitution;
  }

  public int getMaxProf() {
    int prof = -1;
    for (Atom a : this.body) {
      if (a.getMaxProf() > prof) {
        prof = a.getMaxProf();
      }
    }
    return prof;
  }

  private Set<Substitution> getAtomsSubstitutionsFromAtoms(Collection<Atom> atomsToInstantiate,
      Map<String, Collection<Atom>> instanciationAtoms) {
    Set<Substitution> substitutionCandidates = new HashSet<>();
    Set<Variable> bodyVariables = this.getVariablesInBody();

    for (Atom refAtom : atomsToInstantiate) {
      for (Atom candidateAtom : instanciationAtoms.get(refAtom.getName())) {
        Substitution substitution = this.getSubstitution();
        boolean compatible = true;

        for (int i = 0; i < candidateAtom.size(); i++) {
          if (!candidateAtom.get(i).isConstant()) {
            continue;
          }

          // Instantiate the variable with the atom
          if (refAtom.get(i).isVariable()) {
            if (bodyVariables.contains((Variable) refAtom.get(i))) {
              substitution.put((Variable) refAtom.get(i), candidateAtom.get(i));
            }

          } else if (!refAtom.get(i)
              .equals(candidateAtom.get(i))) {  // Verify that the atom is compatible
            compatible = false;
            break;
          }
        }

        if (compatible) {
          substitutionCandidates.add(substitution);
        }
      }
    }

    return substitutionCandidates;
  }

  public Set<Substitution> getBodySubstitutions(Bdd atoms) {
    Map<String, Collection<Atom>> instanciationAtoms = this.getBody().parallelStream()
        .map(Atom::getName).distinct().collect(
            Collectors.toMap(pred -> pred, atoms::getAtomsWithPredicate));

    if (this.bodyContainsOne(atoms.getAllPredicates())) {
      return this.getAtomsSubstitutionsFromAtoms(this.getBody(), instanciationAtoms);
    } else {
      return Collections.emptySet();
    }
  }

  public Set<Substitution> getHeadSubstitutions(Bdd atoms) {
    String headName = this.getHeadNames();
    return this.getAtomsSubstitutionsFromAtoms(List.of(this.getHead()),
        Map.of(headName, atoms.getAtomsWithPredicate(headName)));
  }
}
