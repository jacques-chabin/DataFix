package fr.lifo.updatechase.model.logic;

import fr.lifo.updatechase.model.SyntaxError;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p><b>Query represente une requete qui sera effectuee sur la Bdd.</b>
 * Une query herite de Rule.</p>
 *
 * @see Rule
 * @see Atom
 */
public class Query extends Rule {

  /*Variable utilisee pour le renommage des atomes.*/
  static int numNewVar = 1;

  /**
   * Constructeur de query. Ce constructeur fait uniquement appel au constructeur de Rule.
   *
   * @param h l'Atome qui constitue la partie head d'une Query.
   * @param b Une liste de tout les Atome qui constitueent la partie body d'une Query.
   */
  public Query(Atom h, List<Atom> b) {
    super(h, b);
  }

  /**
   * Verifie si un Atome est present dans une ArrayList d'Atome, sans prendre en compte la presence
   * du String "New" dans son nom.
   *
   * @param a     L'Atome recherche.
   * @param liste La liste d'Atome dans laquelle on effectue la recherche.
   * @return Un booleen qui indique si l'Atome est bien present dans la liste d'Atome.
   * @see Atom#equalsModuloNew
   */
  public static boolean containsNew(Atom a, List<Atom> liste) {
    // return true si a est dans la liste modulo le renomage des variables New reductibles
    //System.out.print("Test si "+a+ " est dans "+liste+" return ");
    for (Atom b : liste) {
      if (a.equalsModuloNew(b)) {
        //System.out.println("True");
        return true;
      }
    }
    //System.out.println("False");
    return false;
  }

  public static boolean update1WithOnePosConstraint1_1(Rule pc, int stopCond, List<Atom> bd,
      List<Atom> oldList, List<Atom> newList) {
    //System.out.println("Try to apply rule : " + pc +" on "+oldList);
    numNewVar++;
    pc = pc.renameVar(numNewVar);
    //System.out.println("numNewVar = " + numNewVar +"  "+ pc);
    List<Atom> res = new ArrayList<>();
    List<Atom> src = pc.getBody();

    boolean fini = false;
    boolean result = false;
    int lg = src.size();

    int[] tabPos = new int[lg];
    for (int j = 0; j < lg; ++j) {
      tabPos[j] = -1;
    }

    Substitution[] tabSub = new Substitution[lg];
    tabSub[0] = new Substitution();

    int i = 0;

    while (!fini) {
      //System.out.print(" "+i+ " tabi " + tabPos[i]);
      if (i == 0) {
        tabSub[i] = new Substitution();
      } else {
        tabSub[i] = (Substitution) tabSub[i - 1].clone();
      }

      tabPos[i] = src.get(i).mapAll(oldList, tabSub[i], tabPos[i], stopCond);
      //System.out.println(" "+src.get(i)+trg);
      //System.out.println(" apres mapList tabi :"+ tabPos[i]);
      //System.out.println(" apres mapList tabsi :"+ tabSub[i]);

      if (tabPos[i] == -1) {
        if (i == 0) {
          fini = true;
        } else {
          i = i - 1;
        }
      } else if (i == lg - 1) {
        Atom a = tabSub[i].applySub(pc.getHead());
        //System.out.println("Atome instancie : "+a);
        if (!containsNew(a, bd) && !containsNew(a, oldList) && !containsNew(a, res) && !containsNew(
            a, newList)) {
          //System.out.println("Test presence de false");
          result = true;
          //a.renameOldNewVar(res);
          res.add(a);

        }
      } else {
        i = i + 1;
      }
      if (fini) {
        if (res.size() != 0) {
          for (Atom b : res) {
            b.putRedOldNewVar(newList);
            newList.add(b);
          }
          fini = false;
          res = new ArrayList<>();
        }
      }

    }
    //System.out.println(result);
    return result;
  }

  public static Atom removeNotEqualPredicat(List<Atom> body) {
    // retourne et supprime l'atome NotEqual(..,..) du body s'il est present
    // retourne null sinon
    Atom res = null;
    int i = 0;
    boolean find = false;
    while (!find && i < body.size()) {
      if (body.get(i).getName().equals("NotEqual")) {
        find = true;
        res = body.get(i);
        body.remove(res);
      } else {
        i += 1;
      }
    }
    return res;
  }

  /**
   * Verifie si l'Atome passe en parametre estdans le <b>body</b> d'une Query.
   *
   * @param a L'Atome recherche.
   * @return boolean Un booleen qui indique si cet Atone est present dans la liste d'Atome du body.
   */
  public boolean contains(Atom a) {
    // return true is a is in body of the query
    for (Atom b : getBody()) {
      if (a.equals(b)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Verifie si un Atome est dans le body d'une Query sans prendre en compte la presence du String
   * "New".
   *
   * @param a Un Atome recherche.
   * @return Un booleen qui indique si l'Atome est bien present dans le body d'une Query.
   */
  public boolean containsNew(Atom a) {
    // return true is a is in body of the query modulo le renomage des variables New reductibles
    return containsNew(a, getBody());
  }

  /**
   * Effectue une completion en appliquant une regle.
   *
   * @param pc       La regle a appliquer.
   * @param stopCond L'Entier qui indique si les valeur nulles sont traites.
   * @return Une valeur booleenne qui indique si une completion a ete effectuee en suivant la regle.
   */
  public boolean completeWithOnePosConstraint(Rule pc, int stopCond) {
    //System.out.println("Try to apply rule : "+pc +" on "+getBody());
    numNewVar++;
    //if(numNewVar >=6)
    //	return false;
    pc = pc.renameVar(numNewVar);
    //System.out.println("numNewVar = " + numNewVar +"  "+ pc);
    ArrayList<Atom> res = new ArrayList<>();

    List<Atom> src = pc.getBody();
    List<Atom> trg = getBody();

    boolean fini = false;
    boolean result = false;
    int lg = src.size();

    int[] tabPos = new int[lg];
    for (int j = 0; j < lg; ++j) {
      tabPos[j] = -1;
    }

    Substitution[] tabSub = new Substitution[lg];
    tabSub[0] = new Substitution();

    int i = 0;

    while (!fini) {
      //System.out.print(" "+i+ " tabi " + tabPos[i]);
      if (i == 0) {
        tabSub[i] = new Substitution();
      } else {
        tabSub[i] = (Substitution) tabSub[i - 1].clone();
      }

      tabPos[i] = src.get(i).mapAll(trg, tabSub[i], tabPos[i], stopCond);
      //System.out.println(" "+src.get(i)+trg);
      //System.out.println(" apres mapList tabi :"+ tabPos[i]);
      //System.out.println(" apres mapList tabsi :"+ tabSub[i]);

      if (tabPos[i] == -1) {
        if (i == 0) {
          fini = true;
        } else {
          i = i - 1;
        }
      } else if (i == lg - 1) {
        Atom a = tabSub[i].applySub(pc.getHead());
        //System.out.println("Atome instancie : "+a);
        if (!containsNew(a) && !containsNew(a, res)) {
          //System.out.println("Test presence de "+a+" dans "+getBody());
          result = true;
          //a.renameOldNewVar(res);
          res.add(a);

        }
      } else {
        i = i + 1;
      }
      if (fini) {
        if (res.size() != 0) {
          for (Atom b : res) {
            b.putRedOldNewVar(body);
            body.add(b);
          }
          fini = false;
          res = new ArrayList<>();
        }
      }

    }
    //System.out.println(result);
    return result;
  }

  /**
   * Applique un ensemble de Rule pour effectuer une completion.
   *
   * @param posConstraintSet L'ensemble des Rule a appliquer.
   * @param stopCond         Un entier qui indique si les valeurs nulles sont traitees.
   */
  public void completeWithPosConstraint(LinkedListRules posConstraintSet, int stopCond) {
    boolean cont = true;
    int nbprof = 0;
    int nbAOld = 0;
    int nbA;
    while (cont) {
      nbA = getBody().size();
      //System.out.println("Completion prof : " + nbprof + " on "+nbA+ "atomes " + nbAOld +" new atomes");
      nbAOld = nbA;
      nbprof += 1;
      cont = false;
      for (Rule pc : posConstraintSet) {
        if (completeWithOnePosConstraint(pc, stopCond)) {
          cont = true;
          //System.out.println("One completion");
        }
        //System.out.println("num dep : "+i);
      }
      //System.out.println(" "+getBody());
    }

  }

  /**
   * Complete uniquement avec les Rule qui ont un predicat unaire.
   *
   * @param posConstraintSet L'Ensemble de regles a appliquer.
   * @param stopCond         Un entier qui indique si les valeurs nulles sont traitees.
   */
  public void completeWithSimplePosConstraint(LinkedListRules posConstraintSet, int stopCond) {
    boolean cont = true;
    Rule pc;
    while (cont) {
      cont = false;
      for (Rule rule : posConstraintSet) {
        pc = rule;
        if (pc.getHead().size() <= 1) {
          // on complete uniquement avec les PC qui ont un prédicat unaire en tete
          if (completeWithOnePosConstraint(pc, stopCond)) {
            cont = true;
          }
        }
      }
    }

  }

  public int update1(LinkedListRules posConstraintSet, int stopCond) {
    boolean cont = true;
    Rule pc;
    int nbprof = 0;
    int nbAOld = 0;
    int nbA = 0;
    List<Atom> oldList;
    List<Atom> newList = getBody();
    List<Atom> bd = new ArrayList<>();
    int nbAtomesAdd = 0;

    while (cont) {
      nbprof += 1;
      cont = false;
      oldList = newList;
      newList = new ArrayList<>();
      System.out.print(
          "bd size : " + bd.size() + "Completion prof : " + nbprof + " on " + oldList.size()
              + "atomes ");

      for (Rule rule : posConstraintSet) {
        pc = rule;
        if (update1WithOnePosConstraint1_1(pc, stopCond, bd, oldList, newList)) {
          cont = true;
          //System.out.println("One completion");
        }
        //System.out.println("num dep : "+i);
      }
      if (nbprof == 1) {
        bd = oldList;
      } else {
        nbAtomesAdd += oldList.size();
        bd.addAll(oldList);
      }
      //System.out.println(" produce : "+newList.size()+" atomes");
      //System.out.println(" "+getBody());
    }
    return nbAtomesAdd;

  }

  boolean checkOneNegConstraint(Rule nc) {
    // return false il there is an instanciation of the negative constraint nc in the body of the query
    List<Atom> src = nc.getBody();
    List<Atom> trg = getBody();

    boolean fini = false;
    boolean ok = true;
    int lg = src.size();

    int[] tabPos = new int[lg];
    for (int j = 0; j < lg; ++j) {
      tabPos[j] = -1;
    }

    Substitution[] tabSub = new Substitution[lg];
    tabSub[0] = new Substitution();

    int i = 0;

    while (!fini && ok) {
      //System.out.print(" "+i+ " tabi " + tabPos[i]);
      if (i == 0) {
        tabSub[i] = new Substitution();
      } else {
        tabSub[i] = (Substitution) tabSub[i - 1].clone();
      }

      tabPos[i] = src.get(i).mapAll(trg, tabSub[i], tabPos[i], 0);

      //System.out.println(" apres mapList tabi :"+ tabPos[i]);
      //System.out.println(" apres mapList tabsi :"+ tabSub[i]);

      if (tabPos[i] == -1) {
        if (i == 0) {
          fini = true;
        } else {
          i = i - 1;
        }
      } else if (i == lg - 1) {
        ok = false;
      } else {
        i = i + 1;
      }
    }
    return ok;
  }

  public int checkNegConstraint(LinkedListRules negConstraintSet) {
    // return i if there is an instanciation of a negative constraint number i, in the body of the query
    // else return -1
    boolean ok = true;
    Rule nc;
    int i = 0;
    int res = -1;
    while (ok && i < negConstraintSet.size()) {
      nc = negConstraintSet.get(i);
      ok = checkOneNegConstraint(nc);
      if (!ok) {
        res = i + 1;
      }
      i++;
    }
    return res;
  }

  public int algo1(LinkedListRules posConstraintSet, LinkedListRules negConstraintSet,
      int stopCond) {
    // complete the query with positive constraints and check negative constraint
    // return -1 if check is ok else return numbre of negative constraint which is conflicted.
    // if stopCond == -1 stop completion when nullvalue arrived.
    completeWithPosConstraint(posConstraintSet, stopCond);

    return checkNegConstraint(negConstraintSet);

  }

  public LinkedListRules algo2SimplePosConstraint(LinkedListRules posConstraintSet) {
    // return the list of queries to check for an answer take account positive constraints
    LinkedListRules res = new LinkedListRules();
    List<Atom> trg = getBody();
    List<Atom> src;

    for (Rule pc : posConstraintSet) {
      if (pc.getHead().size() > 1) {
        src = pc.getBody();
        for (Atom a : trg) {
          // pour tous les atomes de la réponses
          Substitution sub = new Substitution();
          int pos = a.mapAll2(src, sub, -1);
          while (pos != -1) {
            // on construit une nouvelle requette
            List<Atom> reqb = new ArrayList<>();
            Atom b = sub.applySub(pc.getHead());
            reqb.add(b);
            Atom reqh = new Atom("§");
            Rule req = new Query(reqh, reqb);
            res.add(req);

            sub = new Substitution();
            pos = a.mapAll2(src, sub, pos);
          }
        }
      }
    }

        /* if(QS.verbose > 1){
			System.out.println("Query to check for positive constraint " +res);
		} */
    return res;
  }

  public LinkedListRules algo2NegConstraint(LinkedListRules negConstraintSet) {
    // return the list of queries to check for an answer take account negative constraints
    LinkedListRules res = new LinkedListRules();
    List<Atom> trg = getBody();
    List<Atom> src;

    for (Rule nc : negConstraintSet) {
      src = nc.getBody();
      for (Atom a : trg) {
        // pour tous les atomes de la réponses
        Substitution sub = new Substitution();
        int pos = a.mapAll2(src, sub, -1);
        while (pos != -1) {
          // System.out.println("Avec la substitution : "+sub);
          // on construit une nouvelle requette
          List<Atom> reqb = new ArrayList<>();
          for (int j = 0; j < src.size(); ++j) {
            if (j != pos) {
              Atom b = sub.applySub(src.get(j));
              // if b is not in the instancied query
              if (!contains(b)) {
                reqb.add(b);
              }
            }
          }
          if (reqb.size() == 0) {
            reqb.add(new Atom("!"));
          }
          Atom reqh = new Atom("?");
          Rule req = new Query(reqh, reqb);
          // System.out.println("On fabrique la requete : "+req);
          res.add(req);

          sub = new Substitution();
          pos = a.mapAll2(src, sub, pos);
        }
      }
    }
    return res;
  }

  public LinkedListRules algo2KeyConstraint(LinkedListRules keyConstraintSet) {
    // return the list of queries to check for an answer take account key constraints
    LinkedListRules res = new LinkedListRules();
    List<Atom> trg = getBody();
    List<Atom> src;

    for (Rule kc : keyConstraintSet) {
      src = kc.getBody();
      for (Atom a : trg) {
        // pour tous les atomes de la réponses
        Substitution sub = new Substitution();
        int pos = a.mapAll2(src, sub, -1);

        //while(pos != -1){
        // on ne fait au plus qu'une sous requete
        if (pos != -1) {
          // System.out.println("Pos vaut : "+pos);
          // on construit une nouvelle requete
          List<Atom> reqb = new ArrayList<>();
          for (int j = 0; j < src.size(); ++j) {
            if (j != pos) {
              Atom b = sub.applySub(src.get(j));
              // if b is not in the instancied query
              if (!contains(b)) {
                reqb.add(b);
              }
            }
          }
          Atom c = new Atom("NotEqual", sub.applySub(kc.getHead()));
          reqb.add(c);
          List<Element> reqv = new ArrayList<>();
          for (Element e : c) {
            if (e.isVariable()) {
              reqv.add(e);
            }
          }
          Atom reqh = new Atom("?", reqv);
          Rule req = new Query(reqh, reqb);
          res.add(req);

          sub = new Substitution();
          //pos = a.mapList2(src, sub, pos);
        }
      }
    }
    return res;
  }

  public LinkedListRules algo2(LinkedListRules negConstraintSet, LinkedListRules keyConstraintSet,
      LinkedListRules posConstraintSet) {
    // return the list of queries to check for an answer
    LinkedListRules res = algo2NegConstraint(negConstraintSet);
    //LinkedListRules res1 = algo2SimplePosConstraint(posConstraintSet);
    //System.out.println("Req à vérifier avec negconstraint : " + res);
    LinkedListRules res2 = algo2KeyConstraint(keyConstraintSet);
    //System.out.println("Req à vérifier avec keyconstraint : " + res2);

    //res.addAll(res1);
    res.addAll(res2);
    return res;
  }

  public Query completeAllVariables() {
    // on crée une nouvelle requête qui a dans la tête toutes les variables du corps
    List<Element> nhead = new ArrayList<>();
    if (getHead().size() > 0) {
      nhead.addAll(getHead());
    }
    for (Atom a : getBody()) {
      for (Element e : a) {
        if (e.isVariable() && !nhead.contains(e)) {
          nhead.add(e);
        }
      }
    }

    Atom hq2 = new Atom(getHead().getName(), nhead);
    return new Query(hq2, getBody());
  }

  public String queryToSQL(Schema sch, boolean hive) {
    // attention toutes les variables du corps de la query doivent être aussi dans la tete
    String select = "SELECT ";
    if (!hive) {
      select += "SQL_NO_CACHE ";
    }
    if (hive) {
      select += "Count(";
    }
    StringBuilder from = new StringBuilder(" FROM ");
    StringBuilder where = new StringBuilder();

    int cptw = 0;
    List<Atom> bodyReq = getBody();
    Atom neq = removeNotEqualPredicat(bodyReq);

    // Pour faire une correspondance entre les variables et les attributs du schéma correspondant
    Map<Variable, Set<String>> ass = new HashMap<>();

    Atom rh = getHead();
    String pred, predok;
    int cpt = 0;
    if (rh != null) {
      for (Element e : rh) {
        if (e.isVariable()) {
          predok = null;
          int nba = 0;
          for (Atom a : bodyReq) {
            int pos = a.findElement(e, -1);
            while (pos != -1) {
              try {
                pred = sch.getAttributName(a.getName(), pos);
                if (!ass.containsKey(e)) {
                  ass.put((Variable) e, new HashSet<>());
                }
                ass.get(e).add(a.getName() + nba + "." + pred);
                if (predok == null) {
                  predok = a.getName() + nba + "." + pred;
                }
              } catch (SyntaxError se) {
                System.out.println(se.getMessage());
                return null;
              }
              pos = a.findElement(e, pos);
            }
            nba += 1;
          }
          if (cpt > 0) {
            select += ", ";
          }
          select += predok;
          cpt += 1;
        } else {
          System.out.println("head of query :" + rh + "is not well formed");
          return null;
        }
      }
    }

    if (cpt == 0) {
      // it is a boolean query
      select += "* ";  // avant j'avais mis count(*)
    }
    if (hive) {
      select += ")";
    }

    // from part
    cpt = 0;
    for (Atom a : bodyReq) {
      if (cpt > 0) {
        from.append(", ");
      }
      from.append(a.getName()).append(" AS ").append(a.getName()).append(cpt);
      cpt += 1;
    }

    // si dans le corps de la requete il y a des variables qui ne sont pas dans la tete
    // on va completer ass
    for (int i = 0; i < bodyReq.size(); ++i) {
      Atom a = bodyReq.get(i);
      for (Element e : a) {
        if (e.isVariable()) {
          //System.out.println("Recherche de "+e+ " dans "+getHead());
          if (getHead() == null || !getHead().contains(e)) {
            // c'est une nouvelle variable, il faut l'ajouter à ass
            for (int j = i; j < bodyReq.size(); ++j) {
              Atom b = bodyReq.get(j);
              int pos = b.findElement(e, -1);
              //System.out.println("Atome b : "+b+ " pos = "+pos);
              while (pos != -1) {
                try {
                  pred = sch.getAttributName(b.getName(), pos);
                  if (!ass.containsKey(e)) {
                    ass.put((Variable) e, new HashSet<>());
                  }
                  ass.get(e).add(a.getName() + j + "." + pred);

                } catch (SyntaxError se) {
                  System.out.println(se.getMessage());
                  return null;
                }
                pos = b.findElement(e, pos);
              }
            }
          }
        }
      }
    }

    //System.out.println("Dans la transformation le hashmap de toutes les variables est : "+ass);
    //System.out.println("Nomalement rien : ");

    // where part
    // pour chaque variable si elle est associée à plusieurs attributs on ajoute une condition where
    for (Set<String> s : ass.values()) {
      String prem = null;
      if (s.size() > 1) {
        for (String pr : s) {
          if (prem == null) {
            prem = pr;
          } else {
            if (cptw == 0) {
              where = new StringBuilder(" WHERE ");
            } else {
              where.append(" And ");
            }
            where.append(prem).append(" = ").append(pr).append(" ");
            cptw += 1;
          }
        }
      }
    }

    // si dans le corps il y a des constantes ajouter les dans le where
    int cpta = -1;
    for (Atom a : bodyReq) {
      cpta++;
      for (int i = 0; i < a.size(); ++i) {
        if (a.get(i).isConstant()) {
          if (cptw == 0) {
            where = new StringBuilder(" WHERE ");
          } else {
            where.append(" And ");
          }
          try {
            where.append(a.getName()).append(cpta).append(".")
                .append(sch.getAttributName(a.getName(), i)).append(
                    " = '").append(a.get(i).getName()).append("' ");
          } catch (SyntaxError e) {
            System.out.println(e.getMessage());
            return null;
          }
          cptw += 1;
        }
      }
    }

    // s'il y avait un atome NotEqual, on complete le where
    if (neq != null) {
      Element e1 = neq.get(0);
      Element e2 = neq.get(1);
      String s1;
      String s2;
      int v = 0;
      if (e1.isVariable()) {
        v = 1;
        // on récupère l'attribut associé à cette variable
        s1 = (String) ass.get(e1).toArray()[0];
      } else {
        s1 = "'" + e1.getName() + "'";
      }
      if (e2.isVariable()) {
        v = 2;
        // on récupère l'attribut associé à cette variable
        s2 = (String) ass.get(e2).toArray()[0];
      } else {
        s2 = "'" + e2.getName() + "'";
      }
      if (cptw == 0) {
        where = new StringBuilder(" WHERE ");
      } else {
        where.append(" And ");
      }
      if (v == 2) {
        where.append(s2).append("<>").append(s1).append(" ");
      } else {
        where.append(s1).append("<>").append(s2).append(" ");
      }
      cptw += 1;

      //on va remettre neq dans le body de la requete
      bodyReq.add(neq);
    }

    return select + from + where;
  }

  public String queryToSparql(Schema schSparql) {
		/*"SELECT ?author ?birthplace ?artwork ?museum ?ville ?pays WHERE { "
				+ " ?author rdf:type dbo:Artist."
				+ " ?artwork dbo:author ?author."
				+ " ?author dbo:birthPlace ?birthplace."
				+ " ?birthplace dbo:country ?pays."
				+ " ?artwork rdf:type dbo:Artwork."
				+ " ?artwork dbo:museum ?museum."
				+ " ?museum rdf:type dbo:Museum."
				+ " ?museum dbo:location ?ville."
				+ " ?ville dbo:country ?pays."
				+ " }";*/
    StringBuilder select = new StringBuilder("SELECT ");
    StringBuilder where = new StringBuilder(" WHERE { ");
    Atom rh = getHead();
    String pred, var1, var2, predsp;
    int cpt = 0;
    if (rh != null) {
      for (Element e : rh) {
        if (e.isVariable()) {
          select.append("?").append(e.getName()).append(" ");
        }
      }
    }
    for (Atom a : getBody()) {
      pred = a.getName();
      try {
        predsp = schSparql.getAttributName(pred, 0);
      } catch (SyntaxError se) {
        System.out.println(se.getMessage());
        return null;
      }
      var1 = a.get(0).getName();
      if (a.size() == 2) {
        var2 = a.get(1).getName();
        where.append("?").append(var1).append(" ").append(predsp).append(" ?").append(var2)
            .append(". ");
      } else {
        where.append("?").append(var1).append(" rdf:type ").append(predsp).append(". ");
      }
    }
    where.append("}");

    return select + where.toString();
  }

  public String queryBQToSparql(Schema schSparql) {
		/*ASK {
			<http://dbpedia.org/resource/Filippo_Lippi> rdf:type dbo:Artist.
			<http://dbpedia.org/resource/Filippo_Lippi> rdf:type dbo:Policiant.
			} */
    StringBuilder res = new StringBuilder("ASK { ");
    String pred, predsp, var1;
    for (Atom a : getBody()) {
      pred = a.getName();
      try {
        predsp = schSparql.getAttributName(pred, 0);
      } catch (SyntaxError se) {
        System.out.println(se.getMessage());
        return null;
      }
      var1 = a.get(0).getName();
      res.append("<").append(var1).append("> rdf:type ").append(predsp).append(". ");
    }
    res.append("}");

    return res.toString();
  }

  public String queryBQToSparqlBis(Schema schSparql) {
		/*ASK {
			<http://dbpedia.org/resource/Filippo_Lippi> rdf:type dbo:Artist.
			<http://dbpedia.org/resource/Filippo_Lippi> rdf:type dbo:Policiant.
			} */
    StringBuilder res = new StringBuilder("SELECT count(*) WHERE { ");
    String pred, predsp, var1;
    for (Atom a : getBody()) {
      pred = a.getName();
      try {
        predsp = schSparql.getAttributName(pred, 0);
      } catch (SyntaxError se) {
        System.out.println(se.getMessage());
        return null;
      }
      var1 = a.get(0).getName();
      res.append("<").append(var1).append("> rdf:type ").append(predsp).append(". ");
    }
    res.append("}");

    return res.toString();
  }

  public boolean isBQ() {
    return getHead() == null;
  }

  public String queryCountToSparql(Schema schSparql) {
		/* select  count(distinct ?Yb)
			where {
					?Yb dbo:author <http://dbpedia.org/resource/Albrecht_Dürer>.
					#FILTER (  str(?Yb) < "http://dbpedia.org/resource/Self-Portrait_(Dürer,_Munich)" || str(?Yb) > "http://dbpedia.org/resource/Self-Portrait_(Dürer,_Munich)" )
			} la comparaison des chaines ne fonctionne pas bien!*/
    StringBuilder select = new StringBuilder("SELECT COUNT(");
    StringBuilder where = new StringBuilder(" WHERE { ");
    Atom rh = getHead();
    String pred, var1, var2, predsp;
    int cpt = 0;
    if (rh != null) {
      for (Element e : rh) {
        cpt += 1;
        if (e.isVariable()) {
          select.append("?").append(e.getName()).append(" ");
        }
      }
    }
    if (cpt != 1) {
      System.out.println("Syntax error with count query");
    }
    select.append(") ");
    for (Atom a : getBody()) {
      pred = a.getName();
      if (!pred.equals("NotEqual")) {
        try {
          predsp = schSparql.getAttributName(pred, 0);
        } catch (SyntaxError se) {
          System.out.println(se.getMessage());
          return null;
        }
        var1 = a.get(0).getName();
        if (a.size() == 2) {
          var2 = a.get(1).getName();
          if (a.get(0).isVariable()) {
            where.append("?").append(var1).append(" ").append(predsp).append(" <").append(var2)
                .append(">. ");
          } else {
            where.append("<").append(var1).append("> ").append(predsp).append(" ?").append(var2)
                .append(". ");
          }
        } else {
          where.append("?").append(var1).append(" rdf:type ").append(predsp).append(". ");
        }
      }
    }
    where.append("}");

    return select + where.toString();
  }

}

