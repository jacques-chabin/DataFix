package fr.lifo.updatechase.model.logic;

/**
 * L'objet Constant represente un Element constant. Il herite de Element.
 *
 * @see Element
 */
public class Constant extends Element {

  /**
   * Constructeur de Constant. Fait uniquement appel au constructeur de Element.
   *
   * @param name The constant value
   */
  public Constant(final String name) {
    super(name, Element.CONSTANT);
  }

  @Override
  protected void setType(final byte type) throws IllegalArgumentException {
    if (type != Element.CONSTANT) {
      throw new IllegalArgumentException();
    }

    super.setType(type);
  }

  @Override
  public Object clone() {
    return new Constant(this.getName());
  }
}
