package fr.lifo.updatechase.model.logic;

/**
 * <b>AtomeEqual etend la classe Atome. C'est une classe qui represente un membre du corps ou de la
 * tete d'une regle.</b> (??? que de la tete ?) Un AtomeEqual se comporte comme un atome normal.
 * Mais il sera traite differemment d'un Atome normal dans l'algorithme chase.
 *
 * <p>Un Atome est caracterise par :
 * <ul><li>name : le nom de l'atome.</li>
 * <li>tuple : un ArrayList d'Element</li></ul></p>
 *
 * @see Element
 * @see Atom
 * @see Constant
 * @see Variable
 */
public class AtomEqual extends Atom {

  public AtomEqual(String s1, String s2) {
    super("=", new Element[2]);

    if (s1.charAt(0) >= 'A' && s1.charAt(0) <= 'Z') {
      this.set(0, new Variable(s1));
    } else {
      this.set(0, new Constant(s1));
    }

    if (s2.charAt(0) >= 'A' && s2.charAt(0) <= 'Z') {
      this.set(1, new Variable(s2));
    } else {
      this.set(1, new Constant(s2));
    }
  }

  public String toString() {
    return this.get(0) + " = " + this.get(1);
  }
}
