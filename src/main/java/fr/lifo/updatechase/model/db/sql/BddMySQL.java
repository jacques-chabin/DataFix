package fr.lifo.updatechase.model.db.sql;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Substitution;
import fr.lifo.updatechase.model.logic.Variable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BddMySQL extends Bdd {

  private Connection mysql;
  private String db;
  private int nbQueries;

  /**
   * Constructeur de BddMySQL. Si le chargement du driver ou la connexion echoue, l'attribut mysql
   * est mis à null.
   * TODO: add Schema for table/column names
   *
   * @param db    le nom de la base de donne à laquelle on veut se connecte.
   * @param login le login de connexion au serveur.
   * @param mdp   le mot de passe de connexion au serveur.
   */
  public BddMySQL(String host, int port, String db, String login, String mdp) {
    this.nbQueries = 0;

    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      LOGGER.severe("Driver MySQL non trouvé?");
      this.mysql = null;
      return;
    }

    try {
      this.db = db;
      String url = "jdbc:mysql://%s:%d/%s?useUnicode=true&characterEncoding=UTF-8&rewriteBatchedStatements=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Paris";
      this.mysql = DriverManager.getConnection(String.format(url, host, port, this.db), login, mdp);

    } catch (SQLException e) {
      LOGGER.severe("Echec de connexion!" + e.getMessage());
      this.mysql = null;
    }
  }

  /**
   * Getteur permet de savoir si on est connecté
   */
  public boolean isConnected() {
    try {
      return this.mysql != null && !this.mysql.isClosed();
    } catch (SQLException exception) {
      exception.printStackTrace();
    }
    return false;
  }

  /**
   * Getteur permet d'obtenir la connexion
   */
  public Connection getConnexion() {
    return this.mysql;
  }

  private static Set<Atom> resultSetToAtom(String name, ResultSet resultSet) throws SQLException {
    int nbColumns = resultSet.getMetaData().getColumnCount();
    Element[] elements = new Element[nbColumns];
    Set<Atom> result = new HashSet<>();

    while (resultSet.next()) {

      for (int i = 0; i < nbColumns; i++) {
        elements[i] = Element.stringToElement(resultSet.getString(i + 1));
      }

      Atom a = new Atom(name, elements);
      result.add(a);
    }

    return result;
  }

  /**
   * Clears the database by deleting everything
   */
  @Override
  public void clear() {
    // Use concatenation as table name can't be parametrized, but still protect it with quotes
    String query = "DROP TABLE `%s`";

    try (Statement statement = this.mysql.createStatement()) {

      for (String table : this.getAllPredicates()) {
        statement.addBatch(String.format(query, table));
      }

      statement.executeBatch();
      this.nbQueries++;

    } catch (SQLException e) {
      LOGGER.severe("Cannot clear database: " + e.getMessage());
    }
  }

  @Override
  public Iterator<Atom> iterator() {
    return this.getAllPredicates().stream().flatMap(x -> this.getAtomsWithPredicate(x).stream())
        .iterator();
  }

  /**
   * Add a table in the DB if it does not exist
   *
   * @param name  the name of the table
   * @param arity the number of field of the table
   */
  private void addTable(String name, int arity) {
    if (this.getAllPredicates().contains(name)) {
      return;
    }

    // Use concatenation as table name can't be parametrized, but still protect it with quotes
    StringBuilder query = new StringBuilder("CREATE TABLE `" + name + "`(ch1 varchar(50)");
    StringBuilder primaryKey = new StringBuilder("ch1");
    for (int i = 2; i <= arity; ++i) {
      query.append(", ch").append(i).append(" varchar(50)");
      primaryKey.append(", ch").append(i);
    }
    query.append(", PRIMARY KEY (").append(primaryKey).append("))");

    try (Statement statement = this.mysql.createStatement()) {
      statement.executeUpdate(query.toString());
      this.nbQueries++;

    } catch (SQLException e) {
      LOGGER.severe("Cannot add table: " + e.getMessage());
    }
  }

  @Override
  public Collection<String> getAllPredicates() {
    String query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ?";
    Set<String> tables = new HashSet<>();

    try (PreparedStatement statement = this.mysql.prepareStatement(query)) {
      statement.setString(1, this.db);
      ResultSet resultSet = statement.executeQuery();
      this.nbQueries++;

      while (resultSet.next()) {
        tables.add(resultSet.getString(1));
      }

    } catch (SQLException e) {
      LOGGER.severe("Cannot get predicates: " + e.getMessage());
    }

    return tables;
  }

  @Override
  public Collection<Atom> getAtomsWithPredicate(String pred) {
    if (!this.getAllPredicates().contains(pred)) {
      return Collections.emptySet();
    }

    // Use concatenation as table name can't be parametrized, but still protect it with quotes
    String query = "SELECT * FROM `%s`";
    try (Statement statement = this.mysql.createStatement()) {
      ResultSet resultSet = statement.executeQuery(String.format(query, pred));
      this.nbQueries++;
      return resultSetToAtom(pred, resultSet);

    } catch (SQLException e) {
      LOGGER.severe("Cannot get atoms for predicate: " + e.getMessage());
    }

    return null;
  }

  /**
   * Ajoute a la bdd l'Atome a.
   *
   * @param a l'Atome a ajoute
   */
  @Override
  public boolean add(Atom a) {
    return this.addAll(Collections.singleton(a));
  }

  @Override
  public boolean addAll(Collection<? extends Atom> c) {
    if (c.isEmpty()) {
      return false;
    }

    Collection<String> predicates = this.getAllPredicates();
    Bdd bdd = new BddMemoire(c);
    int totalUpdate = 0;

    for (String pred : bdd.getAllPredicates()) {
      Collection<Atom> atoms = bdd.getAtomsWithPredicate(pred);
      StringBuilder query = new StringBuilder("INSERT IGNORE INTO `" + pred + "` values (?");

      // Get atom arity
      int arity = 0;
      for (Atom a : atoms) {
        arity = a.size();
        break;
      }

      // Add table if not exist
      if (!predicates.contains(pred)) {
        this.addTable(pred, arity);
      }

      // Complete query
      query.append(", ?".repeat(Math.max(0, arity - 1))).append(')');

      // Execute batch query
      try (PreparedStatement statement = this.mysql.prepareStatement(query.toString())) {
        for (Atom a : atoms) {
          for (int i = 0; i < a.size(); i++) {
            String elem = a.get(i).getName();
            if (a.get(i).isNullValue()) {
              elem = getNullName((Variable) a.get(i));
            }
            statement.setString(i + 1, elem);
          }
          statement.addBatch();
        }

        int[] result = statement.executeBatch();
        totalUpdate += result.length;
        this.nbQueries++;

      } catch (SQLException e) {
        LOGGER.severe("Cannot insert atoms: " + e.getMessage());
      }
    }

    return totalUpdate > 0;
  }

  /**
   * Return all atom in db which are isomorphic to atom a
   *
   * @param a      An atom we want to suppresse from the database
   * @param delete is true atom is suppressed from database is false atom is kept in database
   */
  @Override
  public Set<Atom> isomorphicAtom(Atom a, boolean delete) {
    Set<Atom> toDel = new HashSet<>();
    String r1 = "SELECT * ";
    String r2 = "DELETE ";
    String cond = atomeModuloNullToSQLString(a);
    r1 += cond;
    r2 += cond;
    try {
      Statement s = this.mysql.createStatement();
      ResultSet rs = s.executeQuery(r1);
      this.nbQueries++;
      ResultSetMetaData rsmd = rs.getMetaData();
      int ar = rsmd.getColumnCount();
      while (rs.next()) {
        StringBuilder latome = new StringBuilder(a.getName() + "(" + rs.getString(1));
        for (int i = 2; i <= ar; ++i) {
          latome.append(", ").append(rs.getString(i));
        }
        latome.append(")");
        Atom atom;
        try {
          atom = Atom.stringToAtom(latome.toString());
          toDel.add(atom);
        } catch (SyntaxError se) {
          LOGGER.severe("Cannot read atom from string: " + se.getMessage());
        }
      }
    } catch (SQLException e) {
      LOGGER.severe("Cannot search iso atoms: " + e.getMessage());
    }

    if (delete) {
      // suppression des atomes de la BD
      try {
        Statement s = this.mysql.createStatement();
        s.executeUpdate(r2);
        this.nbQueries++;
      } catch (SQLException e) {
        LOGGER.severe("Cannot delete iso atoms: " + e.getMessage());
      }
    }
    return toDel;
  }

  @Override
  public boolean remove(Object o) {
    return this.removeAll(Collections.singleton(o));
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    if (c.isEmpty() || c.stream().anyMatch(o -> !(o instanceof Atom))) {
      return false;
    }

    Bdd bdd;
    int totalUpdate = 0;

    if (c instanceof Bdd) {
      bdd = (Bdd) c;
    } else {
      bdd = new BddMemoire();
      bdd.addAll((Collection<Atom>) c);
    }

    for (String pred : bdd.getAllPredicates()) {
      Collection<Atom> atoms = bdd.getAtomsWithPredicate(pred);
      StringBuilder query = new StringBuilder("DELETE FROM `" + pred + "` WHERE ch1 = ?");

      // Get atom arity
      int arity = 0;
      for (Atom a : atoms) {
        arity = a.size();
        break;
      }

      // Complete query
      for (int i = 2; i <= arity; i++) {
        query.append("AND ch").append(i).append(" = ?");
      }

      // Execute batch query
      try (PreparedStatement statement = this.mysql.prepareStatement(query.toString())) {
        for (Atom a : atoms) {
          for (int i = 0; i < a.size(); i++) {
            String elem = a.get(i).getName();
            if (a.get(i).isNullValue()) {
              elem = getNullName((Variable) a.get(i));
            }
            statement.setString(i + 1, elem);
          }
          statement.addBatch();
        }

        int[] result = statement.executeBatch();
        totalUpdate += result.length;
        this.nbQueries++;

      } catch (SQLException e) {
        LOGGER.severe("Cannot remove atoms: " + e.getMessage());
      }
    }

    return totalUpdate > 0;
  }

  /**
   * Exécute la requete MySQL req
   *
   * @param req String texte de la rerquete
   * @return boolean true si on obtient au moins une réponse False sinon
   */
  private boolean executeSQLBQuery(String req) {
    boolean res = false;

    try (Statement statement = this.mysql.createStatement()) {
      ResultSet resultSet = statement.executeQuery(req);
      this.nbQueries++;

      res = resultSet.next();

    } catch (SQLException e) {
      LOGGER.severe("Pb11 " + e.getMessage());
    }

    return res;
  }

  /**
   * Exécute la requete MySQL req
   *
   * @param req String texte de la rerquete
   * @return Set<Atome> l'ensemble des atomes réponse de la requête
   */
  private Set<Atom> executeSQLQuery(String req, String pred) {
    try (Statement statement = this.mysql.createStatement()) {
      ResultSet resultSet = statement.executeQuery(req);
      this.nbQueries++;

      return resultSetToAtom(pred, resultSet);

    } catch (SQLException e) {
      LOGGER.severe("Pb12 " + e.getMessage());
    }

    return new HashSet<>();
  }

  @Override
  public boolean contains(Object o) {
    if (o instanceof Atom) {
      String r1 = "SELECT * " + atomeModuloNullToSQLString((Atom) o);
      this.nbQueries++;
      return executeSQLBQuery(r1);
    }

    return false;
  }

  @Override
  public boolean isEmpty() {
    boolean res = true;
    String query = "SELECT 1 FROM `%s`";

    try (Statement statement = this.mysql.createStatement()) {

      for (String table : this.getAllPredicates()) {
        ResultSet resultSet = statement.executeQuery(String.format(query, table));
        this.nbQueries++;

        if (resultSet.next()) {
          res = false;
          break;
        }
      }

    } catch (SQLException e) {
      LOGGER.severe("Can't check if DB is empty: " + e.getMessage());
    }

    return res;
  }

  /**
   * Retourne le nombre d'Atomes de la Bdd.
   *
   * @return int le nombre d'Atomes de la bdd.
   */
  @Override
  public int size() {
    int count = 0;
    String query = "SELECT count(*) FROM `%s`";

    try (Statement statement = this.mysql.createStatement()) {

      for (String table : this.getAllPredicates()) {
        ResultSet resultSet = statement.executeQuery(String.format(query, table));
        this.nbQueries++;

        if (resultSet.next()) {
          count += resultSet.getInt(1);
        }
      }

    } catch (SQLException e) {
      LOGGER.severe("Cant get DB size: " + e.getMessage());
    }

    return count;
  }

  /**
   * transform atome A(c1,c2,...,cn) modulo null values in String  "from A where "= - "ch1='c1'" if
   * c1 is a constante - "ch1 like '_%" if c1 is a null value - "chi = chj" if ci and cj are the
   * same null value.
   *
   * @param a Atome. Atome to transform
   * @return String
   */
  private String atomeModuloNullToSQLString(Atom a) {
    HashMap<String, HashSet<Integer>> dico = new HashMap<>();
    StringBuilder r1 = new StringBuilder(String.format(" FROM `%s` ", a.getName()));
    int ind = 0;
    for (Element elt : a) {
      ++ind;
      if (ind == 1) {
        r1.append(" WHERE ");
      } else {
        r1.append(" AND ");
      }
      if (elt.isNullValue()) {
        String eltName = getNullName((Variable) elt);
        r1.append("ch").append(ind).append(" LIKE '\\_%'");
        if (dico.containsKey(eltName)) {
          for (int i : dico.get(eltName)) {
            r1.append(" AND ch").append(i).append(" = ch").append(ind).append(" ");
          }
        } else {
          dico.put(eltName, new HashSet<>());
        }
        dico.get(eltName).add(ind);
      } else {
        r1.append("ch").append(ind).append(" = '").append(elt.getName()).append("'");
      }
    }
    return r1.toString();
  }

  /**
   * transform atome A(c1,c2,...,cn) where null are considered as variables in String  "from A where
   * "= - "ch1='c1'" if c1 is a constante - "chi = chj" if ci and cj are the same null value.
   *
   * @param a Atome. Atome to transform
   * @return String
   */
  private String atomeVarNullToSQLString(Atom a) {
    HashMap<String, HashSet<Integer>> dicovn = new HashMap<>();
    StringBuilder r1 = new StringBuilder(String.format(" SELECT * FROM `%s`", a.getName()));
    int ind = 0;
    int cpt = 0;
    for (Element elt : a) {
      ++ind;
      if (elt.isConstant()) {
        cpt++;
        if (cpt == 1) {
          r1.append(" WHERE ");
        } else {
          r1.append(" AND ");
        }
        r1.append("ch").append(ind).append(" = '").append(elt.getName()).append("'");
      } else {
        String eltName = getNullName((Variable) elt);
        // le dico contient les var et les Null
        if (dicovn.containsKey(eltName)) {
          for (int i : dicovn.get(eltName)) {
            cpt++;
            if (cpt == 1) {
              r1.append(" WHERE ");
            } else {
              r1.append(" AND ");
            }
            r1.append("ch").append(i).append(" = ch").append(ind).append(" ");
          }
        } else {
          dicovn.put(eltName, new HashSet<>());
        }
        dicovn.get(eltName).add(ind);
      }
    }
    return r1.toString();
  }

  /**
   * Méthode qui verifie si l'Atome 'a' est dans la Bdd sans prendre en compte les valeurs nulles.
   *
   * @param a Atome. l'Atome recherche.
   * @return boolean retourne un booleen indiquant si l'Atome a est present dans la Bdd.
   */
  @Override
  public boolean containsModuloNull(Atom a) {
    String r1 = "SELECT * " + atomeModuloNullToSQLString(a);
    this.nbQueries++;
    return executeSQLBQuery(r1);
  }

  @Override
  protected Map<Set<Element>, Set<Atom>> getPartitions(Collection<Element> nullBucket) {
    Map<Set<Element>, Set<Atom>> partition = new HashMap<>();
    Collection<String> tables = this.getAllPredicates();
    Collection<Element> traited = new HashSet<>();
    Collection<Element> trait = nullBucket;
    while (!trait.isEmpty()) {
      StringBuilder cond = new StringBuilder("(");
      int cpt = 0;
      for (Element elt : trait) {
        cpt++;
        if (cpt > 1) {
          cond.append(", ");
        }
        cond.append("'_").append(elt.getName()).append('#').append(((Variable) elt).getProf())
            .append("'");
      }
      cond.append(")");
      traited.addAll(trait);
      trait = new HashSet<>();
      for (String table : tables) {
        int ar = arityTable(table);
        StringBuilder r1 = new StringBuilder(
            String.format("SELECT * FROM `%s` WHERE ch1 IN %s", table, cond));
        for (int i = 2; i <= ar; ++i) {
          r1.append(" OR ch").append(i).append(" IN ").append(cond);
        }
        Set<Atom> resR1 = executeSQLQuery(r1.toString(), table);
        this.nbQueries++;
        for (Atom a : resR1) {
          Set<Set<Element>> keysToMerge = new HashSet<>();

          // Search linked partitions
          for (Element ti : a.getNulls()) {
            if (!traited.contains(ti)) {
              trait.add(ti);
            }
            for (Set<Element> k : partition.keySet()) {
              if (k.contains(ti)) {
                keysToMerge.add(k);
              }
            }
          }

          // Merge partitions
          Set<Element> newKey = new HashSet<>(a.getNulls());
          Set<Atom> newPartition = new HashSet<>();
          newPartition.add(a);

          for (Set<Element> oldKey : keysToMerge) {
            newKey.addAll(oldKey);
            newPartition.addAll(partition.get(oldKey));
            partition.remove(oldKey);
          }

          partition.put(newKey, newPartition);
        }
      }
    }

    return partition;
  }

  @Override
  protected Stream<Substitution> getPartitionSubstitions(Collection<Atom> partition) {
    String query = getSubstitutionQuery(partition);
    LOGGER.info("req dans getPartitionSubstitions: " + query);

    Set<Variable> nullsElements = partition.stream().flatMap(a -> a.getNulls().stream())
        .collect(Collectors.toSet());

    try {
      Statement statement = this.mysql.createStatement(ResultSet.TYPE_FORWARD_ONLY,
          ResultSet.CONCUR_READ_ONLY);
      statement.setFetchSize(Integer.MIN_VALUE);
      ResultSet rs = statement.executeQuery(query);
      this.nbQueries++;
      if (rs.next()) {
        return Stream.iterate(rs, resultSet -> {
          try {
            return !resultSet.isAfterLast();
          } catch (SQLException e) {
            e.printStackTrace();
          }
          return false;
        }, resultSet -> {
          try {
            resultSet.next();
          } catch (SQLException e) {
            e.printStackTrace();
          }
          return resultSet;
        }).map((ResultSet resultSet) -> {
          Substitution sub = new Substitution();
          try {
            for (Variable nullElement : nullsElements) {
              String elementName = resultSet.getString(nullElement.getName());
              Element element = Element.stringToElement(elementName);
              sub.put(nullElement, element);
            }
          } catch (SQLException e) {
            e.printStackTrace();
          }
          return sub;
        }).onClose(() -> {
          try {
            rs.close();
            statement.close();
          } catch (SQLException e) {
            e.printStackTrace();
          }
        });
      } else {
        rs.close();
        statement.close();
        return Stream.empty();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return Stream.empty();
  }

  /**
   * transform list<Atomes>  A1(c1,c2,...,cn)A2(...)... where null are considered as variables in
   * String  "select * from A1,A2,... where "= - "Ai.chik='cik'" if ci is a constante - "Ai.chik =
   * Aj.chjl" if cik and cjl are the same null value.
   *
   * @param la List<Atome>. List of atomes to transform
   * @return String
   */
  private String getSubstitutionQuery(Collection<Atom> la) {
    Map<String, Integer> dicovn = new HashMap<>();
    Map<String, String> dicovn2 = new HashMap<>();
    int indP = 0;
    int indW = 0;
    String pred;
    StringBuilder select = new StringBuilder("SELECT true");
    StringBuilder from = new StringBuilder(" FROM ");
    StringBuilder where = new StringBuilder(" WHERE ");
    for (Atom a : la) {
      indP++;
      pred = "Pred" + indP;
      if (indP > 1) {
        from.append(", ");
      }
      from.append('`').append(a.getName()).append("` AS ").append(pred).append(" ");
      int indC = 0;
      for (Element elt : a) {
        ++indC;
        if (elt.isConstant()) {
          if (indW >= 1) {
            where.append(" AND ");
          }
          where.append(pred).append(".ch").append(indC).append(" = '").append(elt.getName())
              .append("'");
          ++indW;
        } else {
          String eltName = elt.getName();
          // c'est une var null si elle est dans le dico des var nulles
          if (dicovn.containsKey(eltName)) {
            if (indW >= 1) {
              where.append(" AND ");
            }
            int i = dicovn.get(eltName);
            String predi = dicovn2.get(eltName);
            where.append(pred).append(".ch").append(indC).append(" = ").append(predi).append(".ch")
                .append(i);
            ++indW;
          } else {
            dicovn.put(eltName, indC);
            dicovn2.put(eltName, pred);
            select.append(", `").append(pred).append("`.ch").append(indC).append(" AS `")
                .append(eltName).append('`');
          }
        }
      }
    }

    if (indW > 0) {
      from.append(where);
    }

    return select.append(from).toString();
  }

  @Override
  protected Set<Element> getNullBucket(Bdd atoms) {
    Set<Element> nullBucket = atoms.getAllPredicates().parallelStream()
        .map(this::getAtomsWithPredicate).flatMap(
            Collection::parallelStream).flatMap(Atom::stream).filter(Element::isNullValue)
        .collect(Collectors.toSet());

    nullBucket.addAll(atoms.getNulls());
    return nullBucket;
  }

  @Override
  public void setNullsDegree(Collection<Element> nulls, int degree) {
    for (String table : this.getAllPredicates()) {
      for (String column : this.tableColumns(table)) {
        String query = "UPDATE `" + table + "` SET " + column + " = ? WHERE " + column + " = ?";

        try (PreparedStatement statement = this.mysql.prepareStatement(query)) {
          for (Element element : nulls) {
            Variable renamed = (Variable) element.clone();
            renamed.setProf(degree);

            statement.setString(2, getNullName((Variable) element));
            statement.setString(1, getNullName(renamed));

            statement.addBatch();
          }

          statement.executeBatch();
          this.nbQueries++;

        } catch (SQLException e) {
          System.err.println(e.getMessage());
        }
      }
    }
  }

  /**
   * Close the connection with a distant database
   */
  @Override
  public void close() {
    try {
      this.mysql.close();
    } catch (SQLException e) {
      LOGGER.severe("Pb16 close connexion" + e.getMessage());
    }
  }

  /**
   * Find the arity of a table in mySql
   *
   * @param table the name of the table
   * @return int the number of columns of table
   */
  private int arityTable(String table) {
    int res = 0;
    String query = "SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ?";

    try (PreparedStatement statement = this.mysql.prepareStatement(query)) {
      statement.setString(1, table);
      ResultSet resultSet = statement.executeQuery();
      this.nbQueries++;

      if (resultSet.next()) {
        res = resultSet.getInt(1);
      }

    } catch (SQLException e) {
      LOGGER.severe("Cant get predicate arity: " + e.getMessage());
    }

    return res;
  }

  private Collection<String> tableColumns(String table) {
    Collection<String> columns = new ArrayList<>();
    String query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ?";

    try (PreparedStatement statement = this.mysql.prepareStatement(query)) {
      statement.setString(1, table);
      ResultSet resultSet = statement.executeQuery();
      this.nbQueries++;

      while (resultSet.next()) {
        columns.add(resultSet.getString(1));
      }

    } catch (SQLException e) {
      LOGGER.severe("Can't get table columns: " + e.getMessage());
    }

    return columns;
  }

  @Override
  protected void chaseStep(Rule rule, Collection<Atom> newToIns, Collection<Atom> newToDel,
      Bdd ToDel, int deltaMax) {
    // Execute chase
    List<Substitution> substitutions = this.executeChase(rule);

    // Apply substitution
    for (Substitution sub : substitutions) {
      Rule r = sub.applySub(rule);
      Atom head = r.getHead();
      int prof = r.getMaxProf() + 1;

      // Set prof
      for (Variable v : head.getVariables()) {
        if (!r.getVariablesInBody().contains(v)) {
          v.setProf(prof);
        }
      }

      // Do chase step
      if (ToDel != null && (ToDel.containsModuloNull(head) || head.getMaxProf() > deltaMax)) {
        newToDel.addAll(r.getAtomesToDelInBody());

      } else {
        newToIns.add(head);
      }
    }
  }

  private List<Substitution> executeChase(Rule rule) {
    StringBuilder select = new StringBuilder("SELECT ");
    StringBuilder from = new StringBuilder(" FROM ");
    StringBuilder where = new StringBuilder(" WHERE ");
    StringBuilder headFrom = new StringBuilder("NOT EXISTS ( SELECT null FROM `");
    StringBuilder headWhere = new StringBuilder(" WHERE ");

    int nbVar = 0, nbHeadElem = 0, i;
    String predicate;
    Map<Element, String> variables = new HashMap<>();
    Map<Atom, Integer> predicates = new HashMap<>();
    Atom head = rule.getHead();

    // Generate query (adapted from listAtomeVarNullToSQLString)
    // Body
    for (Atom a : rule.getBody()) {
      // FROM
      if (!predicates.containsKey(a)) {
        if (!predicates.isEmpty()) {
          from.append(", ");
        }

        from.append('`').append(a.getName()).append("` AS T").append(predicates.size());
        predicates.put(a, predicates.size());
      }

      predicate = "T" + predicates.get(a);

      // SELECT + WHERE
      for (i = 0; i < a.size(); i++) {
        Element e = a.get(i);

        if (e.isConstant() || e.isNullValue()) {
          String name = e.isConstant() ? e.getName() : getNullName((Variable) e);
          where.append(predicate).append(".ch").append(i + 1).append(" = '").append(name)
              .append("'").append(" AND ");

        } else if (variables.containsKey(e)) {
          where.append(predicate).append(".ch").append(i + 1).append(" = ").append(variables.get(e))
              .append(" AND ");

        } else {
          variables.put(e, predicate + ".ch" + (i + 1));

          // Add substitution to SELECT
          if (nbVar++ > 0) {
            select.append(", ");
          }
          select.append(variables.get(e)).append(" AS `").append(e).append('`');
        }
      }
    }

    // No possible substitution
    if (nbVar == 0) {
      select.append("null");
    }

    // Head
    headFrom.append(head.getName()).append("` AS head");
    for (i = 0; i < head.size(); i++) {
      Element e = head.get(i);

      if (variables.containsKey(e)) {
        if (nbHeadElem > 0) {
          headWhere.append(" AND ");
        }
        headWhere.append("head.ch").append(i + 1).append(" = ").append(variables.get(e));
        nbHeadElem++;

      } else if (e.isConstant() || e.isNullValue()) {
        if (nbHeadElem > 0) {
          headWhere.append(" AND ");
        }
        String name = e.isConstant() ? e.getName() : getNullName((Variable) e);
        headWhere.append("head.ch").append(i + 1).append(" = '").append(name).append("'");
        nbHeadElem++;
      }
    }

    String headQuery = headFrom.toString() + headWhere + " )";
    String query = select.toString() + from + where + headQuery;
    LOGGER.info("CHASE query: " + query);

    // Get substitutions
    List<Substitution> substitutions = new ArrayList<>();
    try (Statement statement = this.mysql.createStatement()) {
      ResultSet resultSet = statement.executeQuery(query);
      this.nbQueries++;

      while (resultSet.next()) {
        Substitution sub = rule.getBodySubstitution();
        for (i = 1; i <= nbVar; i++) {
          sub.put((Variable) Element.stringToElement(resultSet.getMetaData().getColumnLabel(i)),
              Element.stringToElement(resultSet.getString(i)));
        }
        substitutions.add(sub);
      }

    } catch (SQLException e) {
      LOGGER.severe("PB Chase: " + e.getMessage());
    }

    return substitutions;
  }

  private void ensureTablesExists(Collection<? extends Atom> atoms, LinkedListRules rules) {
    for (Atom a : atoms) {
      this.addTable(a.getName(), a.size());
    }

    for (Rule r : rules) {
      this.addTable(r.getHead().getName(), r.getHead().size());
      for (Atom a : r.getBody()) {
        this.addTable(a.getName(), a.size());
      }
    }
  }

  @Override
  public BddStats removeWithConstraints(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted) {
    this.nbQueries = 0;
    BddStats bddStats = new BddStats();
    bddStats.start();

    try {
      // Start transaction
      bddStats.startCommit();
      this.mysql.setAutoCommit(false);
      bddStats.stopCommit();

      // Build needed tables
      bddStats.startInsert();
      this.ensureTablesExists(dRequest, rules);
      bddStats.stopInsert();

      // Get and delete request
      bddStats.startDelete();
      Collection<Atom> toDel = this.isomorphicAtom(dRequest, true);
      bddStats.stopDelete();
      bddStats.delAtoms(toDel.size());

      // Chase
      Bdd sideEffects = this.chaseForDeletion(toDel, rules, deltaMax, restricted, bddStats);

      // Get null bucket for core
      bddStats.startNullBucket();
      Collection<Element> nullBucket = this.getNullBucket(sideEffects);
      bddStats.stopNullBucket();

      // Do the core
      this.coreMaintenance(nullBucket, bddStats);

      // Commit
      bddStats.startCommit();
      this.mysql.commit();
      this.mysql.setAutoCommit(true);
      bddStats.stopCommit();

    } catch (SQLException exception) {
      exception.printStackTrace();
    }

    bddStats.stop();
    bddStats.addQueries(this.nbQueries);
    return bddStats;
  }

  @Override
  public BddStats addWithConstraints(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, Bdd thingsToAddAfterChase, boolean restricted) {
    this.nbQueries = 0;
    BddStats bddStats = new BddStats();
    bddStats.start();

    try {
      // Start transaction
      bddStats.startCommit();
      this.mysql.setAutoCommit(false);
      bddStats.stopCommit();

      // Build needed tables
      bddStats.startInsert();
      this.ensureTablesExists(iRequest, rules);
      bddStats.stopInsert();

      // Chase
      Bdd sideEffects = this.chaseForInsertion(iRequest, rules, deltaMax, restricted, bddStats);
      thingsToAddAfterChase.initFromDB(sideEffects);

      // Get null bucket for core
      bddStats.startNullBucket();
      Collection<Element> nullBucket = this.getNullBucket(sideEffects);
      bddStats.stopNullBucket();

      // Do the core
      this.coreMaintenance(nullBucket, bddStats);

      // Check null degree
      bddStats.startNullDegree();
      Collection<Element> invalidNulls = sideEffects.getNulls().stream().map(x -> (Variable) x)
          .filter(
              x -> deltaMax > 0 && x.getProf() > deltaMax).collect(Collectors.toSet());
      boolean cantCommit = this.containsAnyNulls(invalidNulls);
      bddStats.stopNullDegree();

      if (cantCommit) {
        this.mysql.rollback();

      } else {
        // Set null degree to 0
        bddStats.startNullDegree();
        this.setNullsDegree(sideEffects.getNulls(), 0);
        bddStats.stopNullDegree();

        // Commit
        bddStats.startCommit();
        this.mysql.commit();
        bddStats.stopCommit();
      }

      this.mysql.setAutoCommit(true);

    } catch (SQLException exception) {
      exception.printStackTrace();
    }

    bddStats.stop();
    bddStats.addQueries(this.nbQueries);
    return bddStats;
  }

  protected Bdd chaseForInsertion(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    this.ensureTablesExists(iRequest, rules);
    return super.chaseForInsertion(iRequest, rules, deltaMax, restricted, bddStats);
  }

  protected Bdd chaseForDeletion(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    this.ensureTablesExists(dRequest, rules);
    return super.chaseForDeletion(dRequest, rules, deltaMax, restricted, bddStats);
  }
}