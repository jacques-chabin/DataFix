package fr.lifo.updatechase.model.db.cypher;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.db.cypher.query.Neo4JQueryBuilderInterface;
import fr.lifo.updatechase.model.db.cypher.query.NoIndexQueryBuilder;
import fr.lifo.updatechase.model.db.cypher.strategy.NeoQueryStrategyInterface;
import fr.lifo.updatechase.model.db.cypher.strategy.NullDegreeConstantNeoQueryStrategy;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Substitution;
import fr.lifo.updatechase.model.logic.Variable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.QueryRunner;
import org.neo4j.driver.Record;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.exceptions.Neo4jException;
import org.neo4j.driver.summary.SummaryCounters;

public class BddNeo4j extends Bdd {

  /**
   * Neo4J database driver.
   */
  protected final Driver driver;

  /**
   * Size of chunk to use for batch operations.
   */
  protected static final int CHUNK_SIZE = 25000;

  /**
   * Should use recursive path for partition retrieval. Else, we use multiple query like the MySQL
   * implementations.
   */
  protected boolean longPartitionPath = true;

  /**
   * Strategy to use for the isomorphic atoms retrieval query.
   */
  protected NeoQueryStrategyInterface queryStrategy = new NullDegreeConstantNeoQueryStrategy();

  /**
   * The query implementation to use.
   */
  protected Neo4JQueryBuilderInterface queryBuilder = new NoIndexQueryBuilder();

  /**
   * Build a Neo4J database without authentication.
   *
   * @param address The hostname/ip of the database
   * @param port    The port of the database
   */
  public BddNeo4j(String address, int port) {
    this(address, port, null, null);
  }

  /**
   * Build a Neo4J database with authentication.
   *
   * @param address  The hostname/ip of the database
   * @param port     The port of the database
   * @param user     The username to connect to the database
   * @param password The password to connect to the database
   */
  public BddNeo4j(String address, int port, String user, String password) {
    String url = "bolt://" + address + ":" + port;

    if (user != null) {
      this.driver = GraphDatabase.driver(url, AuthTokens.basic(user, password));
    } else {
      this.driver = GraphDatabase.driver(url);
    }

    try (Session session = this.driver.session()) {
      this.postInit(session);

    } catch (Neo4jException error) {
      LOGGER.config("Index and constraints already set: " + error.getMessage());
    }
  }

  @Override
  public boolean isConnected() {
    try {
      this.driver.verifyConnectivity();
      return true;

    } catch (Neo4jException ignored) {
      /* IGNORED */
    }

    return false;
  }

  /**
   * Method called after the database connexion, used to initialized indexes.
   *
   * @param runner The database query runner
   * @throws Neo4jException If an error occur during a query
   */
  protected void postInit(QueryRunner runner) throws Neo4jException {
    // Clear old indexes
    runner.run("CALL apoc.schema.assert({}, {}, true) YIELD label, key");

    // Execute queries
    this.queryBuilder.postInitQueries().forEach(runner::run);
  }

  /**
   * Should use recursive path queries to retrieve partitions. Else, it is replaced by multiple
   * queries.
   *
   * @param value True to enable recursive paths
   */
  public void useLongPartitionPath(boolean value) {
    this.longPartitionPath = value;
  }

  /**
   * Check if the database use recursive path queries to to retrieve partitions.
   *
   * @return True if recursive paths is enabled
   */
  public boolean useLongPartitionPath() {
    return this.longPartitionPath;
  }

  /**
   * Set the query strategy to used.
   *
   * @param strategy The strategy to use
   */
  public void setQueryStrategy(NeoQueryStrategyInterface strategy) {
    this.queryStrategy = strategy;
  }

  /**
   * Set the query implementation to used. You should not change this with existing database as it
   * can lead to inconsistencies.
   *
   * @param queryBuilder The builder to use
   */
  public void setQueryBuilder(Neo4JQueryBuilderInterface queryBuilder) {
    this.queryBuilder = queryBuilder;
  }

  /**
   * Get the query strategy used for this database
   *
   * @return The query strategy instance
   */
  public NeoQueryStrategyInterface getQueryStrategy() {
    return this.queryStrategy;
  }

  /**
   * Get the instance of the query builder
   *
   * @return The query builder instance
   */
  public Neo4JQueryBuilderInterface getQueryBuilder() {
    return queryBuilder;
  }

  protected static Map<String, Object> getQueryParamsFromAtoms(Collection<? extends Atom> atoms) {
    Collection<String> constants = new HashSet<>();
    Collection<String> nulls = new HashSet<>();
    Collection<Map<String, Object>> atomeList = new ArrayList<>();
    Collection<Map<String, Object>> atomesElements;
    Map<String, Object> elemMap, atomeMap;
    Element elem;

    // ---- Convert atomes to map for query binding ----
    for (Atom atom : atoms) {
      atomesElements = new ArrayList<>();

      // Convert elements to map
      for (int rank = 0; rank < atom.size(); rank++) {
        elem = atom.get(rank);
        elemMap = new HashMap<>();
        String name = elem.getName();
        if (!elem.isConstant()) {
          name = getNullName((Variable) elem);
        }
        elemMap.put("name", name);
        elemMap.put("rank", rank);

        if (atom.get(rank).isConstant()) {
          constants.add(name);
        } else {
          nulls.add(name);
        }

        atomesElements.add(elemMap);
      }

      List<String> symbols = atom.stream().map(
              element -> element.isConstant() ? element.toString() : getNullName((Variable) element))
          .collect(Collectors.toList());

      // Convert atome to map
      atomeMap = new HashMap<>();
      atomeMap.put("name", atom.getName());
      atomeMap.put("elems", atomesElements);
      atomeMap.put("symbols", symbols);
      atomeList.add(atomeMap);
    }

    // ---- Prepare params ----
    Map<String, Object> params = new HashMap<>();
    params.put("constants", constants);
    params.put("nulls", nulls);
    params.put("atoms", atomeList);

    return params;
  }

  public static Atom buildAtom(String pred, List<Object> elementNames) {
    Element[] elements = elementNames.stream().map(Object::toString).map(Element::stringToElement)
        .toArray(Element[]::new);

    return new Atom(pred, elements);
  }

  private static Atom recordToAtom(Record record) {
    String pred = record.get("a").asString();
    List<Object> elementNames = record.get("e").asList();
    return buildAtom(pred, elementNames);
  }

  private static Set<Atom> recordToListAtom(Record record) {
    Set<Atom> instance = new HashSet<>();

    for (int j = 0; j < record.size() / 2; ++j) {
      String pred = record.get("a" + j).asString();
      List<Object> elementNames = record.get("e" + j).asList();
      instance.add(buildAtom(pred, elementNames));
    }

    return instance;
  }

  ////// Closable ////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public void close() {
    this.driver.close();
  }

  ////// Collection //////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public boolean add(Atom atom) {
    //noinspection RedundantCollectionOperation
    return this.addAll(Collections.singleton(atom));
  }

  @Override
  public boolean addAll(Collection<? extends Atom> atoms) {
    boolean res = false;
    List<Atom> atomList = new ArrayList<>(atoms);

    try (Session session = driver.session()) {
      for (int i = 0; i < atoms.size(); i += CHUNK_SIZE) {
        res =
            this.addAll(session, atomList.subList(i, Math.min(i + CHUNK_SIZE, atoms.size())), null)
                || res;
      }
    }

    return res;
  }

  protected boolean addAll(QueryRunner runner, Collection<? extends Atom> atoms, BddStats stats) {
    if (atoms == null || atoms.isEmpty()) {
      return false;
    }

    Map<String, Object> params = getQueryParamsFromAtoms(atoms);

    // ---- Execute queries ----
    Result result;
    SummaryCounters summary;
    int nbNodesCreated = 0;

    if (stats != null) {
      stats.addQueries(3);
    }

    // Create constants
    result = runner.run("UNWIND $constants AS c MERGE (:Element:Constant {name: c})", params);
    summary = result.consume().counters();
    nbNodesCreated += summary.nodesCreated();
    LOGGER.info(summary.nodesCreated() + " constants added");

    // Create nulls
    result = runner.run("UNWIND $nulls AS n MERGE (:Element:Null {name: n})", params);
    summary = result.consume().counters();
    nbNodesCreated += summary.nodesCreated();
    LOGGER.info(summary.nodesCreated() + " nulls added");

    // Create predicates

    result = runner.run(this.queryBuilder.addQuery(), params);
    summary = result.consume().counters();
    LOGGER.info(summary.nodesCreated() + " atoms added with " + summary.relationshipsCreated()
        + " relations");

    return (nbNodesCreated + summary.nodesCreated() + summary.relationshipsCreated()) > 0;
  }

  @Override
  public boolean remove(Object o) {
    //noinspection RedundantCollectionOperation
    return this.removeAll(Collections.singleton(o));
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    boolean res = false;
    List<?> list = new ArrayList<>(c);

    try (Session session = driver.session()) {
      for (int i = 0; i < list.size(); i += CHUNK_SIZE) {
        res = this.removeAll(session, list.subList(i, Math.min(i + CHUNK_SIZE, list.size())), null)
            || res;
      }
    }

    return res;
  }

  protected boolean removeAll(QueryRunner runner, Collection<?> c, BddStats stats) {
    if (c == null || c.isEmpty() || c.stream().anyMatch(o -> !(o instanceof Atom))) {
      return false;
    }

    if (stats != null) {
      stats.addQueries(1);
    }

    Map<String, Object> params = getQueryParamsFromAtoms((Collection<Atom>) c);
    Result result = runner.run(this.queryBuilder.delQuery(), params);
    SummaryCounters summary = result.consume().counters();
    LOGGER.info(summary.nodesDeleted() + " atoms deleted with " + summary.relationshipsDeleted()
        + " relations");

    return (summary.nodesDeleted() + summary.relationshipsDeleted()) > 0;
  }

  @Override
  public int size() {
    try (Session session = driver.session()) {
      Result result = session.run("MATCH (:Atom) RETURN COUNT(*) AS nbAtom LIMIT 1");
      return result.single().get("nbAtom", 0);
    }
  }

  @Override
  public void clear() {
    try (Session session = driver.session()) {
      session.run("MATCH (n) DETACH DELETE (n)");
      session.run("CALL db.prepareForReplanning()");
    }
  }

  @Override
  public boolean contains(Object o) {
    //noinspection RedundantCollectionOperation
    return this.containsAll(Collections.singleton(o));
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    if (c == null || c.isEmpty() || c.stream().anyMatch(o -> !(o instanceof Atom))) {
      return false;
    }

    try (Session session = driver.session()) {
      Map<String, Object> params = getQueryParamsFromAtoms((Collection<Atom>) c);
      Result result = session.run(this.queryBuilder.containsQuery(), params);
      return result.stream().allMatch(record -> record.get(0).asBoolean());
    }
  }

  @Override
  public Iterator<Atom> iterator() {
    try (Stream<Atom> stream = this.stream()) {
      return stream.collect(Collectors.toList()).iterator();
    }
  }

  /**
   * Get database as stream. It need to be close by calling close() or by using try-resource
   *
   * @return A stream of the database
   */
  @Override
  public Stream<Atom> stream() {
    Session session = driver.session();
    Result result = session.run(this.queryBuilder.getAll());
    return result.stream().map(BddNeo4j::recordToAtom).onClose(session::close);
  }

  ////// Bdd /////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public void initFromDB(Collection<Atom> init) {
    this.clear();
    try (Session session = this.driver.session()) {
      Map<String, Object> params = getQueryParamsFromAtoms(init);

      // ---- Execute queries ----
      Result result;
      SummaryCounters summary;

      // Create constants
      result = session.run("UNWIND $constants AS c CREATE (:Element:Constant {name: c})", params);
      summary = result.consume().counters();
      LOGGER.info(summary.nodesCreated() + " constants added");

      // Create nulls
      result = session.run("UNWIND $nulls AS n CREATE (:Element:Null {name: n})", params);
      summary = result.consume().counters();
      LOGGER.info(summary.nodesCreated() + " nulls added");

      // Create predicates
      result = session.run(this.queryBuilder.initQuery(), params);
      summary = result.consume().counters();
      LOGGER.info(summary.nodesCreated() + " atoms added with " + summary.relationshipsCreated()
          + " relations");
    }
  }

  @Override
  public Set<String> getAllPredicates() {
    try (Session session = driver.session()) {
      Result result = session.run("MATCH (a:Atom) RETURN DISTINCT a.name");
      return result.stream().map(record -> record.get(0).asString()).collect(Collectors.toSet());
    }
  }

  @Override
  public boolean containsPredicate(String predicate) {
    try (Session session = driver.session()) {
      Map<String, Object> params = new HashMap<>();
      params.put("name", predicate);

      Result result = session.run(
          "OPTIONAL MATCH (an:Atom {name: $name}) " + "RETURN an IS NOT NULL AS isExist LIMIT 1",
          params);
      return result.single().get(0).asBoolean();
    }
  }

  @Override
  public Set<Atom> getAtomsWithPredicate(String predicate) {
    try (Session session = driver.session()) {
      Map<String, Object> params = new HashMap<>();
      params.put("name", predicate);

      Result result = session.run(this.queryBuilder.getAllPredicate(), params);

      return result.stream().map(BddNeo4j::recordToAtom).collect(Collectors.toSet());
    }
  }

  @Override
  public boolean containsAnyNulls(Collection<? extends Element> nulls) {
    try (Session session = driver.session()) {
      return this.containsAnyNulls(session, nulls, null);
    }
  }

  protected boolean containsAnyNulls(QueryRunner runner, Collection<? extends Element> nulls,
      BddStats stats) {
    Map<String, Object> params = new HashMap<>();
    params.put("nulls",
        nulls.stream().map(el -> getNullName((Variable) el)).collect(Collectors.toSet()));

    if (stats != null) {
      stats.addQueries(1);
    }

    Result result = runner.run(
        "UNWIND $nulls AS nullName " + "OPTIONAL MATCH (e:Element {name: nullName}) "
            + "RETURN e IS NOT NULL AS isExist", params);
    return result.stream().anyMatch(record -> record.get(0).asBoolean());
  }

  @Override
  public Set<Element> getNulls() {
    try (Session session = driver.session()) {
      return this.getNulls(session, null);
    }
  }

  protected Set<Element> getNulls(QueryRunner runner, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    Result result = runner.run("MATCH (n:Null) RETURN DISTINCT n.name");
    return result.stream().map(record -> record.get(0).asString()).map(Element::stringToElement)
        .collect(Collectors.toSet());
  }

  @Override
  public int getNbNulls() {
    try (Session session = driver.session()) {
      return this.getNbNulls(session, null);
    }
  }

  protected int getNbNulls(QueryRunner runner, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    Result result = runner.run("MATCH (:Null) RETURN count(*)");
    return result.single().get(0).asInt();
  }

  @Override
  public int getNbConst() {
    try (Session session = driver.session()) {
      return this.getNbConst(session, null);
    }
  }

  protected int getNbConst(QueryRunner runner, BddStats stats) {
    if (stats != null) {
      stats.addQueries(1);
    }

    Result result = runner.run("MATCH (:Constant) RETURN count(*)");
    return result.single().get(0).asInt();
  }

  @Override
  public void setNullsDegree(Collection<Element> nulls, int degree) {
    try (Session session = driver.session()) {
      this.setNullsDegree(session, nulls, degree, null);
    }
  }

  protected void setNullsDegree(QueryRunner runner, Collection<Element> nulls, int degree,
      BddStats stats) {
    List<String[]> mappings = nulls.stream().map(element -> {
      Variable renamed = (Variable) element.clone();
      renamed.setProf(degree);

      return new String[]{getNullName((Variable) element), getNullName(renamed)};
    }).collect(Collectors.toList());

    mappings.forEach(mapping -> LOGGER.warning(mapping[0] + " - " + mapping[1]));

    if (stats != null) {
      stats.addQueries(1);
    }

    Map<String, Object> params = new HashMap<>();
    params.put("mappings", mappings);

    runner.run(this.queryBuilder.setNullDegree(), params).consume();
  }

  @Override
  public Set<Atom> isomorphicAtom(Atom a, boolean delete) {
    try (Session session = driver.session()) {
      String query = this.queryBuilder.isomorphicAtomQuery(a, delete);

      Result result = session.run(query);
      return result.stream().map(BddNeo4j::recordToAtom).collect(Collectors.toSet());
    }
  }

  @Override
  public boolean containsModuloNull(Atom a) {
    try (Session session = this.driver.session()) {
      String query = this.queryBuilder.isomorphQuery(Collections.singleton(a), 1,
          this.queryStrategy);
      return session.run(query).hasNext();
    }
  }

  @Override
  public BddStats addWithConstraints(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, Bdd thingsToAddAfterChase, boolean restricted) {
    try (Session session = driver.session()) {
      try (Transaction tx = session.beginTransaction()) {
        BddStats bddStats = new BddStats();
        bddStats.start();

        Bdd sideEffects = this.chaseForInsertion(tx, iRequest, rules, deltaMax, restricted,
            bddStats);
        thingsToAddAfterChase.initFromDB(sideEffects);

        bddStats.startNullBucket();
        Collection<Element> nullBucket = this.getNullBucket(tx, sideEffects, bddStats);
        bddStats.stopNullBucket();

        this.coreMaintenance(tx, nullBucket, bddStats);

        bddStats.startNullDegree();
        Collection<Element> invalidNulls = sideEffects.getNulls().stream().map(x -> (Variable) x)
            .filter(
                x -> deltaMax > 0 && x.getProf() > deltaMax).collect(Collectors.toSet());
        boolean cantCommit = this.containsAnyNulls(tx, invalidNulls, bddStats);
        bddStats.stopNullDegree();

        if (cantCommit) {
          tx.rollback();

        } else {
          bddStats.startNullDegree();
          this.setNullsDegree(tx, sideEffects.getNulls(), 0, bddStats);
          bddStats.stopNullDegree();

          bddStats.startCommit();
          tx.commit();
          bddStats.stopCommit();
        }

        bddStats.stop();
        return bddStats;
      }
    }
  }

  @Override
  public BddStats removeWithConstraints(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted) {
    try (Session session = driver.session()) {
      try (Transaction tx = session.beginTransaction()) {
        BddStats bddStats = new BddStats();
        bddStats.start();

        bddStats.startDelete();
        Collection<Atom> toDel = this.isomorphicAtom(dRequest, true);
        bddStats.startDelete();
        bddStats.delAtoms(toDel.size());

        Bdd sideEffects = this.chaseForDeletion(tx, toDel, rules, deltaMax, restricted, bddStats);

        bddStats.startNullBucket();
        Collection<Element> nullBucket = this.getNullBucket(tx, sideEffects, bddStats);
        bddStats.stopNullBucket();

        this.coreMaintenance(tx, nullBucket, bddStats);

        bddStats.startCommit();
        tx.commit();
        bddStats.stopCommit();

        bddStats.stop();
        return bddStats;
      }
    }
  }

  @Override
  protected Bdd chaseForInsertion(Collection<? extends Atom> iRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    try (Session session = driver.session()) {
      return this.chaseForInsertion(session, iRequest, rules, deltaMax, restricted, bddStats);
    }
  }

  protected Bdd chaseForInsertion(QueryRunner runner, Collection<? extends Atom> iRequest,
      LinkedListRules rules, int deltaMax, boolean restricted, BddStats bddStats) {
    int step = 1;
    Set<Substitution> substitutionCandidates = new HashSet<>(5);

    Bdd atomsInsertedBefore = new BddMemoire(iRequest); // Atoms inserted at previous step
    Bdd ToIns = new BddMemoire(iRequest);               // All atoms inserted

    bddStats.addAtoms(iRequest.size());
    bddStats.startInsert();
    this.addAll(runner, iRequest, bddStats);
    bddStats.stopInsert();

    while (!atomsInsertedBefore.isEmpty()) {
      Bdd atomsToInsert = new BddMemoire(); // Atoms to insert during the current step

      bddStats.startChase();
      for (Rule rule : rules) {
        // Get substitution candidates
        substitutionCandidates = rule.getBodySubstitutions(atomsInsertedBefore);

        int i = 0;
        for (Substitution substitution : substitutionCandidates) {
          Rule r = substitution.applySub(rule);
          LOGGER.info(
              "CHASE INS " + step + ": (" + ++i + "/" + substitutionCandidates.size() + ")");

          this.chaseStep(runner, r, atomsToInsert, null, null, deltaMax, bddStats);
        }
      }
      bddStats.stopChase();

      LOGGER.info("CHASE INS " + step + ": " + atomsToInsert + "\n");
      bddStats.addAtoms(atomsToInsert.size());
      bddStats.startInsert();
      this.addAll(runner, atomsToInsert, bddStats);
      bddStats.stopInsert();

      ToIns.addAll(atomsToInsert);

      atomsInsertedBefore.clear();
      for (Atom a : atomsToInsert) {
        if ((deltaMax < 1 || a.getMaxProf() < deltaMax) && (!restricted
            || a.containsConstantValues())) {
          atomsInsertedBefore.add(a);
        }
      }

      ++step;
    }

    return ToIns;
  }

  @Override
  protected Bdd chaseForDeletion(Collection<? extends Atom> dRequest, LinkedListRules rules,
      int deltaMax, boolean restricted, BddStats bddStats) {
    try (Session session = driver.session()) {
      return this.chaseForDeletion(session, dRequest, rules, deltaMax, restricted, bddStats);
    }
  }

  protected Bdd chaseForDeletion(QueryRunner runner, Collection<? extends Atom> dRequest,
      LinkedListRules rules, int deltaMax, boolean restricted, BddStats bddStats) {
    int step = 1;
    Set<Substitution> substitutionCandidates = new HashSet<>(5);

    Bdd atomsDeletedBefore = new BddMemoire(dRequest);  // Atoms deleted at previous step
    Bdd ToIns = new BddMemoire();                       // All atoms inserted
    Bdd ToDel = new BddMemoire(dRequest);               // All atoms deleted

    while (!atomsDeletedBefore.isEmpty()) {
      Bdd atomsToDelete = new BddMemoire(); // Atoms to delete during the current step

      bddStats.startChase();
      for (Rule rule : rules) {
        // Get substitution candidates
        substitutionCandidates = rule.getHeadSubstitutions(atomsDeletedBefore);

        int i = 0;
        for (Substitution substitution : substitutionCandidates) {
          Rule r = substitution.applySub(rule);
          LOGGER.info(
              "CHASE DEL " + step + ": (" + ++i + "/" + substitutionCandidates.size() + ")");

          this.chaseStep(runner, r, ToIns, atomsToDelete, ToDel, deltaMax, bddStats);
        }
      }
      bddStats.stopChase();

      ToIns = this.chaseForInsertion(runner, ToIns, rules, deltaMax, restricted, bddStats);
      for (Atom a : new BddMemoire(ToIns)) {
        if ((deltaMax > 0 && a.getMaxProf() > deltaMax) || ToDel.containsModuloNull(a)) {
          atomsToDelete.add(a);
          ToIns.remove(a);
        }
      }

      LOGGER.info("CHASE DEL " + step + ": " + atomsToDelete);
      bddStats.delAtoms(atomsToDelete.size());
      bddStats.startDelete();
      this.removeAll(runner, atomsToDelete, bddStats);
      bddStats.stopDelete();

      ToDel.addAll(atomsToDelete);
      atomsDeletedBefore = atomsToDelete;

      ++step;
    }

    Bdd sideEffects = new BddMemoire(ToDel);
    sideEffects.addAll(ToIns);

    return sideEffects;
  }

  @Override
  protected void chaseStep(Rule rule, Collection<Atom> newToIns, Collection<Atom> newToDel,
      Bdd ToDel, int deltaMax) {
    try (Session session = driver.session()) {
      try (Transaction tx = session.beginTransaction()) {
        this.chaseStep(tx, rule, newToIns, newToDel, ToDel, deltaMax, null);
        tx.rollback();
      }
    }
  }

  protected void chaseStep(QueryRunner runner, Rule rule, Collection<Atom> newToIns,
      Collection<Atom> newToDel, Bdd ToDel, int deltaMax, BddStats stats) {
    // Execute query
    String query = this.queryBuilder.chaseQuery(rule);
    LOGGER.info("Chase query: " + query);
    Result rs = runner.run(query);

    if (stats != null) {
      stats.addQueries(1);
    }

    // Get substitutions
    rs.forEachRemaining((Record record) -> {
      Substitution sub = rule.getBodySubstitution();
      Map<String, Object> subMap = record.get("sub").asMap();

      // Generate substitution
      for (Map.Entry<String, Object> entry : subMap.entrySet()) {
        Element key = Element.stringToElement(entry.getKey());
        Element value = Element.stringToElement(entry.getValue().toString());

        if (key.isVariable()) {
          sub.put((Variable) key, value);
        }
      }

      // Apply substitution
      Rule r = sub.applySub(rule);
      Atom head = r.getHead();
      int prof = r.getMaxProf() + 1;

      // Set prof
      for (Variable v : head.getVariables()) {
        if (!r.getVariablesInBody().contains(v)) {
          v.setProf(prof);
        }
      }

      // Do chase step
      if (ToDel != null && (ToDel.containsModuloNull(head) || head.getMaxProf() > deltaMax)) {
        LOGGER.info("DEL " + r.getAtomesToDelInBody());
        newToDel.addAll(r.getAtomesToDelInBody());

      } else {
        LOGGER.info("ADD " + head);
        newToIns.add(head);
      }
    });
  }

  @Override
  protected Stream<Substitution> getPartitionSubstitions(Collection<Atom> partition) {
    Session session = driver.session();
    return this.getPartitionSubstitions(session, partition, null, null).onClose(session::close);
  }

  protected Stream<Substitution> getPartitionSubstitions(QueryRunner runner,
      Collection<Atom> partition, Integer limit, BddStats stats) {
    String query = this.queryBuilder.isomorphQuery(partition, limit, this.queryStrategy);
    LOGGER.info("ISO query: " + query);

    Set<Variable> nullsElements = partition.stream().flatMap(a -> a.getNulls().stream())
        .collect(Collectors.toSet());

    if (stats != null) {
      stats.addQueries(1);
    }

    Result result = runner.run(query);
    return result.stream().map(record -> {
      Substitution sub = new Substitution();
      for (Variable nullElement : nullsElements) {
        String elementName = record.get(nullElement.getName()).asString();
        Element element = Element.stringToElement(elementName);
        sub.put(nullElement, element);
      }
      return sub;
    }).distinct();
  }

  @Override
  protected Set<Element> getNullBucket(Bdd atoms) {
    try (Session session = driver.session()) {
      return this.getNullBucket(session, atoms, null);
    }
  }

  protected Set<Element> getNullBucket(QueryRunner runner, Bdd atoms, BddStats stats) {
    Map<String, Object> params = new HashMap<>();
    List<Map<String, Object>> paramsList = new ArrayList<>();
    params.put("x", paramsList);
    for (Atom a : atoms) {
      Map<String, Object> subParam = new HashMap<>();
      subParam.put("pred", a.getName());
      subParam.put("ref", a.stream().map(element -> element.isConstant() ? element.getName() : null)
          .collect(Collectors.toList()));
      paramsList.add(subParam);
    }

    if (stats != null) {
      stats.addQueries(1);
    }

    Result result = runner.run(this.queryBuilder.nullBucketQuery(), params);

    Set<Element> nullBucket = atoms.getNulls();
    result.stream().map(record -> record.get(0).asString()).map(Element::stringToElement)
        .forEach(nullBucket::add);

    return nullBucket;
  }

  @Override
  protected Map<Set<Element>, Set<Atom>> getPartitions(Collection<Element> nullBucket) {
    try (Session session = driver.session()) {
      return this.getPartitions(session, nullBucket, null);
    }
  }

  protected Map<Set<Element>, Set<Atom>> getPartitions(QueryRunner runner,
      Collection<Element> nullBucket, BddStats stats) {
    //A set of null values (partition)
    //is associated with a set of elements belonging to this partition
    Map<Set<Element>, Set<Atom>> partitions = new HashMap<>();

    List<Element> toCompute = new ArrayList<>(nullBucket);

    // Max path length (traversal of all null-atom pair)
    int maxPathLength = this.longPartitionPath ? (this.getNbNulls(runner, stats) * 2) + 1 : 2;

    while (!toCompute.isEmpty()) {
      //Variable nullElement = (Variable) toCompute.get(0);
      //toCompute.remove(0);
      List<String> nulls = toCompute.stream().sequential().limit(50) // By chunk of 20
          .map(nullElement -> getNullName((Variable) nullElement)).collect(Collectors.toList());
      toCompute.removeAll(toCompute.stream().sequential().limit(50).collect(Collectors.toList()));

      // Prepare params
      Map<String, Object> params = new HashMap<>();
      params.put("nulls", nulls);
      //params.put("null", getNullName(nullElement));

      //We match a combination of an atom (a) and all the nulls is linked directly or indirectly by other nulls (n).
      //(we are recursively traveling Contains relationships in any direction)
      String cypherQuery = this.queryBuilder.partitionsQuery(maxPathLength);

      //We get all partitions in the database which corresponds to our null values in nullBucket
      LOGGER.info("Run partition query for " + nulls + " " + toCompute.size() + " remaining");
      Result rs = runner.run(cypherQuery, params);

      if (stats != null) {
        stats.addQueries(1);
      }

      rs.forEachRemaining(record -> {
        Atom atom = recordToAtom(record);
        List<Object> partition = record.get("partition").asList();

        Set<Element> foundNulls = partition.stream().map(Object::toString)
            .map(Element::stringToElement).collect(
                Collectors.toSet());

        if (!partitions.containsKey(foundNulls)) {
          partitions.put(foundNulls, new HashSet<>());
        }
        partitions.get(foundNulls).add(atom);

        if (this.longPartitionPath) {
          toCompute.removeAll(foundNulls);
        }
      });
    }

    return partitions;
  }

  @Override
  public boolean coreMaintenance(Collection<Element> nullBucket, BddStats bddStats) {
    try (Session session = driver.session()) {
      return this.coreMaintenance(session, nullBucket, bddStats);
    }
  }

  protected boolean coreMaintenance(QueryRunner runner, Collection<Element> nullBucket,
      BddStats bddStats) {
    boolean simplification = false;

    bddStats.startPartition();
    Map<Set<Element>, Set<Atom>> partitions = this.getPartitions(runner, nullBucket, bddStats);
    bddStats.stopPartition();

    int i = 1;
    for (Map.Entry<Set<Element>, Set<Atom>> partition : partitions.entrySet()) {
      LOGGER.info("Search simplification for partition " + (i++) + "/" + partitions.size());
      bddStats.startSimplification();

      // Simplify the partition on itself in memory to reduce query complexity
      Bdd partSimp = new BddMemoire(partition.getValue());
      if (partSimp.coreMaintenance(partition.getKey(), new BddStats())) {
        LOGGER.info("\n\n=> SIMPLIFY PARTITION\n\n");
      }

      Substitution identity = new Substitution();
      for (Atom atom : partSimp) {
        for (Variable nullElem : atom.getNulls()) {
          identity.put(nullElem, nullElem);
        }
      }

      List<Substitution> substitutions = this.getPartitionSubstitions(runner, partSimp, null,
          bddStats).collect(Collectors.toList());
      bddStats.startMoreSpecific();
      Substitution moreSpecSub = Substitution.getMostSpecific(substitutions, identity);
      bddStats.stopMoreSpecific();
      bddStats.stopSimplification();

      Collection<Atom> instance = moreSpecSub.applySub(partSimp);

      if (partition.getValue().size() != instance.size() || !partition.getValue()
          .containsAll(instance)) {
        LOGGER.info("**** Simplify " + partition + " with " + instance);
        bddStats.delAtoms(partition.getValue().size());
        bddStats.addAtoms(instance.size());

        bddStats.startDelete();
        this.removeAll(runner, partition.getValue(), bddStats);
        bddStats.stopDelete();

        bddStats.startInsert();
        this.addAll(runner, instance, bddStats);
        bddStats.stopInsert();

        simplification = true;
      }
    }

    return simplification;
  }

}