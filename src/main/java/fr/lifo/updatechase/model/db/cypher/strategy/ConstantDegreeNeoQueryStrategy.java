package fr.lifo.updatechase.model.db.cypher.strategy;

import fr.lifo.updatechase.model.logic.Atom;
import java.util.Collection;
import java.util.Comparator;

public class ConstantDegreeNeoQueryStrategy implements NeoQueryStrategyInterface {

  @Override
  public Atom getBFSRoot(Collection<Atom> atoms) {
    return atoms.parallelStream().min(Comparator.comparingLong(Atom::getNbNulls)).orElse(null);
  }

}
