package fr.lifo.updatechase.model.db.cypher.query;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.cypher.strategy.NeoQueryStrategyInterface;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Variable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class NoIndexQueryBuilder implements Neo4JQueryBuilderInterface {

  @Override
  public String isomorphQuery(Collection<Atom> atoms, Integer limit,
      NeoQueryStrategyInterface strategy) {
    StringBuilder query = new StringBuilder();
    StringBuilder queryEnd = new StringBuilder(" RETURN true");

    List<Atom> atomsAtDepth = new ArrayList<>(); // Need to keep order
    Set<Element> nullsAtDepth = new HashSet<>();
    Set<Element> nullsAtNextDepth = new HashSet<>();
    Map<Element, Integer> variableIndex = new HashMap<>();

    int index;
    int atomIndex = 0;

    // Sort by null number
    List<Atom> atomsSorted = new ArrayList<>(atoms);
    atomsSorted.sort(Comparator.comparingLong(Atom::getNbNulls));

    // Search starting atom (ie. the one with maximum nulls but with at least one constant to reduce search space)
    if (strategy != null) {
      Atom root = strategy.getBFSRoot(atomsSorted);
      if (root != null) {
        atomsSorted.remove(root);
        atomsAtDepth.add(root);
      }
    }

    // If no atoms found take the higher null degree
    if (atomsAtDepth.isEmpty() && !atomsSorted.isEmpty()) {
      atomsAtDepth.add(atomsSorted.remove(atomsSorted.size() - 1));
    }

    // Compute Breadth First Search
    while (!atomsAtDepth.isEmpty()) {
      for (Atom atom : atomsAtDepth) {
        StringBuilder nullMatch = new StringBuilder(" MATCH ");
        boolean firstNull = true;
        int rank = 0;

        // Build MATCH clause
        query.append(" MATCH (a").append(atomIndex).append(":Atom {name:'").append(atom.getName())
            .append("'})");

        // Add constants to WHERE clause and nulls to next MATCH
        for (Element element : atom) {
          if (variableIndex.containsKey(element)) {
            index = variableIndex.get(element);

          } else {
            index = variableIndex.size();
            variableIndex.put(element, index);

            if (!element.isConstant()) {
              queryEnd.append(", e").append(index).append(".name AS `").append(element.getName())
                  .append('`');
            }
          }

          // Build WHERE and MATCH
          if (element.isConstant() || nullsAtDepth.contains(element)) {
            query.append(", (a").append(atomIndex).append(")-[{rank:").append(rank).append("}]->(e")
                .append(index);

            if (element.isConstant()) {
              query.append(":Element {name:'").append(element.getName()).append("'})");
            } else {
              query.append(":Element)");
            }

          } else {
            if (!firstNull) {
              nullMatch.append(", ");
            }
            nullMatch.append("(a").append(atomIndex).append(")-[{rank:").append(rank)
                .append("}]->(e").append(index).append(
                    ":Element)");
            nullsAtNextDepth.add(element);
            firstNull = false;
          }

          rank++;
        }

        if (!firstNull) {
          query.append(nullMatch);
        }
        atomIndex++;
      }

      // Compute next depth
      atomsAtDepth.clear();
      nullsAtDepth.addAll(nullsAtNextDepth);

      for (Atom atom : atomsSorted) {
        if (atom.hasAnyElement(nullsAtDepth)) {
          atomsAtDepth.add(atom);
        }
      }

      // Edge case when we have two tree (can't appear in CORE but the query could be call somewhere else)
      if (atomsAtDepth.isEmpty() && !atomsSorted.isEmpty()) {
        atomsAtDepth.addAll(atomsSorted);
      }

      atomsSorted.removeAll(atomsAtDepth);
    }

    return query.toString() + queryEnd + (limit == null ? "" : " LIMIT " + limit);
  }

  @Override
  public String chaseQuery(Rule rule) {
    StringBuilder elements = new StringBuilder("MATCH ");
    StringBuilder match = new StringBuilder(" MATCH ");
    StringBuilder where = new StringBuilder(" WHERE NOT EXISTS { MATCH ");
    StringBuilder end = new StringBuilder(" } RETURN { ");

    int nbVar = 0, nbElem = 0, i = 0, j;
    Map<Element, Integer> variables = new HashMap<>();
    Atom head = rule.getHead();

    for (Atom a : rule.getBody()) {
      if (i > 0) {
        match.append(", ");
      }

      match.append("(a").append(i).append(":Atom {name:'").append(a.getName()).append("'})");

      j = 0;
      for (Element e : a) {

        if (!variables.containsKey(e)) {
          if (e.isConstant() || e.isNullValue()) {
            if (nbElem > 0) {
              elements.append(", ");
            }

            String name = e.isConstant() ? e.getName() : Bdd.getNullName((Variable) e);
            elements.append("(x").append(variables.size()).append(":Element {name:'").append(name)
                .append("'})");
            nbElem++;

          } else {
            if (nbVar > 0) {
              end.append(", ");
            }
            end.append("`").append(e).append("`: x").append(variables.size()).append(".name");
            nbVar++;

          }
          variables.put(e, variables.size());
        }

        match.append(", (a").append(i).append(")-[{rank:").append(j).append("}]->(x")
            .append(variables.get(e)).append(":Element)");
        ++j;
      }

      ++i;
    }

    j = 0;
    where.append("(a:Atom {name:'").append(head.getName()).append("'})");
    for (Element e : head) {

      if (variables.containsKey(e)) {
        where.append(", (a)-[{rank:").append(j).append("}]->(x").append(variables.get(e))
            .append(")");

      } else if (e.isConstant() || e.isNullValue()) {
        String name = e.isConstant() ? e.getName() : Bdd.getNullName((Variable) e);
        where.append(", (a)-[{rank:").append(j).append("}]->(:Element {name:'").append(name)
            .append("'})");

      }

      ++j;
    }

    end.append("} AS sub");

    if (nbElem == 0) {
      elements = new StringBuilder();
    }

    return elements.toString() + match + where + end;
  }

  @Override
  public String initQuery() {
    return "UNWIND $atoms AS a " + "CREATE (new:Atom {name:a.name}) " + "WITH a, new "
        + "UNWIND a.elems AS e "
        + "MATCH (en:Element {name:e.name}) " + "CREATE (new)-[:Contains {rank:e.rank}]->(en);";
  }

  @Override
  public String addQuery() {
    return "UNWIND $atoms AS a "

        // Check atom existence
        + "OPTIONAL MATCH (an:Atom {name:a.name}) "
        + "WHERE ALL(e IN a.elems WHERE (an)-[:Contains {rank:e.rank}]->(:Element {name: e.name})) "

        // Create atom if not exist
        + "WITH a, an " + "WHERE an IS null " + "CREATE (new:Atom {name:a.name}) "

        // Create links with elements
        + "WITH a, new " + "UNWIND a.elems AS e " + "MATCH (en:Element {name:e.name}) "
        + "CREATE (new)-[:Contains {rank:e.rank}]->(en);";
  }

  @Override
  public String delQuery() {
    return "UNWIND $atoms AS a " + "MATCH (an:Atom {name: a.name}) "
        + "WHERE ALL(e IN a.elems WHERE (an)-[{rank:e.rank}]->(:Element {name: e.name})) "
        + "DETACH DELETE an";
  }

  @Override
  public String containsQuery() {
    return "UNWIND $atoms AS a " + "OPTIONAL MATCH (an:Atom {name:a.name}) "
        + "WHERE ALL(e IN a.elems WHERE (an)-[{rank:e.rank}]->(:Element {name: e.name})) "
        + "RETURN an IS NOT NULL AS isExist";
  }

  @Override
  public String getAll() {
    return "MATCH (a:Atom)-[r]->(e:Element) " + "WITH a, e ORDER BY r.rank "
        + "WITH a, collect(e.name) as e "
        + "RETURN a.name as a, e";
  }

  @Override
  public String getAllPredicate() {
    return "MATCH (a:Atom {name: $name})-[r]->(e:Element) " + "WITH a, e ORDER BY r.rank "
        + "WITH a, collect(e.name) as e "
        + "RETURN a.name as a, e";
  }

  @Override
  public String setNullDegree() {
    return "UNWIND $mappings AS mapping " + "MATCH (e:Element {name: mapping[0]}) "
        + "SET e.name = mapping[1]";
  }

  @Override
  public String isomorphicAtomQuery(Atom a, boolean delete) {
    StringBuilder match = new StringBuilder("MATCH (an:Atom {name:'" + a.getName() + "'})");
    String end = "\nMATCH (an)-[r]->(e:Element)";
    end += "\nWITH an, an.name AS a, e ORDER BY r.rank\nWITH an, a, COLLECT(e.name) AS e";
    if (delete) {
      end += "\nDETACH DELETE an";
    }
    end += "\nRETURN a, e";

    Element e;
    Map<Element, Integer> variables = new HashMap<>();
    int varCount = 0;

    for (int i = 0; i < a.size(); i++) {
      e = a.get(i);

      if (e.isConstant()) {
        match.append(
            String.format(", (an)-[{rank:%d}]->(:Element:Constant {name:'%s'})", i, e.getName()));

      } else {
        if (!variables.containsKey(e)) {
          variables.put(e, varCount++);
        }
        match.append(String.format(", (an)-[{rank:%d}]->(x%d:Element:Null)", i, variables.get(e)));
      }
    }

    return match + end;
  }

  @Override
  public String nullBucketQuery() {
    return "UNWIND $x AS x " + "MATCH (a:Atom {name: x.pred})-[r]->(e:Element) "
        + "WITH x, a, r, e ORDER BY r.rank "
        + "WITH a, COLLECT(e) AS elems, x.ref AS ref "
        + "WHERE size(elems) = size(ref) AND ALL(i IN range(0, size(elems)) WHERE elems[i]:Null OR ref[i] IS NULL OR elems[i].name = ref[i]) "
        + "UNWIND elems AS e " + "WITH e " + "WHERE e:Null " + "RETURN DISTINCT e.name";
  }

  @Override
  public String partitionsQuery(int maxPathLength) {
    return "UNWIND $nulls AS n\n" + "MATCH p = (x:Element:Null {name: n})-[*1.." + maxPathLength
        + "]-(y)\n"
        + "WHERE x <> y AND ALL(n IN nodes(p) WHERE NOT (n:Constant))\n"
        + "WITH x, COLLECT(DISTINCT y) AS nodes\n"
        + "WITH [n IN nodes WHERE (n:Atom)] AS atoms, [x.name] + [n IN nodes WHERE (n:Null) | n.name] AS partition\n"
        + "UNWIND atoms AS a\n" + "MATCH (a)-[r]->(e:Element)\n"
        + "WITH a, e, partition ORDER BY r.rank\n"
        + "WITH a, collect(e.name) as e, partition\n" + "RETURN a.name as a, e, partition";
  }

  @Override
  public Collection<String> postInitQueries() {
    return List.of("CREATE INDEX atomName IF NOT EXISTS FOR (a:Atom) ON (a.name)",
        "CREATE CONSTRAINT elemName IF NOT EXISTS FOR (n:Element) REQUIRE n.name IS UNIQUE");
  }
}
