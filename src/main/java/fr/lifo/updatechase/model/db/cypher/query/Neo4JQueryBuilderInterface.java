package fr.lifo.updatechase.model.db.cypher.query;

import fr.lifo.updatechase.model.db.cypher.strategy.NeoQueryStrategyInterface;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Rule;
import java.util.Collection;

public interface Neo4JQueryBuilderInterface {

  /**
   * Generates a Cypher query to get all the atoms matching an atom set in which the null values
   * become variables. Query should return something if the atom set exist in the database
   *
   * @param atoms The collection of atoms to match in the database.
   * @return The generated query
   */
  String isomorphQuery(Collection<Atom> atoms, Integer limit, NeoQueryStrategyInterface strategy);

  /**
   * Generate a Cypher query for the chase algorithm given a specific rule that can be partially
   * instantiated Query should return a map of variable name => instance
   *
   * @param rule The rule to generate query from
   * @return The generated query
   */
  String chaseQuery(Rule rule);

  String initQuery();

  String addQuery();

  String delQuery();

  String containsQuery();

  String getAll();

  String getAllPredicate();

  String setNullDegree();

  String isomorphicAtomQuery(Atom a, boolean delete);

  String nullBucketQuery();

  String partitionsQuery(int maxPathLength);

  Collection<String> postInitQueries();
}
