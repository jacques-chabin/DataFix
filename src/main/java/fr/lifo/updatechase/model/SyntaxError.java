package fr.lifo.updatechase.model;

/**
 * <p><b>SyntaxError est une exception lancee si une erreur dans une syntaxe est detectee.</b></p>
 * <p>SyntaxError herite de Exception.<p>
 *
 * @see Exception
 */
public class SyntaxError extends Exception {

  /**
   * Constructeur de SyntaxError
   */
  SyntaxError() {
    super();
  }

  /**
   * Constructeur de SyntaxError
   *
   * @oaram String e Le message d'erreur.
   */
  public SyntaxError(String e) {
    super(e);
  }
}	
