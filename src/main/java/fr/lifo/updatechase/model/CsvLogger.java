package fr.lifo.updatechase.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvLogger {

  private final Map<String, List<String>> map;
  private int size;

  public CsvLogger() {
    this.map = new HashMap<>();
    this.size = 0;
  }

  public static List<String> initListNulls(int size) {
    List<String> list = new ArrayList<>();
    for (int i = 0; i < size; ++i) {
      list.add(null);
    }
    return list;
  }

  public void put(String key, String value) {
    if (!this.map.containsKey(key)) {
      this.map.put(key, CsvLogger.initListNulls(this.size));
    }
    this.map.get(key).set(this.size - 1, value);
  }

  public void newLine() {
    this.size++;
    for (List<String> list : this.map.values()) {
      list.add(null);
    }
  }

  public void save(String file) throws IOException {
    BufferedWriter writer = new BufferedWriter(new FileWriter(file));

    int i = 0;
    for (String key : this.map.keySet()) {
      writer.write(key);
      if (i == this.map.keySet().size() - 1) {
        writer.write("\n");
      } else {
        writer.write(",");
      }
      ++i;
    }

    for (int j = 0; j < this.size; ++j) {
      String line = "";
      i = 0;
      for (String key : this.map.keySet()) {
        writer.write(this.map.get(key).get(j));
        if (i == this.map.keySet().size() - 1) {
          writer.write("\n");
        } else {
          writer.write(",");
        }
        ++i;
      }
    }

    writer.close();
  }

}
