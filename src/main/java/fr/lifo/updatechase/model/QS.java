package fr.lifo.updatechase.model;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.db.sql.MySQL;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.Constant;
import fr.lifo.updatechase.model.logic.Element;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Query;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.model.logic.Schema;
import fr.lifo.updatechase.model.logic.Variable;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 */
public class QS {

  public static int verbose = 2;
  public static float putRedOldNewVarTime = 0F;
  public static int cycleCond = 0;    //  0 pas de restriction.
  //  n > 0 limite la profondeur de recherche à n
  //  -1 n'utilise pas les atomes contenants des nulles pour déduire une nouvelle information
  //  -2 n'utilise pas les atomes contenants des nulles stared pour déduire une nouvelle information

  public static void mainUpdateAddBdd(String[] args) throws IOException {
    //String directory;
    //directory = "Examples/Update/Cycle/";
    //  Inference rules
    LinkedListRules depSet = new LinkedListRules();
    long tempsDebutLecture;
    long tempsFinLecture;
    long tempsDebut;
    long tempsFin1;
    int ncpb;
    float secondsLecture;
    float seconds;
    Bdd db = new BddMemoire();
    Bdd toAdd = new BddMemoire();
    Bdd thingsToAddAfterChase = new BddMemoire();

    // lecture des règles d'inférences ==============================
    try {
      depSet = LinkedListRules.fromFile(args[1] + "InfRule");
    } catch (Exception ignored) {
    }

    System.out.println("Lecture de la Bdd ");
    tempsDebutLecture = System.currentTimeMillis();

    //Database init ======================================
    try {
      db = BddMemoire.fromFile(args[0] + "BddAll");
    } catch (Exception ignored) {
    }
    tempsFinLecture = System.currentTimeMillis();
    secondsLecture = (tempsFinLecture - tempsDebutLecture) / 1000F;
    System.out.println("Lecture Bdd effectuée en: " + secondsLecture + " secondes.");

    System.out.println("Lecture des atomes à ajouter ");
    //Atomes to add init ====================================
    try {
      toAdd = BddMemoire.fromFile(args[3] + "AddAtome");
    } catch (Exception ignored) {
    }

    System.out.println("Dependencies " + depSet);
    System.out.println("Nb dependencies : " + depSet.size());
    System.out.println("Initial Database : " + db);
    System.out.println("Initial DataBase size " + db.size());
    System.out.println("Atome to add : " + toAdd);

    tempsDebut = System.currentTimeMillis();

    int stopCondition = 0;
    // when stopCond == 1 don't apply dependencies on atom with null values

    BddStats stats = db.addWithConstraints(toAdd, depSet, stopCondition, thingsToAddAfterChase,
        true);

    tempsFin1 = System.currentTimeMillis();
    seconds = (tempsFin1 - tempsDebut) / 1000F;
    System.out.println("updateAdd effectué en: " + seconds + " secondes.");
    System.out.println("Nb atomes to add " + stats.getNumberAdd());
    System.out.println("Temps de put red ... " + putRedOldNewVarTime);
    //System.out.println("Bdd after updateAdd " + db);

    //database ===========================================
    System.out.println("Saving things to add...");
    db.toFile(args[0] + "res/res_bdd.dlp");
    thingsToAddAfterChase.toFile(args[2]);
    System.out.println("Done.");
  }

  public static Bdd mainCore(Bdd db)/*throws IOException*/ {
		/*String directory;
		directory = "Examples/Update/Cycle/";
		Bdd db;
		db = Bdd.initFromFile(directory+"BddAll");*/
    Bdd aux = new BddMemoire();
    aux.addAll(db);
    if (aux.core()) {
      System.out.println("BD simplified "); //+ aux);
    } else {
      System.out.println("BD not simplified "); // + aux);
    }

    return aux;
  }

  //TODO supprimer méthodes inutiles
  public static Bdd mainChaseN(Bdd db, LinkedListRules llr, int cycle,
      boolean restricted)/*throws IOException*/ {

    BddMemoire aux = new BddMemoire();
    aux.addAll(db);
    //String directory;
    //directory = "Examples/Update/Cycle/";
    //Bdd db;
    //db = Bdd.initFromFile(directory+"BddAll");
    /* initialisation of inference rules */
    //LinkedListRules depSet = LinkedListRules.initFromFile(directory+"InfRule");
    System.out.println("Inference Rules " + llr);

    int ncpb = aux.update2(llr, cycle, restricted);
    //System.out.println("Nombre d'atomes ajoutés : "+ncpb+" BD : " + db);

    return aux;
  }

  public static Bdd mainChaseTransaction(Bdd db, LinkedListRules llr, int cycle,
      boolean restricted)/*throws IOException*/ {

    return ((BddMemoire) db).updateForTransaction(llr, cycle, restricted);
  }

  public static Bdd mainAdd(Bdd db, Bdd bddToAdd, LinkedListRules llr, int cycleCond,
      boolean restreint) {
    //thingsToAddAfterChase : Bdd contenant TOUTES les valeurs à ajouter
    //dbbToAdd : Bdd contenant les valeurs que l'ont VEUT ajouter

    Bdd thingsToAddAfterChase = new BddMemoire();
    db.addWithConstraints(bddToAdd, llr, cycleCond, thingsToAddAfterChase, restreint);

    return thingsToAddAfterChase;
  }

  /**
   * Methode principale pour la suppression d'atomes.
   *
   * @param db       Bdd. La Bdd actuel (source).
   * @param bddToDel Bdd. La condition d'arret.
   * @param llr      LinkedListRules. la listes de regles à appliquer lors de la suppression
   * @return Bdd la Bdd contenant tous les atomes que l'ont DOIT supprimer.
   */
  public static Bdd mainDel(Bdd db, Bdd bddToDel, LinkedListRules llr, int cycleCond,
      boolean restreint) {
    //thingsToDel : Bdd contenant TOUTES les valeurs à detruire
    //dbbToDel : Bdd contenant les valeurs que l'ont VEUT detruire

    db.removeWithConstraints(bddToDel, llr, cycleCond, restreint);

    return db;
  }

  public static void mainUpdateTestBdd(String[] args) throws IOException {
    String directory;
    //directory = "Examples/UpdateLUBM/DataSmall1/";
    //		directory = "Examples/Update/Cycle/";
    //		directory = "Examples/UpdateLUBM/TargetTest/";

    LinkedListRules depSet = new LinkedListRules();
    LinkedListRules negConstraintsSet;
    long tempsDebutLecture;
    long tempsFinLecture;
    long tempsDebut;
    long tempsFin1;
    int ncpb;
    float secondsLecture;
    float seconds;
    Bdd db = new BddMemoire();

    // ====== Main Source directory ======
    //directory = "/home/stagiaire/Documents/chasebench-master/scenarios/updateChase/correctness/tgds/";
    //directory = "/home/stagiaire/Documents/UpdateChase/Examples/UpdateLUBM/TargetTest/";
    //=============================================================================================||
    //directory = "Examples/UpdateLUBM/DataSmall1/";
    //directory = "Examples/Update/Cycle/";
    //directory = "Examples/UpdateLUBM/TargetTest/";

    //====== InfRules File ======
    try {
      depSet = LinkedListRules.fromFile(args[1] + "InfRule");
      //depSet = LinkedListRules.initFromFile(directory+"InfRules");
    } catch (Exception ignored) {
    }
    //=============================================================================================||

    System.out.println("Lecture de la Bdd ");
    tempsDebutLecture = System.currentTimeMillis();
    //Bdd db = Bdd.initFromFile(directory+"Queries10");

    //====== DB File ======
    try {
      db = BddMemoire.fromFile(args[0] + "BddAll");
    } catch (Exception ignored) {
    }
    //db = Bdd.initFromFile(directory+"BddAll");
    //========================================================||

    //System.out.println("Initial Bdd : " + db);
    tempsFinLecture = System.currentTimeMillis();
    secondsLecture = (tempsFinLecture - tempsDebutLecture) / 1000F;
    System.out.println("Lecture requete effectuée en: " + secondsLecture + " secondes.");

    /* no negative constraints for instance */
    //negConstraintsSet = new LinkedListRules();

    //Query q1 = (Query)queriesSet.get(0);

    //System.out.println("Dependencies " + depSet);
    System.out.println("Nb dependencies : " + depSet.size());
    System.out.println("Initial DataBase size " + db.size());
    //System.out.println("Initial Database : " + db);

    tempsDebut = System.currentTimeMillis();

    // ncpb = db.update2(depSet, cycleCond);

    tempsFin1 = System.currentTimeMillis();
    seconds = (tempsFin1 - tempsDebut) / 1000F;
    System.out.println("tranfo requete effectuée en: " + seconds + " secondes.");
    System.out.println("Result DataBase size " + db.size());
    System.out.println("Temps de put red ... " + putRedOldNewVarTime);

    //affichage bd resultat.

    //System.out.println("Result Data Base " + db);

    //====== SaveDb ======
    System.out.println("Writing db to file...");
    db.toFile(args[2]);
    System.out.println("Done.");
    //===========================================================||
  }

  public static void mainUpdateTest(String[] args) {
    boolean CREATEDATABASE = false;
    String directory = "Examples/UpdateLUBM/TargetTest/";
    //String directory = "Examples/UpdateLUBM/";
    //String directory = "Examples/WeakCorr/";
    try {
      LinkedListRules depSet = LinkedListRules.fromFile(directory + "InfRule");

      System.out.println("Lecture de la query ");
      long tempsDebutLecture = System.currentTimeMillis();
      LinkedListRules queriesSet = LinkedListRules.fromFile(directory + "Queries100");
      long tempsFinLecture = System.currentTimeMillis();
      float secondsLecture = (tempsFinLecture - tempsDebutLecture) / 1000F;
      System.out.println("Lecture requete effectuée en: " + secondsLecture + " secondes.");

      /* no negative constraints for instance */
      LinkedListRules negConstraintsSet = new LinkedListRules();
      Query q1 = (Query) queriesSet.get(0);

      System.out.println("Dependencies " + depSet);
      System.out.println("Nb dependencies : " + depSet.size());
      System.out.println("Initial DataBase size " + q1.getBody().size());
      //System.out.println("Initial Database : " + q1.getBody());

      FileWriter fw = null;

      if (CREATEDATABASE) {
        try {
          // open the file named ansAlgo1.dlp in the current directory
          fw = new FileWriter(directory + "Queries100.dlp");
        } catch (IOException e3) {
          System.out.println("Can't create file " + e3.getMessage());
        }
        fw.write(q1.toString() + ".\n");
        fw.close();
      }
      long tempsDebut = System.currentTimeMillis();

      //int ncpb = q1.algo1(depSet, negConstraintsSet, stopCondition);
      int ncpb = q1.update1(depSet, cycleCond);

      long tempsFin1 = System.currentTimeMillis();
      float seconds = (tempsFin1 - tempsDebut) / 1000F;
      System.out.println("tranfo requete effectuée en: " + seconds + " secondes.");
      System.out.println("Result DataBase size " + q1.getBody().size());
      //System.out.println("Result Data Base " + q1.getBody());
    } catch (Exception ignored) {
    }
  }

  public static void mainUpdate(String[] args) {

    String directory = "Examples/Update/";
    String dataBase = "test";
    String user = "berlin";
    String mdp = "berlin";
    try {
      LinkedListRules posConstraintsSet = LinkedListRules.fromFile(directory + "PosConstraint");
      System.out.println("Positives Constraints " + posConstraintsSet);

      /* no negative constraints for instance */
      LinkedListRules negConstraintsSet = new LinkedListRules();
			/* negConstraintsSet = LinkedListRules.initFromFile(directory+"NegConstraint");
			System.out.println("Negatives Constraints " + negConstraintsSet); */

      LinkedListRules queriesSet = LinkedListRules.fromFile(directory + "Queries");
      System.out.println("Queries : " + queriesSet);

      /* We consider for instance only the first query */
      Query q1 = (Query) queriesSet.get(0);

      /* Query is completed with positive constraints and we check if there is no inconsistance with neg constraints */
      long tempsDebut = System.currentTimeMillis();
      if (verbose >= 1) {
        System.out.println("query before algo1 : " + q1);
      }
      cycleCond = -1;
      int ncpb = q1.algo1(posConstraintsSet, negConstraintsSet, cycleCond);

      if (verbose >= 1) {
        System.out.println("query after algo1 : " + q1);
      }
      if (ncpb != -1) {
        System.out.println("STOP  query maps the negative constraint number  " + ncpb);
        return;
      } else {
        if (verbose >= 1) {
          System.out.println("No problem with negative constraints");
        }
      }
      long tempsFin1 = System.currentTimeMillis();
      float seconds = (tempsFin1 - tempsDebut) / 1000F;
      System.out.println("tranfo requete effectuée en: " + seconds + " secondes.");
    } catch (Exception ignored) {
    }

  }

  public static void mainQS(String[] args) {
    boolean LUBM = false;
    boolean classic = true;
		
		/* String directory = "Examples/Paper2/";
		String dataBase = "paper";
		String user = "userpaper";
		String mdp="userpaper"; 	*/

    /* Example avec Raqueline */
		/*
		String directory = "Examples/Berlin1/Query1/";
		String dataBase = "benchmark";
		String user = "berlin";
		String mdp = "berlin"; */

    /* Example LUBM */
		/*
		LUBM=true;
		classic=false;
		String directory = "Examples/LUBM/result_data_cf_60/";
		String dataBase = "lubm";
		String user = "lubm";
		String mdp = "lubm";  
		*/

    //Example pour tester la réécriture avec les CP
		/*
		String directory = "Examples/Update/";
		String dataBase = "test";
		String user = "berlin";
		String mdp = "berlin";
		*/
    // String directory = "Examples/DBPedia/";

    String directory = "Examples/Paper/";
    String dataBase = "test";
    String user = "userpaper";
    String mdp = "userpaper";

    try {
      /* initialisation of the sets of constraints */
      LinkedListRules keyConstraintsSet = LinkedListRules.fromFile(directory + "KeyConstraint");
      System.out.println("Key Constraints " + keyConstraintsSet);
      LinkedListRules posConstraintsSet = LinkedListRules.fromFile(directory + "PosConstraint");
      System.out.println("Positives Constraints " + posConstraintsSet);
      LinkedListRules negConstraintsSet = LinkedListRules.fromFile(directory + "NegConstraint");
      System.out.println("Negatives Constraints " + negConstraintsSet);

      /* initialisation of inference rules */
      LinkedListRules infRulesSet = LinkedListRules.fromFile(directory + "InfRule");
      System.out.println("Inference Rules " + infRulesSet);

      /* initialisation of queries */
      LinkedListRules queriesSet = LinkedListRules.fromFile(directory + "Queries");
      System.out.println("Queries : " + queriesSet);


      /* initialisation of the graph schema liste of predicate(attributs) */
      Schema sch = Schema.fromFile(directory + "Schema");
      System.out.println("Schema : " + sch);

      /* initialisation of the sparql schema liste of predicate(type) */
      /* used for sparql translation */
      Schema schSp = Schema.fromFile(directory + "SchemaSparql");
      System.out.println("SchemaSparql : " + schSp);

      /* We consider for instance only the first query */
      Query q1 = (Query) queriesSet.get(0);

      /* Query is completed with positive constraints and we check if there is no inconsistance with neg constraints */
      long tempsDebut = System.currentTimeMillis();
      if (verbose >= 1) {
        System.out.println("query before algo1 : " + q1);
      }
      int ncpb = q1.algo1(posConstraintsSet, negConstraintsSet, 0);
      if (verbose >= 1) {
        System.out.println("query after algo1 : " + q1);
      }
      if (ncpb != -1) {
        System.out.println("STOP  query maps the negative constraint number  " + ncpb);
        return;
      } else {
        if (verbose >= 1) {
          System.out.println("No problem with negative constraints");
        }
      }
      long tempsFin1 = System.currentTimeMillis();
      float seconds = (tempsFin1 - tempsDebut) / 1000F;
      System.out.println("tranfo requete effectuée en: " + seconds + " secondes.");

      /* ans of the query is obtained from datalog file : */
      // LinkedListRules ansQuerySet = LinkedListRules.initFromFile(directory+"AnsQuery");

      /* ans of the query is obtained from MySQL evaluator : */
      // LinkedListRules ansQuerySet = MySQL.answer(q1, sch, dataBase, user, mdp);
      // on n'obtient plus les réponses mais le nombre de réponses
      // les réponses sont dans un fichier ansAlgo1.dlp

      int nbAnsAlgo1 = 0;
      //EVALUATOR
      if (classic) {
        nbAnsAlgo1 = MySQL.answer(q1, sch, dataBase, user, mdp);
      }
      /* ans of the query is obtained from SPARQL end point : */
      //LinkedListRules ansQuerySet = Sparql.answer(q1, sch);

      /* ans of the query is obtained from Raqueline answer file : */
      //nbAnsAlgo1 = LinkedListRules.initFromAnsSparqlFile(directory+"ansQuerySmall", q1);  //first
      //nbAnsAlgo1 = LinkedListRules.initFromAnsSparqlFile(directory+"ansQueryTotal", q1);
      else if (LUBM) {
        nbAnsAlgo1 = LinkedListRules.initFromAnsMapRedFile(directory + "ansQuerySmall", q1);
        //nbAnsAlgo1 = LinkedListRules.initFromAnsMapRedFile(directory+"ansQueryTotal", q1);
      }
      long tempsFin2 = System.currentTimeMillis();
      seconds = (tempsFin2 - tempsFin1) / 1000F;
      System.out.println("reponse requete effectuée en: " + seconds + " secondes.");

      /* Pourquoi la suite : juste voir une traduction vers sparql ??? */
	        /* Query qc = q1.completeAllVariables();
	        String querySparql = qc.queryToSparql(schSp);
	        System.out.println("Sparql Query to answer : " + querySparql); */

      //if(verbose>=1) System.out.println("Answers of a query used fro algo2 " + ansQuerySet);

      // Algo2 check if constraints are valid over answers

      System.out.println("Number of answers after algo1 : " + nbAnsAlgo1);

      LinkedListRules ansQueryValidSet = new LinkedListRules();
      int cptQV = 0;
      int cptQAV = 0;

      Cache queriesChecked = new Cache();
      // dans le cache on conserve les requêtes déjà évaluées associées à 0 ou 1
      // Si la requête est associée à 0 (affiché False) cela signifie que la contrainte qui a fait créer cette requête
      // peut être instanciée dans notre instance de BD (réponse non valide)
      // Si la requête est associée à 1 (affiché True) cela signifie que la contrainte qui a fait créer cette requête
      // n'est pas instanciable dans notre instance de BD (la réponse est valide si c'est le cas pour toutes les requêtes crées par la réponse).
      FileWriter fw = null;

      if (LUBM) {
        try {
          // open the file named ansAlgo1.dlp in the current directory
          fw = new FileWriter(directory + "queriesAlgo2.dlp");
        } catch (IOException e3) {
          System.out.println("Can't create file " + e3.getMessage());
        }
      }
      // Pour prendre en compte les réponses à partir d'un fichier transmis
      // queriesChecked = Cache.initFromAns2File(directory+"ansCacheT11Algo2");

      int nombreEntier;
      //System.out.println("Rentrez un nombre positif ou nul :");
      Scanner lectureClavier = new Scanner(System.in);
      //nombreEntier = lectureClavier.nextInt();
      System.out.println("Cache before validation :\n" + queriesChecked);
      //nombreEntier = lectureClavier.nextInt();

      int cpt = 0;
      LinkedListRules ansQuerySet;
      try {
        // Solutions are in file ./ansAlgo1.dlp
        FileReader fr = new FileReader("./ansAlgo1.dlp");
        BufferedReader br = new BufferedReader(fr);
        boolean ended = false;
        while (!ended) {
          ansQuerySet = new LinkedListRules();
          try {
            String line = br.readLine();
            //Solutions were processed per packet of 5000
            while (line != null && (cpt + 1) % 5000 != 0) {
              cpt++;
              try {
                line = line.substring(0, line.length() - 1);
                ansQuerySet.add(Rule.stringToRule(line));
              } catch (SyntaxError es) {
                System.out.println("Syntaxe error " + es.getMessage());
              }
              line = br.readLine();
            }
            if (line != null) {
              cpt++;
              try {
                line = line.substring(0, line.length() - 1);
                ansQuerySet.add(Rule.stringToRule(line));
              } catch (SyntaxError es) {
                System.out.println("Syntaxe error " + es.getMessage());
              }
            }
            for (Rule r2 : ansQuerySet) {
              // for each solution we build a subset of queries to check for the validation of the solution
              Query q2 = (Query) r2;
              // VALIDATOR
              LinkedListRules newQueries = q2.algo2(negConstraintsSet, keyConstraintsSet,
                  posConstraintsSet);
              if (verbose >= 2) {
                System.out.println("\n\n For the answer : " + q2);
              }
              if (verbose >= 2) {
                System.out.println("Queries to check  " + newQueries);
              }

              boolean ok = true;
              int i = 0;
              Query q3 = null;
              while (i < newQueries.size() && ok) {
                q3 = (Query) newQueries.get(i);
                Integer checked = queriesChecked.isInCache(q3);
                // si la requete n'est pas dans le cache
                //Pour ne pas utiliser le cache
                //checked = null;
                if (checked == null) {
                  if (verbose >= 2) {
                    System.out.println("\n\n query not in cache : " + q3);
                  }
                  //nombreEntier = lectureClavier.nextInt();

                  cptQV++;

                  //EVALUATOR
                  if (classic) {
                    ok = MySQL.checkBQ(q3, sch, dataBase, user, mdp);
                  } else if (LUBM) {
                    ok = true;
                    //System.out.println("Query to check  " + q3 + " rep " + ok);
                    //nombreEntier = lectureClavier.nextInt();
                    //ok = Sparql.checkQuery(q3, schSp);
                  }
                  int ans;
                  if (ok) {
                    ++i;
                    ans = 1;
                  } else {
                    ans = -1;
                  }
                  // on ajoute la requete dans le cache avec sa reponse
                  queriesChecked.addInCache(q3, ans);
                  if (LUBM) {
                    // on ajoute la requete dans le fichier
                    System.out.println("Query to check  " + q3);
                    String s3 = q3.queryToSQL(sch, true);
                    fw.write(s3 + ".\n");
                  }
                } else {
                  if (verbose >= 2) {
                    System.out.println("\n\n query in cache : " + q3 + " checked : " + checked);
                  }
                  cptQAV++;
                  // la requete est dans le cache, on prend la réponse
                  if (checked == 1) {
                    ok = true;
                    ++i;
                  } else {
                    ok = false;
                  }
                }
              }

              if (ok) {
                if (verbose >= 2) {
                  System.out.println(" answer is valide \n");
                }
                ansQueryValidSet.add(q2);
              } else {
                if (verbose >= 2) {
                  if (q3.getHead().getName().equals("?")) {
                    System.out.println(
                        " answer is not valide because  " + q3 + " has answers in BDD \n");
                  } else {
                    System.out.println(
                        " answer is not valide because  " + q3 + " has answers in BDD \n");
                  }
                }
              }
            }
          } catch (IOException e1) {
            System.out.println("Error reading " + e1.getMessage());
          }
          if (ansQuerySet.size() < 5000) {
            ended = true;
          }
        } // while not ended
      } catch (FileNotFoundException e2) {
        System.out.println("File not found " + e2.getMessage());
      }

      if (LUBM) {
        fw.close();
      }
      //System.out.println("Cache after validation :\n" + queriesChecked);
      long tempsFin3 = System.currentTimeMillis();
      seconds = (tempsFin3 - tempsFin2) / 1000F;
      System.out.println("validation effectuée en: " + seconds + " secondes.");
      System.out.println("Number of queries in cache : " + queriesChecked.size());
      System.out.println("Number of new queries for validation : " + cptQV);
      System.out.println("Number of queries allready validated " + cptQAV);
      System.out.println("Number of respons after algo1: " + cpt);
      System.out.println("Number of respons : " + ansQueryValidSet.size());
      if (classic || LUBM) {
        System.out.println(" Validate respons for query " + q1 + " \n are ");
        for (Rule rs : ansQueryValidSet) {
          System.out.println(rs.getHead());
        }
        System.out.println("Cache after validation :\n" + queriesChecked);
      }
    } catch (Exception ignored) {
    }

  }

  static void argsOrder() {
    System.out.println("args[0] : path to BddAll file (no filename).");
    System.out.println("args[1] : path to InfRule file (no filename).");
    System.out.println("args[2] : SaveBdd path (with filename).");
    System.out.println("args[3] : path to AddAtome (no filename).");
  }

  public static void test_null(String[] args) throws IOException {
    Bdd db = new BddMemoire();
    try {
      db = BddMemoire.fromFile(args[0] + "BddAll");
    } catch (Exception ignored) {
    }
    Variable v;
    Constant c;
    for (Atom a : db) {
      for (Element e : a) {
        System.out.print(e);
        switch (e.getType()) {
          case Element.CONSTANT:
            System.out.println(" est une constante");
            break;
          case Element.VARIABLE:
            System.out.println(" est une variable");
            break;
          case Element.NULL:
            System.out.println(" est EXISTENTIELLE =======");
            break;
          case Element.STARED_NULL:
            System.out.println(" est existencielle starred");
            break;
        }
      }
    }
    db.toFile(args[1]);
  }

/*	public static void main(String[] args) throws IOException{

		if (args.length > 0) {
			for (String a : args) {
				System.out.println(a);

			}
			//test_null(args);
			//mainUpdateAddBdd(args);
			//mainUpdateTestBdd(args);
			//mainUpdateTest(args);
			//mainQS(args);
			//mainChaseN();
			//mainCore();
			
			
		}else{

			argsOrder();
		}
		
	}*/

}
	
