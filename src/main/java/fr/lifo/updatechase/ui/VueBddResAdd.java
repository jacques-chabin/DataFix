package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurValiderAdd;
import fr.lifo.updatechase.model.logic.Atom;
import javax.swing.DefaultListModel;
import javax.swing.JButton;

/**
 * @author stagiaire Classe d'affichage d'une vue de resultat specifique a l'algorithme d'insertion.
 */
public class VueBddResAdd extends VueBddRes {

  JButton b_accept_with_core;

  VueBddResAdd(String label, ChaseUI mf, DefaultListModel<Atom> dlm, VueInsertion vi) {
    super(label, mf, dlm);

    b_accept_with_core = new JButton("Accept and apply Core");
    buttonFlow.add(b_accept_with_core);
    b_accept_with_core.addActionListener(
        new ControlleurValiderAdd(mainFrame, ControlleurValiderAdd.WITH_CORE));

    b_accept.addActionListener(
        new ControlleurValiderAdd(mainFrame, ControlleurValiderAdd.WITHOUT_CORE));
    this.bddDisplay.setCellRenderer(new JListResAddCustomCellRenderer(mainFrame.bddSource));
  }

  @Override
  public int getBddSize() {
    return mainFrame.vueInsertion.resBdd.size();
  }

}
