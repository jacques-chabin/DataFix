package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurAjoutAtomeRegles;
import fr.lifo.updatechase.controller.ControlleurCommentAtomeRegles;
import fr.lifo.updatechase.controller.ControlleurSupprimerSelectionRegle;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author stagiaire Vue d'ajout d'atome a une Bdd specifique a la vue des regles.
 */
public class VueAjoutAtomeRegles extends JPanel {

  VueRegles vueRegles;
  public JTextField inputText;
  JButton b_add;
  JButton b_remove;
  JButton b_comment;

  JLabel size;

  VueAjoutAtomeRegles(VueRegles vueRegles) {
    super();
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    this.vueRegles = vueRegles;

    JPanel textPanel = new JPanel();
    textPanel.setLayout(new FlowLayout());

    ControlleurAjoutAtomeRegles action = new ControlleurAjoutAtomeRegles(this.vueRegles);
    // Ajout ddu champ texte de saisie d'une regle
    this.inputText = new JTextField();
    this.inputText.setColumns(33);
    textPanel.add(this.inputText);
    this.add(textPanel);
    inputText.addActionListener(action);

    // Ajout du bouton d'ajout d'une regle
    JPanel boutons = new JPanel();
    boutons.setLayout(new FlowLayout());
    this.b_add = new JButton("Add");
    b_add.setBackground(Color.GREEN);
    boutons.add(this.b_add);
    b_add.addActionListener(action);

    // Ajout du bouton de commentaire
    this.b_comment = new JButton("(Un)Comment");
    b_comment.setBackground(Color.ORANGE);
    boutons.add(this.b_comment);
    b_comment.addActionListener(new ControlleurCommentAtomeRegles(this.vueRegles));

    // Ajout du bouton de suppression
    this.b_remove = new JButton("Remove");
    b_remove.setBackground(Color.RED);
    boutons.add(this.b_remove);
    b_remove.addActionListener(new ControlleurSupprimerSelectionRegle(vueRegles));

    this.add(boutons);

    JPanel taille = new JPanel();
    taille.setLayout(new FlowLayout());
    size = new JLabel("size : 0");
    taille.add(size);

    this.add(taille);
  }

  /**
   * Modifie l'affichage du nombre de regles.
   *
   * @param taille
   */
  void setSize(int taille) {
    this.size.setText("size : " + taille);
  }

  /**
   * Recupere l'input de l'utilisateur.
   *
   * @return String l'input utilisateur
   */
  String getInputText() {
    return this.inputText.getText();
  }
}
