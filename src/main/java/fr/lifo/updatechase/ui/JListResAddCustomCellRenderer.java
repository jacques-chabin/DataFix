package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.logic.Atom;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class JListResAddCustomCellRenderer extends JLabel implements ListCellRenderer<Atom> {

  Bdd bddSource;

  public JListResAddCustomCellRenderer(Bdd bdd) {
    setOpaque(true);
    bddSource = bdd;
  }

  @Override
  public Component getListCellRendererComponent(JList<? extends Atom> jlist, Atom atom, int index,
      boolean isSelected, boolean cellHasFocus) {

    String s = atom.toString();
    ImageIcon img = null;

    setText(s);

		/*if (!bddSource.contains(atom)) {
			img = new ImageIcon(getClass().getResource("images/plus-black-symbol.png"));
		} else {
			img = new ImageIcon(getClass().getResource("images/blank.png"));
		}
		setIcon(img);*/

    if (isSelected) {
      setBackground(jlist.getSelectionBackground());
      setForeground(jlist.getSelectionForeground());
    } else {
      setBackground(jlist.getBackground());
      setForeground(jlist.getForeground());
    }

    return this;
  }

}
