package fr.lifo.updatechase.ui;

import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author stagiaire Cette vue contient les bouton d'ajout et de suppression d'Atome d'une Bdd,
 * ainsi que le champ d'input d'un atome et le label contenant la taille de la bdd associee.
 */
public class VueAjoutAtomeBdd extends JPanel {

  VueBdd vueBdd;
  public JTextField inputText;
  JButton b_add;
  JButton b_remove;
  JButton b_comment;

  JLabel size;

  VueAjoutAtomeBdd(VueBdd vueBdd) {
    super();
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    this.vueBdd = vueBdd;

    JPanel textPanel = new JPanel();
    textPanel.setLayout(new FlowLayout());

    // Ajout ddu champ texte de saisie d'un atome
    this.inputText = new JTextField();
    this.inputText.setColumns(33);
    textPanel.add(this.inputText);
    this.add(textPanel);

    // Ajout du bouton d'ajout d'un atome
    JPanel boutons = new JPanel();
    boutons.setLayout(new FlowLayout());
    this.b_add = new JButton("Add");
    b_add.setBackground(Color.GREEN);
    boutons.add(this.b_add);

    // Ajout du bouton de commenter un atome
    b_comment = new JButton("(Un)Comment");
    b_comment.setBackground(Color.ORANGE);
    boutons.add(b_comment);

    // Ajout du bouton de suppression
    this.b_remove = new JButton("Remove");
    b_remove.setBackground(Color.RED);
    boutons.add(this.b_remove);

    this.add(boutons);

    JPanel taille = new JPanel();
    taille.setLayout(new FlowLayout());
    size = new JLabel("size : 0");
    taille.add(size);

    this.add(taille);

  }

  /**
   * Modifie le Label qui affiche la taille d'une Bdd.
   *
   * @param taille int
   */
  void displaySize(int taille) {
    this.size.setText("size : " + taille);
  }

  /**
   * Retourne le String tappe par l'utilisateur dans le champ d'input.
   *
   * @return String l'input de l'utilisateur
   */
  String getInputText() {
    return this.inputText.getText();
  }

  /**
   * Remplace le contenu du champ d'input par un String vide.
   */
  void emptyTextField() {
    inputText.setText("");
  }

  /**
   * Modifie l'affichage de la taille d'une Bdd.
   *
   * @param i int
   */
  void setCount(int i) {
    this.size.setText("size : " + i);
  }

}
