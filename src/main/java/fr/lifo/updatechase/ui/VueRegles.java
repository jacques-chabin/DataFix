package fr.lifo.updatechase.ui;

import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

/**
 * @author stagiaire La vue qui affichent les regles qui seront utilisees par les algorithme de
 * Chase,Core et d'Insertion dans une liste. Ainsi que le moyen de modifier ces regles.
 */
public class VueRegles extends JPanel {

  static int MAX_DISPLAY_SIZE = 50;

  public JList ruleDisplay;

  public ChaseUI mainFrame;

  public VueAjoutAtomeRegles vueAjoutAtome;

  VueRegles(String label, ChaseUI mf) {
    super();
    this.mainFrame = mf;

    // Layout
    this.setLayout(new BorderLayout());

    // Ajout du label et boder
    TitledBorder bordure = BorderFactory.createTitledBorder(label);
    this.setBorder(bordure);

    // Ajout de l'affichage de la bdd avec des barres de scroll
    this.ruleDisplay = new JList(mainFrame.getListModelRegles());
    JScrollPane scroll = new JScrollPane(this.ruleDisplay, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
        JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    this.add(scroll, BorderLayout.CENTER);

    this.vueAjoutAtome = new VueAjoutAtomeRegles(this);
    this.add(this.vueAjoutAtome, BorderLayout.SOUTH);
  }

  public String getInputText() {
    return vueAjoutAtome.getInputText();
  }

  int getRulesSize() {
    return this.mainFrame.getRegles().size();
  }

  void setCount(int i) {
    vueAjoutAtome.setSize(i);
  }
}
