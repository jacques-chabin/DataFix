package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.logic.Atom;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

/**
 * @author stagiaire Vue Commune a toutes les Bdd. Contient une JList et un lien vers l'application
 * principale.
 */
public abstract class VueBdd extends JPanel {

  static int MAX_DISPLAY_SIZE = 50;

  public JList<Atom> bddDisplay;

  public ChaseUI mainFrame;

  VueBdd(String label, ChaseUI mf) {
    super();
    this.mainFrame = mf;
    // Layout
    this.setLayout(new BorderLayout());

    // Ajout du label et boder
    TitledBorder bordure = BorderFactory.createTitledBorder(label);
    this.setBorder(bordure);

    // Ajout de l'affichage de la bdd avec des barres de scroll
    this.bddDisplay = new JList<>();
    this.bddDisplay.setLayoutOrientation(JList.VERTICAL);
    JScrollPane scroll = new JScrollPane(this.bddDisplay, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
        JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    this.add(scroll, BorderLayout.CENTER);
  }

  /**
   * Retourne la taille d'une Bdd.
   *
   * @return int la taille d'une Bdd
   */
  public abstract int getBddSize();

  /**
   * Permet de modifier l'affichage de la taille d'une Bdd.
   *
   * @param i int
   */
  public abstract void setCount(int i);

}
