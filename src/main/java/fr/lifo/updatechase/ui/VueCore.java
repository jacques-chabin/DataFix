package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.logic.Atom;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.DefaultListModel;
import javax.swing.JPanel;

/**
 * @author stagiaire L'onglet d'affichage de Core. contient l'affichage de la Bdd Sourceet du
 * resultat de l'algorithme de Core.
 */
public class VueCore extends JPanel {

  public VueBddRes vueRes;
  public DefaultListModel<Atom> auxListModel;
  public Bdd resBdd;
  VueBdd vueBddSource;
  VueSetUpCore vueSetUpCore;
  ChaseUI mainFrame;

  VueCore(ChaseUI mf) {
    super();
    this.setLayout(new BorderLayout());

    JPanel centre = new JPanel();
    centre.setLayout(new GridLayout(1, 2));

    this.mainFrame = mf;

    this.auxListModel = new DefaultListModel<>();

    this.vueBddSource = new VueBddSource("Initial database : ", mf);
    this.vueRes = new VueBddResCore("Database after Core : ", mainFrame, auxListModel, this);

    centre.add(vueBddSource);
    centre.add(vueRes);
    this.add(centre, BorderLayout.CENTER);

    this.vueSetUpCore = new VueSetUpCore(mf);
    this.add(vueSetUpCore, BorderLayout.NORTH);

  }

  public void populateJList() {
    this.auxListModel.clear();
    for (Atom atom : this.resBdd) {
      this.auxListModel.addElement(atom);
    }
  }
}
