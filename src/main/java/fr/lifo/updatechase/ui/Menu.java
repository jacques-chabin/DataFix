package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurMenuLoadBddAdd;
import fr.lifo.updatechase.controller.ControlleurMenuLoadBddDel;
import fr.lifo.updatechase.controller.ControlleurMenuLoadBddSource;
import fr.lifo.updatechase.controller.ControlleurMenuLoadRules;
import fr.lifo.updatechase.controller.ControlleurMenuQuitter;
import fr.lifo.updatechase.controller.ControlleurMenuSaveBddAdd;
import fr.lifo.updatechase.controller.ControlleurMenuSaveBddDel;
import fr.lifo.updatechase.controller.ControlleurMenuSaveBddSource;
import fr.lifo.updatechase.controller.ControlleurMenuSaveRules;
import fr.lifo.updatechase.controller.ControlleurMenuShowNeo4jForm;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * @author stagiaire La Barre des menus du programme. Regroupe l'affichage du menu et l'association
 * d'action a chacun de ses items.
 */
public class Menu extends JMenuBar {

  ChaseUI mainFrame;

  Menu(ChaseUI cui) {
    super();
    mainFrame = cui;

    JMenu menu = new JMenu("File");
    menu.getAccessibleContext().setAccessibleDescription("Save, Open, Quit");
    this.add(menu);

    // Actions sur Bdd Source
    JMenuItem menuItem = new JMenuItem("Open initial database");
    menuItem.getAccessibleContext().setAccessibleDescription("Load initial database from a file");
    menuItem.addActionListener(new ControlleurMenuLoadBddSource(mainFrame));
    menu.add(menuItem);

    menuItem = new JMenuItem("Connect to Neo4j database");
    menuItem.getAccessibleContext()
        .setAccessibleDescription("Allows you to run algorithms on a Neo4j database");
    menuItem.addActionListener(new ControlleurMenuShowNeo4jForm(mainFrame));
    menu.add(menuItem);

    menuItem = new JMenuItem("Save initial database");
    menuItem.getAccessibleContext().setAccessibleDescription("Save initial database in a file");
    menuItem.addActionListener(new ControlleurMenuSaveBddSource(mainFrame));
    menu.add(menuItem);

    menu.addSeparator();

    // Action sur Regles
    menuItem = new JMenuItem("Open set of constraints");
    menuItem.getAccessibleContext()
        .setAccessibleDescription("Load set of cronstraints from a file");
    menuItem.addActionListener(new ControlleurMenuLoadRules(mainFrame));
    menu.add(menuItem);

    menuItem = new JMenuItem("Save set of constraints");
    menuItem.getAccessibleContext().setAccessibleDescription("Save set of constraints in a file");
    menuItem.addActionListener(new ControlleurMenuSaveRules(mainFrame));
    menu.add(menuItem);

    menu.addSeparator();

    //Action sur Bdd Add
    menuItem = new JMenuItem("Open a set of atoms for insertion");
    menuItem.getAccessibleContext()
        .setAccessibleDescription("Open a set of atoms for insertion from a file");
    menuItem.addActionListener(new ControlleurMenuLoadBddAdd(mainFrame));
    menu.add(menuItem);

    menuItem = new JMenuItem("Save a set of atoms for insertion");
    menuItem.getAccessibleContext()
        .setAccessibleDescription("Save a set of atoms for insertion in a file");
    menuItem.addActionListener(new ControlleurMenuSaveBddAdd(mainFrame));
    menu.add(menuItem);

    menu.addSeparator();

    //Action sur Bdd Delete
    menuItem = new JMenuItem("Open a set of atoms for deletion");
    menuItem.getAccessibleContext()
        .setAccessibleDescription("Open a set of atoms for deletion from a file");
    menuItem.addActionListener(new ControlleurMenuLoadBddDel(mainFrame));
    menu.add(menuItem);

    menuItem = new JMenuItem("Save a set of atoms for deletion");
    menuItem.getAccessibleContext()
        .setAccessibleDescription("Save a set of atoms for deletion in a file");
    menuItem.addActionListener(new ControlleurMenuSaveBddDel(mainFrame));
    menu.add(menuItem);

    menu.addSeparator();

    // Fermer
    menuItem = new JMenuItem("Quit");
    menuItem.getAccessibleContext().setAccessibleDescription("Quit program");
    menuItem.addActionListener(new ControlleurMenuQuitter(mainFrame));
    menu.add(menuItem);

  }
}
