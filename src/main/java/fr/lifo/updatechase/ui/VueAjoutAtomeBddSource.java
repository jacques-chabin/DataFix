package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.controller.ControlleurAjoutAtomeBddSource;
import fr.lifo.updatechase.controller.ControlleurCommentAtomeBddSource;
import fr.lifo.updatechase.controller.ControlleurSupprimerSelectionAtomeBddSource;

/**
 * @author stagiaire Vue d'ajout d'atome a une Bdd specifique a la vue Bdd Source.
 */
public class VueAjoutAtomeBddSource extends VueAjoutAtomeBdd {

  VueAjoutAtomeBddSource(VueBdd vueBdd) {
    super(vueBdd);
    ControlleurAjoutAtomeBddSource action = new ControlleurAjoutAtomeBddSource(
        (VueBddSource) vueBdd);
    b_add.addActionListener(action);
    b_remove.addActionListener(new ControlleurSupprimerSelectionAtomeBddSource(vueBdd));
    b_comment.addActionListener(new ControlleurCommentAtomeBddSource(vueBdd));
    inputText.addActionListener(action);
  }

}
