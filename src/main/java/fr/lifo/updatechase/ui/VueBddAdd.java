package fr.lifo.updatechase.ui;

import fr.lifo.updatechase.model.logic.Atom;
import java.awt.BorderLayout;
import javax.swing.DefaultListModel;

/**
 * @author stagiaire Vue Bdd specifique a la Bdd que l'on souhaite inserer.
 */
public class VueBddAdd extends VueBdd {

  public VueAjoutAtomeBdd vueAjoutAtomeBdd;

  VueBddAdd(String label, ChaseUI mf, DefaultListModel<Atom> dlm) {

    super(label, mf);
    bddDisplay.setModel(dlm);
    vueAjoutAtomeBdd = new VueAjoutAtomeBddAdd(mainFrame, this);

    this.add(vueAjoutAtomeBdd, BorderLayout.SOUTH);

  }

  @Override
  public int getBddSize() {
    return mainFrame.vueInsertion.addAtomeBdd.size();
  }

  @Override
  public void setCount(int i) {
    vueAjoutAtomeBdd.setCount(getBddSize());
  }

  public String getInputText() {
    return vueAjoutAtomeBdd.getInputText();
  }

  public void emptyInputText() {
    vueAjoutAtomeBdd.emptyTextField();
  }

}
