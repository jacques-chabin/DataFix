package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.ChaseUI;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controlleur qui sauvegarde l'etat de la Bdd Source courante dans un fichier
 * texte.
 */
public class ControlleurMenuSaveBddSource extends ControlleurMenu {

  public ControlleurMenuSaveBddSource(ChaseUI cui) {
    super(cui);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JFileChooser fc = new JFileChooser();
    fc.setDialogTitle("Enregistrer la Bdd Source");
    try {
      fc.setCurrentDirectory(new File(".").getCanonicalFile());
    } catch (Exception e1) {
      /* IGNORED */
    }

    if (!mainFrame.bddSource.isEmpty()) {
      int retour = fc.showSaveDialog(mainFrame);
      if (retour == JFileChooser.APPROVE_OPTION) {
        // un fichier a été choisi (sortie par OK)
        // chemin absolu du fichier choisi

        try {
          String path = fc.getSelectedFile().getAbsolutePath();
          mainFrame.bddSource.toFile(path);
          JOptionPane.showConfirmDialog(mainFrame, "La Bdd Source a bien ete sauvegardee.",
              "Succes", JOptionPane.DEFAULT_OPTION);
        } catch (IOException e1) {
          JOptionPane.showConfirmDialog(mainFrame,
              "Erreur : Impossible d'enregistrer le Fichier : " + e1.getMessage(), "Erreur",
              JOptionPane.DEFAULT_OPTION);
        }

      }
    } else {
      JOptionPane.showConfirmDialog(mainFrame,
          "Erreur : La Bdd Source est vide.\nIl n'y a rien a enregistrer.", "Erreur",
          JOptionPane.DEFAULT_OPTION);
    }
  }

}
