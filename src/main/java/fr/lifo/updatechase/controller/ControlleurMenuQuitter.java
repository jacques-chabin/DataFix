package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.ChaseUI;
import java.awt.event.ActionEvent;

/**
 * @author stagiaire Controlleur qui quitte l'application.
 */
public class ControlleurMenuQuitter extends ControlleurMenu {

  public ControlleurMenuQuitter(ChaseUI cui) {
    super(cui);
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    mainFrame.dispose();
  }

}
