package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.ui.VueRegles;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * Controlleur qui gere l'ajout d'une Rule a la LinkedListRule de UpdateChase et la mise a jour de
 * laffichage.
 */
public class ControlleurAjoutAtomeRegles implements ActionListener {

  VueRegles vr;

  public ControlleurAjoutAtomeRegles(VueRegles v) {
    super();
    this.vr = v;
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {
    Rule rule;
    String ruleString;

    ruleString = this.vr.getInputText();
    ruleString = ruleString.replaceAll(" ", "");
    try {
      rule = Rule.stringToRule(ruleString);
      System.out.println("aaa   " + rule);
      System.out.println(vr.mainFrame.getRegles());
      this.vr.mainFrame.getRegles().add(rule);
      System.out.println("Regles : " + rule + " a ete ajoute");

      this.vr.mainFrame.addRegleToJListReglesModele(rule);

      System.out.println("REgles apres ajout " + this.vr.mainFrame.getRegles());
      System.out.println("LISTMODELREGLES " + this.vr.mainFrame.reglesListModel);

      vr.mainFrame.updateAllRuleCounter();
      vr.vueAjoutAtome.inputText.setText("");
      vr.vueAjoutAtome.inputText.requestFocus();

    } catch (SyntaxError se) {
      JOptionPane.showMessageDialog(this.vr,
          "Erreur de syntaxe detectee\n" + "Syntaxe d'un atome :\n"
              + "nomPredicat(variable1,variable2,...), nomPred(...) -> nomPred(...) .");
    }
  }

}
