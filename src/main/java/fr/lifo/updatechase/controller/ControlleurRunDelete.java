package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.QS;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueSetUpDelete;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class ControlleurRunDelete implements ActionListener {

  ChaseUI mainFrame;
  VueSetUpDelete vueSetUpDelete;

  public ControlleurRunDelete(ChaseUI mf, VueSetUpDelete vsud) {
    super();
    mainFrame = mf;
    vueSetUpDelete = vsud;
  }

  @Override
  public void actionPerformed(ActionEvent arg0) {

    Bdd db = mainFrame.getBddSource();

    LinkedListRules llr = mainFrame.getRegles();

    boolean restreint = vueSetUpDelete.isRestricted();

    try {
      int cycle = Integer.parseInt(vueSetUpDelete.cycleC.getText());
      if (cycle >= 0) {
        if (!(db == null) && db.size() > 0) {
          if (!(llr == null)) {
            //base de données qui contient les atomes que l'ont VEUT supprimer
            Bdd bddToDel = mainFrame.vueDelete.delAtomeBdd;
            mainFrame.vueDelete.delAtomeBdd = new BddMemoire();
            //nettoyage de la liste d'atome à supprimer qui est afficher sur l'appli en haut à droite
            mainFrame.vueDelete.delAtomeListModel.clear();
            //actualisation du compteur d'atome
            mainFrame.vueDelete.vueDel.setCount(mainFrame.vueDelete.vueDel.getBddSize());

            long tempsDel = System.currentTimeMillis();
            mainFrame.vueDelete.resBdd = QS.mainDel(db, bddToDel, llr, cycle, restreint);
            long tempsFin = System.currentTimeMillis();

            float temps = (tempsFin - tempsDel) / 1000f;
            vueSetUpDelete.setExecTime(temps);

            //rempli la liste des atomes resultat avec la resBdd
            mainFrame.vueDelete.populateJList();
            //actualisation du compteur d'atome
            mainFrame.vueDelete.vueRes.setCount(mainFrame.vueDelete.vueRes.getBddSize());
          } else {
            JOptionPane.showMessageDialog(vueSetUpDelete, "Aucune regle a appliquer", "ATTENTION",
                JOptionPane.WARNING_MESSAGE);
          }
        } else {
          JOptionPane.showMessageDialog(vueSetUpDelete, "La base de donnee source est vide",
              "ATTENTION",
              JOptionPane.WARNING_MESSAGE);
        }
      } else {
        JOptionPane.showMessageDialog(vueSetUpDelete, "Un entier N >= 0 doit etre entre");
        vueSetUpDelete.cycleC.setText("0");
      }
    } catch (NumberFormatException e) {
      JOptionPane.showMessageDialog(vueSetUpDelete, "Un entier doit etre entre");
      vueSetUpDelete.cycleC.setText("0");
    }

  }

}
