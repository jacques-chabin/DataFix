package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.ChaseUI;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controlleur des elements du menu du programme.
 */
public abstract class ControlleurMenu implements ActionListener {

  ChaseUI mainFrame;

  ControlleurMenu(ChaseUI cui) {
    mainFrame = cui;
  }

  @Override
  abstract public void actionPerformed(ActionEvent e);

}
