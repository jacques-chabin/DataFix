package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.ui.VueBddSource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * Controlleur qui gere l'ajout d'un atome a la Bdd Source et la mise a jour de l'affichage.
 */
public class ControlleurAjoutAtomeBddSource implements ActionListener {

  VueBddSource vbs;

  public ControlleurAjoutAtomeBddSource(VueBddSource vueBddSource) {
    super();
    this.vbs = vueBddSource;

  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Atom atom;
    String atomeString;

    atomeString = this.vbs.getInputText();
    atomeString = atomeString.replaceAll(" ", "");
    if (!atomeString.contains("-")) {
      try {
        atom = Atom.stringToAtom(atomeString);
        System.out.println("aaa   " + atom);

        this.vbs.mainFrame.getBddSource().add(atom);
        System.out.println("Atome : " + atom + " a ete ajoute");

        this.vbs.mainFrame.addAtomeToJListBddSource(atom);

        System.out.println("Bdd apres ajout " + this.vbs.mainFrame.getBddSource());
        System.out.println("LISTMODEL " + this.vbs.mainFrame.bddListModel);

        vbs.mainFrame.updateAllCounter();
        vbs.vueAjoutAtome.inputText.setText("");
        vbs.vueAjoutAtome.inputText.requestFocus();

      } catch (SyntaxError se) {
        JOptionPane.showMessageDialog(this.vbs,
            "Erreur de syntaxe detectee\n" + "Syntaxe d'un atome :\n" + "nomPredicat('element').\n"
                + "  ou\n"
                + "nomPredicat('element1','element2').");
      }
    } else {
      JOptionPane.showMessageDialog(this.vbs,
          "Erreur de syntaxe detectee\n" + "Le symbole '-' n'est pas autorisé ici. \n"
              + "Ce dernier est cependant fortement conseillé dans \n"
              + "la création de règles ayant un corps de plusieurs atomes.");
    }

  }

}
