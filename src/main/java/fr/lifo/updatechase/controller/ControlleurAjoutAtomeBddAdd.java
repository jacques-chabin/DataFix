package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueBddAdd;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 * Le controlleur qui gere l'ajout d'un Atome a la Bdd d'Insertion et la mise a jour de
 * l'affichage.
 *
 * @author stagiaire
 */
public class ControlleurAjoutAtomeBddAdd implements ActionListener {

  VueBddAdd vueBddAdd;
  ChaseUI mainFrame;

  public ControlleurAjoutAtomeBddAdd(ChaseUI mf, VueBddAdd vba) {
    super();
    vueBddAdd = vba;
    mainFrame = mf;
  }

  /* (non-Javadoc)
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    Atom atom;
    String atomeString;

    atomeString = this.vueBddAdd.getInputText();
    atomeString = atomeString.replaceAll(" ", "");

    try {
      atom = Atom.stringToAtom(atomeString);

      mainFrame.vueInsertion.addAtomeBdd.add(atom);
      System.out.println("Atome : " + atom + " a ete ajoute");

      this.mainFrame.vueInsertion.addAtomeToJListBddAdd(atom);

      System.out.println("Bdd apres ajout " + mainFrame.vueInsertion.addAtomeBdd);
      System.out.println("LISTMODEL " + mainFrame.vueInsertion.addAtomeListModel);

      mainFrame.vueInsertion.vueAdd.setCount(mainFrame.vueInsertion.addAtomeBdd.size());
      vueBddAdd.emptyInputText();
      vueBddAdd.vueAjoutAtomeBdd.inputText.requestFocus();

    } catch (SyntaxError se) {
      JOptionPane.showMessageDialog(this.vueBddAdd,
          "Erreur de syntaxe detectee\n" + "Syntaxe d'un atome :\n" + "nomPredicat('element').\n"
              + "  ou\n"
              + "nomPredicat('element1','element2').");
    }

  }

}
