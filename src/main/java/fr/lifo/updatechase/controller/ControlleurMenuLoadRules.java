package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Rule;
import fr.lifo.updatechase.ui.ChaseUI;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controlleur qui charge une LinkedListRules a partir d'un fichier et met a jour
 * l'affichage de tout les vues regles.
 */
public class ControlleurMenuLoadRules extends ControlleurMenu {

  public ControlleurMenuLoadRules(ChaseUI cui) {
    super(cui);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JFileChooser fc = new JFileChooser();
    fc.setDialogTitle("Enregistrer Les Regles");
    try {
      fc.setCurrentDirectory(new File(".").getCanonicalFile());
    } catch (Exception e1) {
    }

    int retour = fc.showOpenDialog(mainFrame);
    if (retour == JFileChooser.APPROVE_OPTION) {
      // un fichier a été choisi (sortie par OK)
      // chemin absolu du fichier choisi

      try {
        String path = fc.getSelectedFile().getAbsolutePath();
        mainFrame.rules = LinkedListRules.fromFile(path);
        mainFrame.reglesListModel = new DefaultListModel<Rule>();
        mainFrame.addAllRulesToJList(mainFrame.rules);
        mainFrame.updateAllVueRegles();
        mainFrame.updateAllRuleCounter();
      } catch (FileNotFoundException e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur : Fichier non trouve.", "Erreur",
            JOptionPane.DEFAULT_OPTION);
      } catch (UnsupportedEncodingException e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur : Probleme d'encodage.", "Erreur",
            JOptionPane.DEFAULT_OPTION);
      } catch (SyntaxError e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur de Syntaxe dans le fichier.", "Erreur",
            JOptionPane.DEFAULT_OPTION);
      } catch (IOException e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur : Erreur lors de la lecture du fichier.",
            "Erreur",
            JOptionPane.DEFAULT_OPTION);
      }
    }

  }

}
