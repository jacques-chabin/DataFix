package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueBdd;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControlleurCommentAtomeBddDel implements ActionListener {

  VueBdd vueBdd;

  public ControlleurCommentAtomeBddDel(VueBdd vb, ChaseUI mf) {
    super();
    vueBdd = vb;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    int[] index;

    index = vueBdd.bddDisplay.getSelectedIndices();
    if (index != null) {
      for (int i = 0; i < index.length; i++) {
        System.out.println("INDICE COMMENT == " + index[i]);
      }

      //mise en commentaire des atomes selectionné (dans la liste et suppression des atomes en commentaires
      // dans la Bdd des atomes à supprimer)
      vueBdd.mainFrame.vueDelete.commentAtomeFromJListBddDel(index);

      System.out.println("Bdd apres comment :" + vueBdd.mainFrame.vueDelete.delAtomeBdd);
      //mise à jour du compteur dans la partie d'ajout des atomes à supprimer
      vueBdd.mainFrame.vueDelete.vueDel.setCount(vueBdd.mainFrame.vueDelete.delAtomeBdd.size());
      System.out.println("LISTMODEL APRES comment" + vueBdd.mainFrame.vueDelete.delAtomeListModel);
    }

  }

}
