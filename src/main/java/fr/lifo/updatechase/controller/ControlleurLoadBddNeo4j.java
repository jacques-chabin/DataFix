package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.db.cypher.BddNeo4j;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueNeo4jForm;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

public class ControlleurLoadBddNeo4j extends ControlleurMenu {

  private final VueNeo4jForm neo4jForm;

  public ControlleurLoadBddNeo4j(ChaseUI mainFrame, VueNeo4jForm neo4jForm) {
    super(mainFrame);
    this.neo4jForm = neo4jForm;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    this.neo4jForm.saveConfig();

    String address = this.neo4jForm.addressTextField.getText();
    String port = this.neo4jForm.portTextField.getText();
    String user = this.neo4jForm.userTextField.getText();
    String pass = this.neo4jForm.passwordTextField.getText();

    int iport = Integer.parseInt(port);
    this.mainFrame.bddSource = new BddNeo4j(address, iport, user, pass);

    this.mainFrame.resetDB();

    neo4jForm.dispatchEvent(new WindowEvent(neo4jForm, WindowEvent.WINDOW_CLOSING));
  }

}
