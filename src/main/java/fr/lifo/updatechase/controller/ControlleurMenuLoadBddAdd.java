package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.ui.ChaseUI;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * Controlleur du Bouton Charger une Bdd d'Ajout du menu qui charge une Bdd d'Ajout specifique a
 * l'onglet Insertion.
 */
public class ControlleurMenuLoadBddAdd extends ControlleurMenu {

  public ControlleurMenuLoadBddAdd(ChaseUI cui) {
    super(cui);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JFileChooser fc = new JFileChooser();
    fc.setDialogTitle("Charger une Bdd d'Ajout");
    try {
      fc.setCurrentDirectory(new File(".").getCanonicalFile());
    } catch (Exception e1) {
    }

    int retour = fc.showSaveDialog(mainFrame);
    if (retour == JFileChooser.APPROVE_OPTION) {
      // un fichier a été choisi (sortie par OK)
      // chemin absolu du fichier choisi

      try {
        String path = fc.getSelectedFile().getAbsolutePath();
        mainFrame.vueInsertion.addAtomeBdd = BddMemoire.fromFile(path);
        mainFrame.vueInsertion.addAtomeListModel = new DefaultListModel<>();
        mainFrame.vueInsertion.addAllAtomeToJListBddAdd();

        mainFrame.vueInsertion.vueAdd.bddDisplay.setModel(mainFrame.vueInsertion.addAtomeListModel);
        mainFrame.vueInsertion.populateJList();
        mainFrame.vueInsertion.vueAdd.setCount(mainFrame.vueInsertion.addAtomeBdd.size());
      } catch (FileNotFoundException e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur : Fichier non trouve.", "Erreur",
            JOptionPane.DEFAULT_OPTION);
      } catch (UnsupportedEncodingException e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur : Probleme d'encodage.", "Erreur",
            JOptionPane.DEFAULT_OPTION);
      } catch (SyntaxError e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur : Erreur de syntaxe dans le fichier.",
            "Erreur",
            JOptionPane.DEFAULT_OPTION);
      } catch (IOException e1) {
        JOptionPane.showConfirmDialog(mainFrame,
            "Erreur : Erreur d'Input/Output lors de l'ecriture/lecture.", "Erreur",
            JOptionPane.DEFAULT_OPTION);
      }
    }

  }

}
