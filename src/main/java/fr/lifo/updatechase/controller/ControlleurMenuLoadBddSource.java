package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.ui.ChaseUI;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * @author stagiaire Controlleur du bouton charger une Bdd Source. Charge une Bdd source et met a
 * jour l'affichage.
 */
public class ControlleurMenuLoadBddSource extends ControlleurMenu {

  public ControlleurMenuLoadBddSource(ChaseUI cui) {
    super(cui);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    JFileChooser fc = new JFileChooser();
    fc.setDialogTitle("Charger une Bdd Source");
    try {
      fc.setCurrentDirectory(new File(".").getCanonicalFile());
    } catch (Exception e1) {
    }

    int retour = fc.showSaveDialog(mainFrame);
    if (retour == JFileChooser.APPROVE_OPTION) {
      // un fichier a été choisi (sortie par OK)
      // chemin absolu du fichier choisi

      try {
        String path = fc.getSelectedFile().getAbsolutePath();
        BddMemoire bdd = BddMemoire.fromFile(path);
        bdd.putStartNullValue();

        mainFrame.bddSource = bdd;
        mainFrame.resetDB();
      } catch (FileNotFoundException e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur : Fichier non trouve.", "Erreur",
            JOptionPane.DEFAULT_OPTION);
      } catch (UnsupportedEncodingException e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur : Probleme d'encodage.", "Erreur",
            JOptionPane.DEFAULT_OPTION);
      } catch (SyntaxError e1) {
        JOptionPane.showConfirmDialog(mainFrame, "Erreur : Erreur de syntaxe dans le fichier.",
            "Erreur",
            JOptionPane.DEFAULT_OPTION);
      } catch (IOException e1) {
        JOptionPane.showConfirmDialog(mainFrame,
            "Erreur : Erreur d'Input/Output lors de l'ecriture.", "Erreur",
            JOptionPane.DEFAULT_OPTION);
      }
    }

  }

}
