package fr.lifo.updatechase.controller;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.ui.ChaseUI;
import fr.lifo.updatechase.ui.VueBddAdd;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author stagiaire Controlleur qui effectue la suppression des atomes selectionnes dans la JList
 * de la Bdd Add et la BddAdd courante.
 */
public class ControlleurSupprimerSelectionAtomeBddAdd implements ActionListener {

  VueBddAdd vueBddAdd;
  ChaseUI mainFrame;

  public ControlleurSupprimerSelectionAtomeBddAdd(ChaseUI mf, VueBddAdd vba) {
    super();
    vueBddAdd = vba;
    mainFrame = mf;

  }

  @Override
  public void actionPerformed(ActionEvent e) {
    int[] index;
    Atom a;
    Bdd aux = new BddMemoire();

    index = vueBddAdd.bddDisplay.getSelectedIndices();
    if (index != null) {
      for (int i = 0; i < index.length; i++) {
        System.out.println("INDICE DELETE == " + index[i]);
      }

      aux = mainFrame.vueInsertion.removeAtomeFromJListBddAdd(index);

      System.out.println(aux);

      mainFrame.vueInsertion.addAtomeBdd.destroyIntersectionWith(aux);

      System.out.println("Bdd apres suppression :" + mainFrame.vueInsertion.addAtomeBdd);
      mainFrame.vueInsertion.vueAdd.setCount(mainFrame.vueInsertion.addAtomeBdd.size());
      System.out.println("LISTMODEL APRES SUPPRESSION" + mainFrame.vueInsertion.addAtomeListModel);
    }

  }

}
