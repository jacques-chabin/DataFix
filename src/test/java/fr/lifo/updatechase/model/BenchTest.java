package fr.lifo.updatechase.model;

import fr.lifo.updatechase.benchs.Benchmarks;
import fr.lifo.updatechase.benchs.neo4j.MovieBenchmark;
import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddStats;
import fr.lifo.updatechase.model.db.cypher.BddNeo4j;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import fr.lifo.updatechase.model.logic.Atom;
import fr.lifo.updatechase.model.logic.LinkedListRules;
import fr.lifo.updatechase.model.logic.Schema;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Collection;

public class BenchTest {

  private final static PrintStream OUT_STREAM = System.out;
  private final static int SCALE = 1;
  private final static int RULES = 150;
  private final static int UPDATES = 20;

  private final static Benchmarks BENCH = new MovieBenchmark();
  private final static Schema SCHEMA = BENCH.getSchema();

  private final static Bdd[] BDD = new Bdd[]{
      //new BddMySQL("localhost", 3306, "test_database", "test_user", "test_pass"),                            // MySQL (JDBC + SQL)
      new BddNeo4j("localhost", 7687, "neo4j", "root"),
      // Neo4J (BOLT + Cypher) + NullDegree + shortPartitionPath
      //new BddNeo4j("localhost", 7687, "neo4j", "root"),                                 // Neo4J (BOLT + Cypher) + NullDegree + longPartitionPath
      //new BddNeo4j("localhost", 7687, "neo4j", "root"),                                 // Neo4J (BOLT + Cypher) + NullDegreeConstant
      //new BddNeo4j("localhost", 7687, "neo4j", "root"),                                 // Neo4J (BOLT + Cypher) + ConstantDegree
      //new BddNeo4j("localhost", 7687, "neo4j", "root"),                                 // Neo4J (BOLT + Cypher) + LinkedConstantDegree
      //new BddNeo4jV2("localhost", 7687, "neo4j", "root", SCHEMA),                       // Neo4J v2 (BOLT + Cypher)
      //new BddRedis("localhost", 6379, "bench"),                                           // RedisGraph
      //new BddNeo4j("localhost", 7688),                                                  // MemGraph (BOLT + Cypher)
      //new BddCypher("jdbc:neo4j:bolt://localhost:7687", "neo4j", "root"),                 // Neo4J (JDBC + Cypher)
      //new BddCypher("jdbc:neo4j:bolt://localhost:7688",                                   // MemGraph (JDBC + Cypher)
      //new BddCypher("jdbc:agensgraph://localhost:5432/agens", "postgre", "agensgraph"),   // AgensGraph (JDBC + Cypher)
      //new BddMemoire()                                                                    // Memory
  };

  static {
    System.setOut(new PrintStream(new OutputStream() {
      @Override
      public void write(int i) {
      }
    }));

    //((BddNeo4j) BDD[0]).setQueryStrategy(new NullDegreeNeoQueryStrategy());
    //((BddNeo4j) BDD[1]).setQueryStrategy(new NullDegreeConstantNeoQueryStrategy());
    //((BddNeo4j) BDD[2]).setQueryStrategy(new ConstantDegreeNeoQueryStrategy());
    //((BddNeo4j) BDD[3]).setQueryStrategy(new LinkedConstantDegreeNeoQueryStrategy());

    //((BddNeo4j) BDD[1]).setQueryStrategy(new ConstantDegreeNeoQueryStrategy());

    //((BddNeo4j) BDD[1]).useLongPartitionPath(true);

    //((BddRedis) BDD[0]).setQueryStrategy(new ConstantDegreeNeoQueryStrategy());
  }

  public static void main(String[] args) {

    OUT_STREAM.println("Start " + BENCH.getClass().getName());

    //// Load
    OUT_STREAM.println("\nLoad:");
    long startTime = System.currentTimeMillis();
    Collection<Atom> generated = BENCH.generateDatabase(SCALE);
    LinkedListRules rules = BENCH.generateRules(RULES);
    OUT_STREAM.println("Generated " + generated.size() + " atoms and " + rules.size() + " rules");

    BddMemoire generator = new BddMemoire(generated);
    Collection<Atom> pos_update = BENCH.generatePosUpdate(generator, UPDATES);
    Collection<Atom> neg_update = BENCH.generateNegUpdate(generator, UPDATES);
    OUT_STREAM.println(
        "Update Generated " + pos_update.size() + " add, " + neg_update.size() + " del");

    System.out.println(new BddMemoire(pos_update));

    long endTime = System.currentTimeMillis();
    OUT_STREAM.println("Load: " + (endTime - startTime) + "ms\n");

    for (Bdd bd : BDD) {
      OUT_STREAM.println("\nSetup: " + bd.getClass().getName());
      startTime = System.currentTimeMillis();

      bd.initFromDB(generated);
      OUT_STREAM.println(
          "Initialized with " + bd.getNbNulls() + " nulls and " + bd.getNbConst() + " constants");

      endTime = System.currentTimeMillis();
      OUT_STREAM.println("Setup: " + (endTime - startTime) + "ms");

      //// ADD
      BddStats stats = bd.addWithConstraints(pos_update, rules, 0, new BddMemoire(), true);
      OUT_STREAM.println("\nADD\n" + stats.getSummary());

      //// DEL
      stats = bd.removeWithConstraints(neg_update, rules, 0, true);
      OUT_STREAM.println("\nDEL\n" + stats.getSummary());
    }

        /*OUT_STREAM.println("\nDiff report:");
        BDD[1].printDiff(BDD[0], OUT_STREAM);*/

    for (Bdd bd : BDD) {
      bd.close();
    }
  }
}
