package fr.lifo.updatechase.model.db.cypher.ext;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;
import fr.lifo.updatechase.model.db.cypher.BddCypher;

public class AgensGraphTest extends BddTest {

  @Override
  protected Bdd getBdd() {
    return new BddCypher("jdbc:agensgraph://localhost:5432/postgres", "postgres", "agensgraph");
  }
}
