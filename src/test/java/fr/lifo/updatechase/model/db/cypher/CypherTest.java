package fr.lifo.updatechase.model.db.cypher;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;

public class CypherTest extends BddTest {

  @Override
  protected Bdd getBdd() {
    return new BddCypher("jdbc:neo4j:bolt://localhost:7687/", "neo4j", "root");
  }
}
