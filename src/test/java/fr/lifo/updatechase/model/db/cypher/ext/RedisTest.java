package fr.lifo.updatechase.model.db.cypher.ext;

import fr.lifo.updatechase.model.db.Bdd;
import fr.lifo.updatechase.model.db.BddTest;
import fr.lifo.updatechase.model.db.cypher.BddRedis;

public class RedisTest extends BddTest {

  protected final static String DN_NAME = "test";
  protected final static String DB_ADDR = "localhost";
  protected final static int DB_PORT = 6379;

  @Override
  protected Bdd getBdd() {
    return new BddRedis(DB_ADDR, DB_PORT, DN_NAME);
  }

}
