package fr.lifo.updatechase.model.logic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import fr.lifo.updatechase.model.SyntaxError;
import fr.lifo.updatechase.model.db.mem.BddMemoire;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RuleTest {

  private final Variable varX = new Variable("X1");
  private final Variable varY = new Variable("X2");
  private final Variable varZ = new Variable("X3");
  private final Atom body1 = new Atom("P1", new Element[]{varX});
  private final Atom body2 = new Atom("P1", new Element[]{varY});
  private final Atom body3 = new Atom("P2-", new Element[]{varX, varY, new Constant("a")});
  private final Atom head = new Atom("P2", new Element[]{varX, varZ, new Constant("b")});
  private final Rule rule = new Rule(head, List.of(body1, body2, body3));

  @Test
  void testStringToRule() throws SyntaxError {
    Rule parsedRule = Rule.stringToRule("P1(X1),P1(X2),P2-(X1, X2, a) ->P2(X1, X3, b)");
    assertEquals(this.rule, parsedRule);
  }

  @Test
  void testGetBody() {
    assertEquals(List.of(body1, body2, body3), this.rule.getBody());
  }

  @Test
  void testToString() {
    assertEquals("P1('X1'), P1('X2'), P2-('X1', 'X2', 'a') -> P2('X1', 'X3', 'b')",
        rule.toString());
  }

  @Test
  void testGetHeadNames() {
    Assertions.assertEquals("P2", this.rule.getHeadNames());
  }

  @Test
  void testEquals() {
    assertEquals(new Rule(head, List.of(body1, body2, body3)), this.rule);
  }

  @Test
  void testBodyContainsOne() {
    assertTrue(this.rule.bodyContainsOne(List.of("P1", "P3")));
    assertFalse(this.rule.bodyContainsOne(List.of("P3")));
  }

  @Test
  void testBodyContains() {
    assertTrue(this.rule.bodyContains("P1"));
    assertFalse(this.rule.bodyContains("P3"));
  }

  @Test
  void testGetVariables() {
    assertEquals(Set.of(varX, varY, varZ), this.rule.getVariables());
  }

  @Test
  void testGetVariablesInBody() {
    assertEquals(Set.of(varX, varY), this.rule.getVariablesInBody());
  }

  @Test
  void testGetSubstitution() {
    Substitution substitution = new Substitution();
    substitution.put(varX, varX);
    substitution.put(varY, varY);
    substitution.put(varZ, varZ);

    assertEquals(substitution, this.rule.getSubstitution());
  }

  @Test
  void testGetBodySubstitution() {
    Substitution substitution = new Substitution();
    substitution.put(varX, varX);
    substitution.put(varY, varY);

    assertEquals(substitution, this.rule.getBodySubstitution());
  }

  @Test
  void testGetMaxProf() {
    assertEquals(0, this.rule.getMaxProf());
  }

  @Test
  void testGetBodySubstitutions() {
    Substitution substitution1 = new Substitution();
    substitution1.put(varX, new Constant("a"));
    substitution1.put(varY, varY);
    substitution1.put(varZ, varZ);

    Substitution substitution2 = new Substitution();
    substitution2.put(varX, varX);
    substitution2.put(varY, new Constant("a"));
    substitution2.put(varZ, varZ);

    Substitution substitution3 = new Substitution();
    substitution3.put(varX, new Constant("b"));
    substitution3.put(varY, varY);
    substitution3.put(varZ, varZ);

    Substitution substitution4 = new Substitution();
    substitution4.put(varX, varX);
    substitution4.put(varY, new Constant("b"));
    substitution4.put(varZ, varZ);

    Atom atom1 = new Atom("P1", new Element[]{new Constant("a")});
    Atom atom2 = new Atom("P1", new Element[]{new Constant("b")});
    Atom atom3 = new Atom("P3", new Element[]{new Constant("b")});

    assertEquals(Set.of(substitution1, substitution2, substitution3, substitution4),
        this.rule.getBodySubstitutions(new BddMemoire(Set.of(atom1, atom2, atom3))));
  }

  @Test
  void testGetHeadSubstitutions() {
    Substitution substitution1 = new Substitution();
    substitution1.put(varX, new Constant("a"));
    substitution1.put(varY, varY);
    substitution1.put(varZ, varZ);

    Substitution substitution2 = new Substitution();
    substitution2.put(varX, new Constant("c"));
    substitution2.put(varY, varY);
    substitution2.put(varZ, varZ);

    Atom atom1 = new Atom("P2",
        new Element[]{new Constant("a"), new Constant("a"), new Constant("b")});
    Atom atom2 = new Atom("P2",
        new Element[]{new Constant("c"), new Variable("N1", Element.NULL), new Constant("b")});
    Atom atom3 = new Atom("P2",
        new Element[]{new Constant("c"), new Constant("c"), new Constant("a")});
    Atom atom4 = new Atom("P3", new Element[]{new Constant("b")});

    assertEquals(Set.of(substitution1, substitution2),
        this.rule.getHeadSubstitutions(new BddMemoire(Set.of(atom1, atom2, atom3, atom4))));
  }
}
