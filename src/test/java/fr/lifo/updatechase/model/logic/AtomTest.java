package fr.lifo.updatechase.model.logic;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import fr.lifo.updatechase.model.SyntaxError;
import java.util.List;
import org.junit.jupiter.api.Test;

class AtomTest {

  private final Constant constantA = new Constant("a");
  private final Constant constantB = new Constant("b");
  private final Constant constantC = new Constant("c");
  private final Variable variable = new Variable("X", Element.VARIABLE);
  private final Variable variableNull = new Variable("N1", Element.NULL);
  private final Variable variableStarredNull = new Variable("N2", Element.STARED_NULL);

  @Test
  void stringToAtom() throws SyntaxError {
    Atom atom = Atom.stringToAtom("P-(a, b, X, _N1).");
    assertEquals("P", atom.getName());
    assertEquals("P-", atom.getFullName());
    assertEquals(4, atom.size());

    assertEquals(constantA, atom.get(0));
    assertEquals(constantB, atom.get(1));
    assertEquals(variable, atom.get(2));
    assertEquals(variableNull, atom.get(3));
  }

  @Test
  void getMaxProf() {
    Atom atom = new Atom("P", new Element[]{constantA});
    assertEquals(-1, atom.getMaxProf());

    atom = new Atom("P", new Element[]{
        new Constant("a"),
        new Variable("N1", Element.NULL, 0)
    });
    assertEquals(0, atom.getMaxProf());

    atom = new Atom("P", new Element[]{
        new Constant("a"),
        new Variable("N1", Element.NULL, 0),
        new Variable("N2", Element.NULL, 6)
    });
    assertEquals(6, atom.getMaxProf());
  }

  @Test
  void setName() {
    Atom atom = new Atom("P1-", new Element[]{constantA});
    atom.setName("P2");
    assertEquals("P2", atom.getName());
    assertEquals("P2-", atom.getFullName());
  }

  @Test
  void setFullName() {
    Atom atom = new Atom("P1", new Element[]{constantA});
    atom.setFullName("P2-");
    assertEquals("P2", atom.getName());
    assertEquals("P2-", atom.getFullName());
  }

  @Test
  void getVariables() {
    Atom atom = new Atom("P",
        new Element[]{constantA, constantB, variable, variableNull, variableStarredNull});
    assertArrayEquals(new Element[]{variableStarredNull, variableNull, variable},
        atom.getVariables().stream().sorted().toArray());
  }

  @Test
  void getNulls() {
    Atom atom = new Atom("P",
        new Element[]{constantA, constantB, variable, variableNull, variableStarredNull});
    assertArrayEquals(new Element[]{variableStarredNull, variableNull},
        atom.getNulls().stream().sorted().toArray());
  }

  @Test
  void getNullsName() {
    Atom atom = new Atom("P",
        new Element[]{constantA, constantB, variable, variableNull, variableStarredNull});
    assertArrayEquals(new String[]{"N1", "N2"}, atom.getNullsName().stream().sorted().toArray());
  }

  @Test
  void getConstants() {
    Atom atom = new Atom("P",
        new Element[]{constantA, constantB, variable, variableNull, variableStarredNull});
    assertArrayEquals(new Element[]{constantA, constantB},
        atom.getConstants().stream().sorted().toArray());
  }

  @Test
  void getNbNulls() {
    Atom atom = new Atom("P",
        new Element[]{constantA, constantB, variable, variableNull, variableStarredNull});
    assertEquals(2, atom.getNbNulls());
  }

  @Test
  void hasAnyElement() {
    Atom atom = new Atom("P",
        new Element[]{constantA, constantB, variable, variableNull, variableStarredNull});
    assertTrue(atom.hasAnyElement(List.of(constantA, variableNull)));
    assertTrue(atom.hasAnyElement(List.of(constantB, constantC)));
    assertFalse(atom.hasAnyElement(List.of(constantC)));
    assertFalse(atom.hasAnyElement(List.of()));
  }

  @Test
  void containsNullValues() {
    Atom atom = new Atom("P", new Element[]{constantA, constantB});
    assertFalse(atom.containsNullValues());

    atom = new Atom("P", new Element[]{constantA, constantB, variable});
    assertFalse(atom.containsNullValues());

    atom = new Atom("P", new Element[]{constantA, constantB, variable, variableNull});
    assertTrue(atom.containsNullValues());

    atom = new Atom("P",
        new Element[]{constantA, constantB, variable, variableNull, variableStarredNull});
    assertTrue(atom.containsNullValues());
  }

  @Test
  void containsNullValuesStared() {
    Atom atom = new Atom("P", new Element[]{constantA, constantB});
    assertFalse(atom.containsNullValuesStared());

    atom = new Atom("P", new Element[]{constantA, constantB, variable});
    assertFalse(atom.containsNullValuesStared());

    atom = new Atom("P", new Element[]{constantA, constantB, variable, variableNull});
    assertFalse(atom.containsNullValuesStared());

    atom = new Atom("P",
        new Element[]{constantA, constantB, variable, variableNull, variableStarredNull});
    assertTrue(atom.containsNullValuesStared());
  }

  @Test
  void containsConstantValues() {
    Atom atom = new Atom("P", new Element[]{constantA, constantB});
    assertTrue(atom.containsConstantValues());

    atom = new Atom("P", new Element[]{constantA, constantB, variable});
    assertTrue(atom.containsConstantValues());

    atom = new Atom("P", new Element[]{constantA, constantB, variable, variableNull});
    assertTrue(atom.containsConstantValues());

    atom = new Atom("P",
        new Element[]{constantA, constantB, variable, variableNull, variableStarredNull});
    assertTrue(atom.containsConstantValues());

    atom = new Atom("P", new Element[]{variableNull});
    assertFalse(atom.containsConstantValues());
  }

  @Test
  void isDeletableInRuleBody() {
    Atom atom = new Atom("P", new Element[]{constantA});
    assertFalse(atom.isDeletableInRuleBody());

    atom = new Atom("-P", new Element[]{constantA});
    assertTrue(atom.isDeletableInRuleBody());
  }

  @Test
  void isLessInstanciate() {
    Atom atom1 = new Atom("P", new Element[]{new Variable("N1", Element.NULL)});
    Atom atom2 = new Atom("P", new Element[]{constantA});
    assertTrue(atom1.isLessInstanciate(atom2));
    assertFalse(atom2.isLessInstanciate(atom1));

    atom1 = new Atom("P", new Element[]{
        new Variable("N1", Element.NULL), new Variable("N2", Element.NULL)
    });
    atom2 = new Atom("P", new Element[]{
        constantA, new Variable("N2", Element.NULL)
    });
    assertTrue(atom1.isLessInstanciate(atom2));
    assertFalse(atom2.isLessInstanciate(atom1));

    atom1 = new Atom("P", new Element[]{
        new Variable("N1", Element.NULL), new Variable("N2", Element.NULL)
    });
    atom2 = new Atom("P", new Element[]{
        new Variable("N2", Element.NULL), new Variable("N2", Element.NULL)
    });
    assertTrue(atom1.isLessInstanciate(atom2));
    assertFalse(atom2.isLessInstanciate(atom1));
  }

  @Test
  void equalsModuloNull() {
    Atom atom1 = new Atom("P", new Element[]{
        constantA, new Variable("N1", Element.NULL)
    });
    Atom atom2 = new Atom("P", new Element[]{
        constantA, new Variable("N2", Element.NULL)
    });

    assertTrue(atom1.equalsModuloNull(atom2));
    assertTrue(atom2.equalsModuloNull(atom1));
  }
}