package fr.lifo.updatechase.benchs.ldbc_nulls;

import fr.lifo.updatechase.benchs.FileBenchmark;

public class LDBC10K50NBenchmark extends FileBenchmark {

  @Override
  protected String getConstraintsFile() {
    return "benchs/ldbc_nulls/Constraints.dlp";
  }

  @Override
  protected String getInitFile() {
    return "benchs/ldbc_nulls/ldbc-50N/BddInit.dlp";
  }

  @Override
  protected String getSchemaFile() {
    return "benchs/ldbc-Schema.dlp";
  }
}
