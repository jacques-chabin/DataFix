package fr.lifo.updatechase.benchs.ldbc_nulls;

import fr.lifo.updatechase.benchs.FileBenchmark;

public class LDBC10K1000NBenchmark extends FileBenchmark {

  @Override
  protected String getConstraintsFile() {
    return "benchs/ldbc_nulls/Constraints.dlp";
  }

  @Override
  protected String getInitFile() {
    return "benchs/ldbc_nulls/ldbc-1000N/BddInit.dlp";
  }

  @Override
  protected String getSchemaFile() {
    return "benchs/ldbc-Schema.dlp";
  }
}
