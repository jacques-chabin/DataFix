import java.util.ArrayList;
import java.io.*;
import java.util.Random;

class GenererBD{
    
    static ArrayList<String> genererMot(String deb, int nb){
        // retourne une liste de nb mot commencants par deb et distinct par un numero
        ArrayList<String> res = new ArrayList<String>();
        for(int i=0; i<nb; ++i){
            res.add(deb+i);
        }
        return res;
    }

    static ArrayList<String> genererYear(int deb, int nb){
        // retourne une liste de nb années commencants par deb et distinct par un numero
        ArrayList<String> res = new ArrayList<String>();
        for(int i=0; i<nb; ++i){
            res.add(""+(deb+i));
        }
        return res;
    }
        
	public static void main(String[] args){
		int nbUniv = 20;
		int nbCnrs= 20;
		int nbAuthor = 100;
		int nbYear = 10;
		int nbConf = 50;
		int nbJournal = 50;
		int nbPubli = 500; 
		
		
		/* int nbUniv = 50;
		int nbCnrs= 50;
		int nbAuthor = 500;
		int nbYear = 30;
		int nbConf = 100;
		int nbJournal = 100;
		int nbPubli = 1000; */

		
		/* int nbUniv = 50;
		int nbCnrs= 50;
		int nbAuthor = 500;
		int nbYear = 30;
		int nbConf = 100;
		int nbJournal = 100;
		int nbPubli = 3000; */
		
        ArrayList<String> listeUniv= genererMot("LaboU", nbUniv);
        ArrayList<String> listeCnrs= genererMot("LaboC", nbCnrs);
        ArrayList<String> listeAuthor = genererMot("Author", nbAuthor);
        ArrayList<String> listeYear = genererYear(1990, nbYear);
        ArrayList<String> listeTitre = genererMot("Titre", nbPubli);
        ArrayList<String> listeJournal = genererMot("Journal", nbJournal);
        ArrayList<String> listeConf = genererMot("Conf", nbConf);
		ArrayList<String> listeLocation = new ArrayList<String>();
		for(int i=0; i<listeConf.size(); ++i){
			listeLocation.add(listeConf.get(i));
		}
		for(String s : listeJournal){
			listeLocation.add(s);
		}
		
        System.out.println(listeYear);
        System.out.println(listeLocation);
        int cpt, nombreAleatoire;
        Random rand = new Random();
        try{
			FileWriter fr = new FileWriter("./Insert.sql");

			fr.write("insert into conf values");
			cpt=0;
			for(String s : listeConf){
				if(cpt>0){
					fr.write(",('"+s+"')");
				}
				else{
					fr.write("('"+s+"')");
				}
				cpt++;
			}
			// On ajoute 5% de journaux
			
			nombreAleatoire = rand.nextInt();
			for(String s : listeJournal){
				nombreAleatoire = rand.nextInt(100);
				if(nombreAleatoire > 94){
					fr.write(",('"+s+"')");
				}
			}
			fr.write(";\n");
			
			// les journaux
			fr.write("insert into journal values");
			cpt=0;
			for(String s : listeJournal){
				if(cpt>0){
					fr.write(",('"+s+"')");
				}
				else{
					fr.write("('"+s+"')");
				}
				cpt++;
			}
			// On ajoute 5% de conf
			for(String s : listeConf){
				nombreAleatoire = rand.nextInt(100);
				if(nombreAleatoire > 94){
					fr.write(",('"+s+"')");
				}
			}
			fr.write(";\n");
			
			// les labo cnrs
			fr.write("insert into cnrs values");
			cpt=0;
			for(String s : listeCnrs){
				if(cpt>0){
					fr.write(",('"+s+"')");
				}
				else{
					fr.write("('"+s+"')");
				}
				cpt++;
			}
			// On ajoute 5% de univ
			for(String s : listeUniv){
				nombreAleatoire = rand.nextInt(100);
				if(nombreAleatoire > 94){
					fr.write(",('"+s+"')");
				}
			}
			fr.write(";\n");
			
			// les labo univ
			fr.write("insert into univ values");
			cpt=0;
			for(String s : listeUniv){
				if(cpt>0){
					fr.write(",('"+s+"')");
				}
				else{
					fr.write("('"+s+"')");
				}
				cpt++;
			}
			// On ajoute 5% de cnrs
			for(String s : listeCnrs){
				nombreAleatoire = rand.nextInt(100);
				if(nombreAleatoire > 94){
					fr.write(",('"+s+"')");
				}
			}
			fr.write(";\n");

			// les rankings
			fr.write("insert into ranking values");
			cpt=0;
			char taux='A';
			for(String s : listeLocation){
				if(cpt>0){
					fr.write(",");
				}
				cpt++;
				nombreAleatoire = rand.nextInt(3);
				if(nombreAleatoire == 0){
					taux='A';
				}
				else if(nombreAleatoire == 1){
					taux='B';
				}
				else{
					taux='C';
				}
				fr.write("('"+s+"', '"+taux+"')");
				// 10% ont 2 taux
				nombreAleatoire = rand.nextInt(100);
				if(nombreAleatoire >=90){
					taux = (char)((int)taux+1);
					if(taux=='D'){
						taux='A';
					}
					fr.write(",('"+s+"', '"+taux+"')");
					// 10% ont trois taux
					nombreAleatoire = rand.nextInt(100);
					if(nombreAleatoire >=90){
						taux = (char)((int)taux+1);
						if(taux=='D'){
							taux='A';
						}
						fr.write(",('"+s+"', '"+taux+"')");
					}
				}
			}
			
			
			fr.write(";\n");
			
			

			fr.close();
		}catch (IOException ioe){System.out.println(ioe.getMessage());};
		int nba1, nba2, nby, nbloc, nblab, cpta=0;
		String a1, a2, y, loc, lab;
		try{
			FileWriter fprod = new FileWriter("./InsertProd.sql");
			FileWriter faff = new FileWriter("./InsertAff.sql");
			fprod.write("insert into prod values ");
			faff.write("insert into aff values ");
			cpt = 0;
			for(String s : listeTitre){
				nba1 = rand.nextInt(listeAuthor.size());
				a1=listeAuthor.get(nba1);
				nby = rand.nextInt(listeYear.size());
				y=listeYear.get(nby);
				nbloc = rand.nextInt(listeLocation.size());
				loc = listeLocation.get(nbloc);
				nblab = rand.nextInt(listeCnrs.size()+listeUniv.size());
				if(nblab < listeCnrs.size()){
					lab = listeCnrs.get(nblab);
				}
				else{
					nblab -= listeCnrs.size();
					lab = listeUniv.get(nblab);
				}
				
				// chaque publi a entre 1 et 4 auteurs
				int nbau=rand.nextInt(4)+1;
				while(nbau>0){	
					if(cpt>0){
						fprod.write(",");
					}
					cpt++;
					nba1 = rand.nextInt(listeAuthor.size());
					a1=listeAuthor.get(nba1);
					fprod.write("('"+s+"','"+a1+"','"+y+"','"+loc+"','"+lab+"')");
					//80% sont affiliés dans le labo 
					nombreAleatoire = rand.nextInt(100);
					if(nombreAleatoire < 80){
						if(cpta>0){
							faff.write(",");
						}
						cpta++;
						faff.write("('"+a1+"','"+lab+"','"+y+"')");
					}
					nbau--;
				}
				
			}		
			fprod.write(";\n");
			faff.write(";\n");
			fprod.close();
			faff.close();
			
		}catch (IOException ioe){System.out.println(ioe.getMessage());};
	}
}
