CREATE DATABASE IF NOT EXISTS `lubm` DEFAULT CHARACTER SET utf8;

USE `lubm`;

-- FullProfessor(idFullProfessor, cf_factor).
-- GraduateStudent(idGraduateStudent, cf_factor).
-- Publication(idPublication, cf_factor).
-- publicationAuthor(publication,person, cf_factor).
-- ResearchAssistant(idResearchAssistant, cf_factor).
-- TeachingAssistant(idTeachingAssistant, cf_factor).
-- Organization(idOrganization, cf_factor).
-- worksFor(person,organization, cf_factor).
-- memberOf(person,organization, cf_factor).

DROP TABLE IF EXISTS `FullProfessor`;
CREATE TABLE `FullProfessor` (
  `idFullProfessor` varchar(100) character set utf8 collate utf8_bin primary key,
  `cf_factor` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `GraduateStudent`;
CREATE TABLE `GraduateStudent` (
  `idGraduateStudent` varchar(100) character set utf8 collate utf8_bin primary key,
  `cf_factor` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Publication`;
CREATE TABLE `Publication` (
  `idPublication` varchar(100) character set utf8 collate utf8_bin primary key,
  `cf_factor` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Organization`;
CREATE TABLE `Organization` (
  `idOrganization` varchar(100) character set utf8 collate utf8_bin primary key,
  `cf_factor` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ResearchAssistant`;
CREATE TABLE `ResearchAssistant` (
  `idResearchAssistant` varchar(100) character set utf8 collate utf8_bin primary key,
  `cf_factor` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `TeachingAssistant`;
CREATE TABLE `TeachingAssistant` (
  `idTeachingAssistant` varchar(100) character set utf8 collate utf8_bin primary key,
  `cf_factor` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `publicationAuthor`;
CREATE TABLE `publicationAuthor` (
  `publication` varchar(100) character set utf8 collate utf8_bin,
  `person` varchar(100) character set utf8 collate utf8_bin,
  `cf_factor` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `worksFor`;
CREATE TABLE `worksFor` (
  `person` varchar(100) character set utf8 collate utf8_bin,
  `organization` varchar(100) character set utf8 collate utf8_bin,
  `cf_factor` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `memberOf`;
CREATE TABLE `memberOf` (
  `person` varchar(100) character set utf8 collate utf8_bin,
  `organization` varchar(100) character set utf8 collate utf8_bin,
  `cf_factor` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
