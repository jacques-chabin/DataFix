--Test2_1 2pos 0neg 0key taus85
 set out_table_result=result_T2_Q1_85;
 set factor=85

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor} ;


--Test2_2 2pos(1tgd) 0neg 0key taus60
 set out_table_result=result_T2_Q1_60;
 set factor=60

Loading data to table default.result_t2_q1_60
Table default.result_t2_q1_60 stats: [numFiles=1, numRows=177371, totalSize=50168139, rawDataSize=49990768]
MapReduce Jobs Launched:
Stage-Stage-1: Map: 130  Reduce: 3   Cumulative CPU: 5202.72 sec   HDFS Read: 534055851 HDFS Write: 23337907541 SUCCESS
Stage-Stage-2: Map: 258  Reduce: 92   Cumulative CPU: 5650.75 sec   HDFS Read: 23358828595 HDFS Write: 1491400129 SUCCESS
Stage-Stage-3: Map: 158  Reduce: 7   Cumulative CPU: 1531.59 sec   HDFS Read: 1557717053 HDFS Write: 65868848 SUCCESS
Stage-Stage-4: Map: 65  Reduce: 1   Cumulative CPU: 1553.08 sec   HDFS Read: 238132915 HDFS Write: 50168228 SUCCESS
Total MapReduce CPU Time Spent: 0 days 3 hours 52 minutes 18 seconds 140 msec
OK
Time taken: 3577.832 seconds

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor} ;


--Test3_1 3pos(2tgd) 0neg 0key taus85
 set out_table_result=result_T3_Q1_85;
 set factor=85

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor} ;

--Test3_2 3pos(2tgd) 0neg 0key taus85
 set out_table_result=result_T3_Q1_60;
 set factor=60

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor} ;

--Test4_1 3pos(2tgd) 0neg 1key taus85
 set out_table_result=result_T4_Q1_85;
 set factor=85

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor}
 And fprof.idFullProfessor IN(SELECT pa3.person FROM  publicationAuthor_p pa1 GROUP BY pa3.person HAVING Count(*)=1);

--Test4_2 3pos(2tgd) 0neg 1key taus60
 set out_table_result=result_T4_Q1_60;
 set factor=60

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor}
 And fprof.idFullProfessor IN(SELECT pa3.person FROM  publicationAuthor_p pa3 GROUP BY pa3.person HAVING Count(*)=1);

--Test4_3 3pos(2tgd) 0neg 1key taus85
 set out_table_result=result_T4b_Q1_85;
 set factor=85

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor};
 
 
--Test5_1 3pos(2tgd) 1neg 0key taus85
 set out_table_result=result_T5_Q1_85;
 set factor=85

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor}
 And te.course <> tc.course;


--Test5_2 3pos(2tgd) 1neg 0key taus85
 set out_table_result=result_T5_Q1_60;
 set factor=60

 set hive.auto.convert.join=false;
 create table IF NOT EXISTS ${hiveconf:out_table_result} (idPublication STRING, idFullProfessor STRING, idGraduateStudent STRING, organization  STRING, course STRING, course2 STRING );
 INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
 SELECT p.idPublication, fprof.idFullProfessor, gstu.idGraduateStudent, w.organization, te.course, tc.course
 FROM Publication_p p
 JOIN publicationAuthor_p pa1 ON (p.idPublication = pa1.publication)
 JOIN publicationAuthor_p pa2 ON (p.idPublication = pa2.publication)
 JOIN FullProfessor_p fprof ON (pa1.person = fprof.idFullProfessor)
 JOIN GraduateStudent_p gstu ON (pa2.person = gstu.idGraduateStudent)
 JOIN worksFor_p w ON (pa1.person = w.person)
 JOIN memberOf_p m ON (pa2.person = m.person)
 JOIN teachingAssistantOf_p te ON (pa2.person = te.teachingAssistant)
 JOIN takesCourse_p tc ON (pa2.person = tc.student)
 WHERE w.organization=m.organization
 And p.cf_factor>=${hiveconf:factor}
 And pa1.cf_factor>=${hiveconf:factor}
 And pa2.cf_factor>=${hiveconf:factor}
 And fprof.cf_factor>=${hiveconf:factor}
 And gstu.cf_factor>=${hiveconf:factor}
 And w.cf_factor>=${hiveconf:factor}
 And m.cf_factor>=${hiveconf:factor} 
 And te.cf_factor>=${hiveconf:factor}
 And tc.cf_factor>=${hiveconf:factor}
 And te.course <> tc.course;
