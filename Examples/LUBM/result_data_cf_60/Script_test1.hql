-- Test1 validation des 46 réponses 35 requêtes à évaluer

set out_table_result=result_T1_Q2_1;
set factor=85

set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent10' And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_2;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result}
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent10' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_3;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor7'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor7/Publication0' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};


set out_table_result=result_T1_Q2_4;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent10'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor7/Publication0' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_5;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor7'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_6;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent16'  And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_7;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent16' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_8;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor6'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor6/Publication12' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_9;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent16'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor6/Publication12' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_10;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor6'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_11;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent25'  And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_12;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent25' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_13;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor1'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication0' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_14;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent25'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication0' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_15;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor1'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_16;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor3'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor3/Publication6' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_17;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent25'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor3/Publication6' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_18;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor3'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_19;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor1'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication16' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_20;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent25'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication16' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_21;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent101' And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_22;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent101' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_23;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor3'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor3/Publication11' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_24;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent101'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor3/Publication11' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_25;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor1'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication5' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_26;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent101'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor1/Publication5' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_27;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent107' And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_28;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent107' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_29;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor4'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor4/Publication16' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_30;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent107'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor4/Publication16' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_31;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(worksFor_p0.organization) FROM worksFor_p AS worksFor_p0 WHERE worksFor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor4'  And worksFor_p0.organization<>'http://www.Department0.University1697.edu' And worksFor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_32;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM ResearchAssistant_p AS ResearchAssistant_p0 WHERE ResearchAssistant_p0.idResearchAssistant = 'http://www.Department0.University1697.edu/GraduateStudent31' And ResearchAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_33;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(*) FROM TeachingAssistant_p AS TeachingAssistant_p0 WHERE TeachingAssistant_p0.idTeachingAssistant = 'http://www.Department0.University1697.edu/GraduateStudent31' And TeachingAssistant_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_34;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/FullProfessor4'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor4/Publication15' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};

set out_table_result=result_T1_Q2_35;
set factor=85
set hive.auto.convert.join=false;
create table IF NOT EXISTS ${hiveconf:out_table_result} (nbRep int);
INSERT OVERWRITE TABLE  ${hiveconf:out_table_result} 
SELECT Count(publicationAuthor_p0.publication) FROM publicationAuthor_p AS publicationAuthor_p0 WHERE publicationAuthor_p0.person = 'http://www.Department0.University1697.edu/GraduateStudent31'  And publicationAuthor_p0.publication<>'http://www.Department0.University1697.edu/FullProfessor4/Publication15' And publicationAuthor_p0.cf_factor>=${hiveconf:factor};
