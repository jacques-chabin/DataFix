CREATE DATABASE IF NOT EXISTS `benchmark` DEFAULT CHARACTER SET utf8;

USE `benchmark`;

DROP TABLE IF EXISTS `productfeatureproduct`;
CREATE TABLE `productfeatureproduct` (
  `product` int(11) not null,
  `productFeature` int(11) not null,
  PRIMARY KEY (product, productFeature)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `productfeatureproduct` WRITE;
ALTER TABLE `productfeatureproduct` DISABLE KEYS;

INSERT INTO `productfeatureproduct` VALUES (1,142),(1,144),(1,154),(1,156),(1,158),(1,159),(1,171),(1,175),(1,177),(1,178),(1,180),(1,3),(1,4),(1,16),(1,22),(1,25),(1,28),(1,34),(1,35),(2,187),(2,190),(2,196),(2,200),(2,203),(2,208),(2,211),(2,213),(2,215),(2,41),(2,42),(2,44),(2,45),(2,49),(2,50),(2,52),(2,57),(2,59),(2,61),(2,63),(2,67),(2,70),(2,72);

ALTER TABLE `productfeatureproduct` ENABLE KEYS;
UNLOCK TABLES;