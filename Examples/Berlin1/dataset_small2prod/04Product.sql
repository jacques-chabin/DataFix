CREATE DATABASE IF NOT EXISTS `benchmark` DEFAULT CHARACTER SET utf8;

USE `benchmark`;

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `nr` int(11) primary key,
  `label` varchar(100) character set utf8 collate utf8_bin default NULL,
  `comment` varchar(2000) character set utf8 collate utf8_bin default NULL,
  `producer` int(11) default NULL,
  `propertyNum1` int(11) default NULL,
  `propertyNum2` int(11) default NULL,
  `propertyNum3` int(11) default NULL,
  `propertyNum4` int(11) default NULL,
  `propertyNum5` int(11) default NULL,
  `propertyNum6` int(11) default NULL,
  `propertyTex1` varchar(250) character set utf8 collate utf8_bin default NULL,
  `propertyTex2` varchar(250) character set utf8 collate utf8_bin default NULL,
  `propertyTex3` varchar(250) character set utf8 collate utf8_bin default NULL,
  `propertyTex4` varchar(250) character set utf8 collate utf8_bin default NULL,
  `propertyTex5` varchar(250) character set utf8 collate utf8_bin default NULL,
  `propertyTex6` varchar(250) character set utf8 collate utf8_bin default NULL,
  `publisher` int(11) default NULL,
  `publishDate` date default NULL,
  INDEX USING BTREE (producer)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product` WRITE;
ALTER TABLE `product` DISABLE KEYS;

INSERT INTO `product` VALUES (1,'manner gatemen','lordlings dialyzed hoardings palmitate resisters redesigned trowing fledging disinters occasionally refry objective comedown senders attendance calculous redux zed bidets subacute swinks berhymed pumping overassured outrush corteges chitters civilest chiffonniers kimonos protects epizootic centimos dismast boomage issues aggrieves sociably ammoniacs polliwogs labyrinths infatuates whiteout dissentients newmown flunkey titillated caduceus rediscovered breaststrokes schillings endorsement cheerleaders nonconcurrent intoned outpaces inkle superpowers habaneras subsoils paramours laughed',1,831,312,735,null,150,null,'guzzling jillion psychotherapists substantiation nonuple deluded snowmelt interlards overrefinement annoyed stuntedness calcimining stereophonically','recommendation embezzler reconviction misproportions discountenances callings defacers crummiest triglyceride','decentralizations impacting promulgations bibliotherapy murexes professorships locomotes durning lyncher spoonier abhorrence assize goglets','distracts universally trashily enervator',null,null,1,'2000-11-01'),(2,'coterie','naughtiness illuminating careerers computerese brakeless mesozoa lineate fulminant batholiths mohawks exhalation paraguayan alcaldes foulings primordially almightily placed flukey improvises pommelled sententious bookmark rashers truces mordanted shunter praxeological causable compassed decertified transubstantiation automatize boxful befouling tragedienne visiting alliums triangulates hounders compressively camphorates mammons armories scrapes hanger nucleation loftless refractoriness nonhabitual paperer aridness jingliest sportswriters gained efficiently marshals tomogram tambura pureeing doughty compromised antineutrinos revertible picadors oddballs hominies drek irradiations fearlessness cortin hussy museful pupfish bulletproofing geminates nacre subsistence presifted abhors whereat wanes mooing refused biodegradability oghamic stouter venosities recopying supplantation buxomly foregoers pathologist welches comicality manifestos untangles mongols sluices demits inventers entitled taxability fancifulness claimed gastroenterology geotropically glenwood alack autochthonous nabob preempts alternativeness xviii fruiter deist electorally cooker voce abbeys composts jugsful glowing basset worshipfully rebait bushwhacker implorer jackknifing paraguayan enrolls blazonry dendrological pavilioned cully epistles foreshorten couth usurps legibilities yammered somnolently',1,1891,1040,1731,null,992,null,'scalded decoct practitioners infolds levered quartan calcined untransferable auditoria','charred payment linoleums cowsheds preconceive undergrounder nosier sawhorse coerces assn turgidities venins obliged homogenize','componential redemonstrates dewberries pearlers triplicates planked goddaughters largeness citator',null,'palpal thoroughly enactive swimmiest syrups',null,1,'2005-03-08');

ALTER TABLE `product` ENABLE KEYS;
UNLOCK TABLES;