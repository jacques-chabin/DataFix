CREATE DATABASE IF NOT EXISTS `benchmark` DEFAULT CHARACTER SET utf8;

USE `benchmark`;

DROP TABLE IF EXISTS `producttypeproduct`;
CREATE TABLE `producttypeproduct` (
  `product` int(11) not null,
  `productType` int(11) not null,
  PRIMARY KEY (product, productType)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `producttypeproduct` WRITE;
ALTER TABLE `producttypeproduct` DISABLE KEYS;

INSERT INTO `producttypeproduct` VALUES (1,5),(1,2),(1,1),(2,6),(2,3),(2,1);

ALTER TABLE `producttypeproduct` ENABLE KEYS;
UNLOCK TABLES;