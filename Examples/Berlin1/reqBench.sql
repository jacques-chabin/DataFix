select rating4, count(*) from review group by rating4;
+---------+----------+
| rating4 | count(*) |
+---------+----------+
|    NULL |     2996 |
|       1 |      660 |
|       2 |      695 |
|       3 |      761 |
|       4 |      690 |
|       5 |      676 |
|       6 |      729 |
|       7 |      678 |
|       8 |      655 |
|       9 |      754 |
|      10 |      706 |
+---------+----------+
-- similaire qq soit le rating

select language, count(*) from review group by language;
+----------+----------+
| language | count(*) |
+----------+----------+
| de       |      987 |
| en       |     4830 |
| es       |      562 |
| fr       |      717 |
| ja       |      956 |
| ko       |      308 |
| ru       |      544 |
| zh       |     1096 |
+----------+----------+

-- Est-ce que le producer d'un produit dans review est le meme que celui conservé dans product  --OUI
select product.nr, product.producer, review.producer from product, review where product.nr = review.product and product.producer != review.producer;
Empty set (0.02 sec)

-- Les types sans parent -- Il n'y a que le premier 
select nr from producttype where parent is null;
+----+
| nr |
+----+
|  1 |
+----+

select nr from producttype T1 where not exists (select parent from producttype T2 where T2.parent=T1.nr);
--96 types ne sont pas utilisés comme parent

select count(*) from producttype;
--151 types de produit possibles

--Country du reviewer et language du review sont-ils les mêmes
select review.nr, review.language, person.name, person.country from person, review where review.person=person.nr and person.country != review.language;
-- ils sont tous différents mais le langage correspond à la langue du pays

-- nombre de langages par product dans review
select product, count(distinct language) from review group by product having count(distinct language)=1;
-- 54 rows in set (0.02 sec) 54 produits ne sont reviewer que dans une seule langue
+---------+--------------------------+
| product | count(distinct language) |
+---------+--------------------------+
|       3 |                        1 |
|       5 |                        1 |
|      10 |                        1 |
|      13 |                        1 |
|      15 |                        1 |
|      16 |                        1 |
|      21 |                        1 |
|      23 |                        1 |
|      25 |                        1 |
|      26 |                        1 |
|      36 |                        1 |
|      48 |                        1 |
|      57 |                        1 |
|      82 |                        1 |
|      84 |                        1 |
|      88 |                        1 |
|      92 |                        1 |
|      98 |                        1 |
|     103 |                        1 |
|     114 |                        1 |
|     121 |                        1 |
|     123 |                        1 |
|     124 |                        1 |
|     129 |                        1 |
|     134 |                        1 |
|     169 |                        1 |
|     191 |                        1 |
|     193 |                        1 |
|     855 |                        1 |
|     884 |                        1 |
|     888 |                        1 |
|     890 |                        1 |
|     893 |                        1 |
|     903 |                        1 |
|     905 |                        1 |
|     938 |                        1 |
|     941 |                        1 |
|     942 |                        1 |
|     946 |                        1 |
|     950 |                        1 |
|     956 |                        1 |
|     957 |                        1 |
|     958 |                        1 |
|     973 |                        1 |
|     975 |                        1 |
|     977 |                        1 |
|     982 |                        1 |
|     985 |                        1 |
|     986 |                        1 |
|     987 |                        1 |
|     988 |                        1 |
|     990 |                        1 |
|     996 |                        1 |
|     997 |                        1 |
+---------+--------------------------+

-- nombre de rating1 par product dans review
select product, count(distinct rating1) from review group by product having count(distinct rating1)=1;
-- 75 rows in set (0.02 sec) 54 produits n'ont qu'une valeur pour le rating1
+---------+-------------------------+
| product | count(distinct rating1) |
+---------+-------------------------+
|       3 |                       1 |
|       5 |                       1 |
|       9 |                       1 |
|      13 |                       1 |
|      15 |                       1 |
|      17 |                       1 |
|      19 |                       1 |
|      22 |                       1 |
|      23 |                       1 |
|      25 |                       1 |
|      26 |                       1 |
|      32 |                       1 |
|      43 |                       1 |
|      44 |                       1 |
|      46 |                       1 |
|      57 |                       1 |
|      60 |                       1 |
|      64 |                       1 |
|      69 |                       1 |
|      74 |                       1 |
|      78 |                       1 |
|      88 |                       1 |
|      90 |                       1 |
|      94 |                       1 |
|      98 |                       1 |
|     103 |                       1 |
|     114 |                       1 |
|     122 |                       1 |
|     123 |                       1 |
|     126 |                       1 |
|     127 |                       1 |
|     128 |                       1 |
|     130 |                       1 |
|     134 |                       1 |
|     149 |                       1 |
|     157 |                       1 |
|     168 |                       1 |
|     169 |                       1 |
|     193 |                       1 |
|     208 |                       1 |
|     782 |                       1 |
|     806 |                       1 |
|     838 |                       1 |
|     851 |                       1 |
|     861 |                       1 |
|     871 |                       1 |
|     882 |                       1 |
|     892 |                       1 |
|     893 |                       1 |
|     903 |                       1 |
|     905 |                       1 |
|     911 |                       1 |
|     914 |                       1 |
|     924 |                       1 |
|     939 |                       1 |
|     940 |                       1 |
|     941 |                       1 |
|     942 |                       1 |
|     946 |                       1 |
|     948 |                       1 |
|     949 |                       1 |
|     952 |                       1 |
|     955 |                       1 |
|     957 |                       1 |
|     958 |                       1 |
|     966 |                       1 |
|     972 |                       1 |
|     973 |                       1 |
|     982 |                       1 |
|     987 |                       1 |
|     988 |                       1 |
|     990 |                       1 |
|     993 |                       1 |
|     996 |                       1 |
|     997 |                       1 |
+---------+-------------------------+
75 rows in set (0.00 sec)

-- nombre d'offres par vendeur
select vendor, count(*) from offer group by vendor;
+--------+----------+
| vendor | count(*) |
+--------+----------+
|      1 |     2419 |
|      2 |     2509 |
|      3 |     1342 |
|      4 |     1802 |
|      5 |     2354 |
|      6 |     1218 |
|      7 |     2556 |
|      8 |      683 |
|      9 |     1629 |
|     10 |     1095 |
|     11 |     2205 |
|     12 |      188 |
+--------+----------+
12 rows in set (0.02 sec)

-- nombre d'offres par produit
select product, count(*) from offer group by product having count(*)=1;
+---------+----------+
| product | count(*) |
+---------+----------+
|       2 |        1 |
|      18 |        1 |
|      20 |        1 |
|     986 |        1 |
+---------+----------+
4 rows in set (0.00 sec)

-- produit dont toutes les offres sont avec des vendeurs qui viennent du même pays
select product, count(distinct country) from offer, vendor where offer.vendor=vendor.nr group by product having(count(distinct country)=1);
+---------+-------------------------+
| product | count(distinct country) |
+---------+-------------------------+
|       2 |                       1 |
|       4 |                       1 |
|       7 |                       1 |
|      16 |                       1 |
|      18 |                       1 |
|      20 |                       1 |
|      55 |                       1 |
|     986 |                       1 |
|     995 |                       1 |
+---------+-------------------------+
9 rows in set (0.06 sec)

-- les produits qui n'ont qu'un review
select product, count(*) from review group by product having count(*)=1;
+---------+----------+
| product | count(*) |
+---------+----------+
|       3 |        1 |
|       5 |        1 |
|      10 |        1 |
|      13 |        1 |
|      15 |        1 |
|      16 |        1 |
|      21 |        1 |
|      23 |        1 |
|      25 |        1 |
|      26 |        1 |
|      48 |        1 |
|      88 |        1 |
|      92 |        1 |
|     103 |        1 |
|     134 |        1 |
|     169 |        1 |
|     893 |        1 |
|     903 |        1 |
|     905 |        1 |
|     941 |        1 |
|     956 |        1 |
|     957 |        1 |
|     958 |        1 |
|     975 |        1 |
|     977 |        1 |
|     982 |        1 |
|     985 |        1 |
|     986 |        1 |
|     987 |        1 |
|     990 |        1 |
|     996 |        1 |
+---------+----------+
31 rows in set (0.01 sec)

select SQL_NO_CACHE nameProduct1.label, nameProduct1.nrProduct, vendorName4.nameVendor, producerName8.nameProducer, reviewerName10.nameReviewer, offerVCountry0.country, reviewRCountry11.country, offerVCountry0.nrOffer, offerVendor3.vendor, make5.producer, productReview9.nrReview from offerVCountry as offerVCountry0, nameProduct as nameProduct1, offerProduct as offerProduct2, offerVendor as offerVendor3, vendorName as vendorName4, make as make5, vendorCountry as vendorCountry6, producerCountry as producerCountry7, producerName as producerName8, productReview as productReview9, reviewerName as reviewerName10, reviewRCountry as reviewRCountry11 where producerName8.nrProducer = producerCountry7.nrProducer  and producerName8.nrProducer = make5.producer  and vendorCountry6.country = producerCountry7.country  and vendorCountry6.country = offerVCountry0.country  and offerProduct2.product = productReview9.product  and offerProduct2.product = make5.product  and offerProduct2.product = nameProduct1.nrProduct  and offerVendor3.nrOffer = offerProduct2.nrOffer  and offerVendor3.nrOffer = offerVCountry0.nrOffer  and vendorName4.nrVendor = offerVendor3.vendor  and vendorName4.nrVendor = vendorCountry6.nrVendor  and reviewRCountry11.nrReview = productReview9.nrReview  and reviewRCountry11.nrReview = reviewerName10.nrReview 

select count(*) from offerVCountry as offerVCountry0, nameProduct as nameProduct1, offerProduct as offerProduct2, offerVendor as offerVendor3, vendorName as vendorName4, make as make5, vendorCountry as vendorCountry6, producerCountry as producerCountry7, producerName as producerName8, productReview as productReview9, reviewerName as reviewerName10, reviewRCountry as reviewRCountry11 where producerName8.nrProducer = producerCountry7.nrProducer  and producerName8.nrProducer = make5.producer  and vendorCountry6.country = producerCountry7.country  and vendorCountry6.country = offerVCountry0.country  and offerProduct2.product = productReview9.product  and offerProduct2.product = make5.product  and offerProduct2.product = nameProduct1.nrProduct  and offerVendor3.nrOffer = offerProduct2.nrOffer  and offerVendor3.nrOffer = offerVCountry0.nrOffer  and vendorName4.nrVendor = offerVendor3.vendor  and vendorName4.nrVendor = vendorCountry6.nrVendor  and reviewRCountry11.nrReview = productReview9.nrReview  and reviewRCountry11.nrReview = reviewerName10.nrReview 
+----------+
| count(*) |
+----------+
|    81373 |
+----------+

select count(*) from offerVCountry as offerVCountry0, nameProduct as nameProduct1, offerProduct as offerProduct2, offerVendor as offerVendor3, vendorName as vendorName4, make as make5, vendorCountry as vendorCountry6, producerCountry as producerCountry7, producerName as producerName8, productReview as productReview9, reviewerName as reviewerName10, reviewRCountry as reviewRCountry11 where producerName8.nrProducer = producerCountry7.nrProducer  and producerName8.nrProducer = make5.producer  and vendorCountry6.country = producerCountry7.country  and vendorCountry6.country = offerVCountry0.country  and offerProduct2.product = productReview9.product  and offerProduct2.product = make5.product  and offerProduct2.product = nameProduct1.nrProduct  and offerVendor3.nrOffer = offerProduct2.nrOffer  and offerVendor3.nrOffer = offerVCountry0.nrOffer  and vendorName4.nrVendor = offerVendor3.vendor  and vendorName4.nrVendor = vendorCountry6.nrVendor  and reviewRCountry11.nrReview = productReview9.nrReview  and reviewRCountry11.nrReview = reviewerName10.nrReview and not exists(select * from reviewPrCountry as reviewPrCountry12 where reviewPrCountry12.nrReview = reviewRCountry11.nrReview and reviewPrCountry12.country = reviewRCountry11.country);

select count(*) from reviewRCountry as reviewRCountry11
where not exists(select * from reviewPrCountry as reviewPrCountry12 where reviewPrCountry12.nrReview = reviewRCountry11.nrReview and reviewPrCountry12.country = reviewRCountry11.country);
